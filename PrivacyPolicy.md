
CryptoChat privacy policy
=========================

CryptoChat is comitted to preserving your privacy.
Because of that, we store as little data as possible.

What we store
-------------------------
* Your email: when you register, we store your email. This is done to let you reset your password if you ever forget it
* Your username: the username you enter when you register
* Your IP address is stored in our logs up to 90 days. This lets us help you if you face any problems when using the application.
* Your UNREAD messages are stored in encrypted form. We can't read your messages and when you read them, they are removed from our servers.

Data deletion
-------------------------
At any time, you can request the removal of your data from our servers by contacting the support email in the Play Store entry.
Your account and all the data asssociated with it will be deleted within a week.
