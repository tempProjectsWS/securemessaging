﻿using CryptoChat;
using System;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.Resources;
using Windows.Data.Xml.Dom;
using Windows.Networking.PushNotifications;
using Windows.UI.Notifications;

namespace MessageNotifier
{
    public sealed class MessageNotifier:IBackgroundTask
    {
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            if (DataStorage.ApplicationRunning) return;
            RawNotification notification = (RawNotification)taskInstance.TriggerDetails;
            foreach(string[] credential in DataStorage.GetNotifierCredentials())
            {
                if (notification.Content == credential[0])
                {
                    showNotification(credential[0]);
                }   
            }
        }
        private void showNotification(String text)
        {
            ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
            var toastTextElements = toastXml.GetElementsByTagName("text");
            ResourceLoader resourceGetter = ResourceLoader.GetForViewIndependentUse();
            string appName = resourceGetter.GetString("ApplicationTitle");
            string notificationText = resourceGetter.GetString("NotifierNewMessageText") + text;
            toastTextElements[0].AppendChild(toastXml.CreateTextNode(appName));
            toastTextElements[1].AppendChild(toastXml.CreateTextNode(notificationText));
            ToastNotification toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
