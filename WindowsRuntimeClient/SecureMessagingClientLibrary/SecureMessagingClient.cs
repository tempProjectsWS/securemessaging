using SecureMessagingClient.Cryptography;
using SecureMessagingClient.HTTP;
using SecureMessagingClient.Messaging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace SecureMessagingClient
{
    public class SecureMessagingException : Exception
    {
        public SecureMessagingException(string message)
            :base(message)
        {
            
        }
    }
    public static class UserManagement
    {
        public static async Task RegisterNewAccountAsync(string username, string email, string password)
        {
            var POSTparameters = new Dictionary<string, string>();
            POSTparameters[POSTKeys.POST_USER_NAME] = username;
            POSTparameters[POSTKeys.POST_USER_EMAIL] = email;
            POSTparameters[POSTKeys.POST_USER_PASSWORD] = password;
            await new RequestSender().SendRequestAsync(Addresses.ADDR_ADD_USER, POSTparameters);
        }
        public static async Task RequestPasswordResetAsync(string username, string email)
        {
            var POSTParams = new Dictionary<string, string>();
            POSTParams[POSTKeys.POST_USER_NAME] = username;
            POSTParams[POSTKeys.POST_USER_EMAIL] = email;
            await new RequestSender().SendRequestAsync(Addresses.ADDR_FORGOT, POSTParams);
        }
    }
	public class Session
	{
        private static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static readonly long MAX_TIMEDIFF = 3600;
        private long timeDiff;
		private bool loggedIn;
        private string username;
        private string privateKey, publicKey, token;
        private string oldPublic, oldPrivate;
        private RequestSender requestSender;
        private string password;
        public string Username
        {
            get
            {
                return username;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            private set
            {
                password = value;
            }
        }
        public string Token
        {
            get
            {
                return token;
            }
        }
        public string PublicKey
        {
            get
            {
                return publicKey;
            }
        }
        public string PrivateKey
        {
            get
            {
                return privateKey;
            }
        }
        public bool LoggedIn
        {
            get
            {
                return loggedIn;
            }
        }
        public static async Task<Session> StartSessionAsync(string username, string password, string token, string publicKey, string privateKey, bool updateKey = true)
        {
            var newSession = new Session(username, password, token, publicKey, privateKey);
            await newSession.loginAsync(updateKey);
            return newSession;
        }
		private Session(string username, string password, string token, string publicKey, string privateKey)
		{
            this.username = username;
            this.publicKey = publicKey;
            this.token = token;
            this.privateKey = privateKey;
            this.requestSender = new RequestSender();
            this.Password = password;
            oldPrivate = oldPublic = null;
			loggedIn = false;
		}
        private async Task loginAsync(bool updateKey)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_USER_NAME] = Username;
            parameters[POSTKeys.POST_USER_PASSWORD] = Password;
            parameters[POSTKeys.POST_USER_TOKEN] = token;
            String response = await requestSender.SendRequestAsync(Addresses.ADDR_LOGIN, parameters);
            if(response!=Responses.RESPONSE_OK)
            {
                throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
            }
            if (updateKey)
            {
                await updateKeyAsync();
            }
            loggedIn = true;
            long serverUnixTime;
            if(!long.TryParse(await requestSender.SendRequestAsync(Addresses.ADDR_TIME, new Dictionary<string, string>()), out serverUnixTime))
            {
                throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
            }
            var serverTime = epoch.AddSeconds(serverUnixTime);
            timeDiff = (long)(serverTime - DateTime.UtcNow).TotalSeconds;
            if(Math.Abs(timeDiff)>3600)
            {
                throw new SecureMessagingException(Responses.RESPONSE_TIMEDIFF_TOO_BIG);
            }
        }
		public async Task LogoutAsync()
		{
			if (loggedIn) {
				await requestSender.SendRequestAsync (Addresses.ADDR_LOGOUT, new Dictionary<string,string> ());
				loggedIn = false;
			}
		}
        private async Task updateKeyAsync()
        {
            oldPrivate = privateKey;
            oldPublic = publicKey;
            privateKey = await Task<string>.Run(() => { return DiffieHellman.generate_DH_Private(); });
            publicKey = await Task<string>.Run(() => { return DiffieHellman.calculate_DH_Public(privateKey); });
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_USER_PUBKEY] = publicKey;
            await requestSender.SendRequestAsync(Addresses.ADDR_UPDATE_PUBKEY, parameters);
        }
        private async Task<string> getPublicKeyAsync(string peer=null)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            if (peer!=null)
            {
                parameters[POSTKeys.POST_MSG_PEER]=peer;
            }
            return await requestSender.SendRequestAsync(Addresses.ADDR_GET_PUBKEY, parameters);

        }
        public static async Task<string[]> RequestNewTokenAsync(string username, string password)
        {
            string privateKey = await Task<string>.Run(() => { return DiffieHellman.generate_DH_Private(); });
            string publicKey = await Task<string>.Run(() => { return DiffieHellman.calculate_DH_Public(privateKey); });
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_USER_NAME] = username;
            parameters[POSTKeys.POST_USER_PASSWORD] = password;
            parameters[POSTKeys.POST_USER_PUBKEY] = publicKey;
            string token = await new RequestSender().SendRequestAsync(Addresses.ADDR_AUTHORIZE_NEW_TOKEN, parameters);
            string[] returnValue = { token, publicKey, privateKey };
            return returnValue;
        }
        public async Task UnauthorizeAsync()
        {
            await requestSender.SendRequestAsync(Addresses.ADDR_UNAUTHORIZE, new Dictionary<string, string>());
        }
        public async Task<bool> CheckNewMessagesAsync()
        {
            return (await requestSender.SendRequestAsync(Addresses.ADDR_CHECK_NEW_MESSAGES, new Dictionary<string, string>())) == Responses.RESPONSE_HAS_NEW_MESSAGES;
        }
		public async Task<Message> SendMessageAsync(string peer, string contents, Action<bool, Exception> callback=null, string signatureSecret=null)
		{
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_MSG_PEER] = peer;
            string peerKey = await getPublicKeyAsync(peer);
            parameters = await Task<string>.Run(() => { return Message.GeneratePOSTParameters(contents, peer,peerKey, timeDiff, signatureSecret ); });
            await requestSender.SendRequestAsync(Addresses.ADDR_SEND_MESSAGE, parameters);
            return new Message(contents, Username, peer, timeDiff);
		}
        public async Task SetPasswordAsync(string newPassword)
        {
            var parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_USER_PASSWORD] = newPassword;
            await requestSender.SendRequestAsync(Addresses.ADDR_SET_PASSWORD, parameters);
            Password = newPassword;
        }
		public async Task<MessageList> GetMessagesAsync()
		{
			Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_MSG_COUNT] = "10";
            parameters[POSTKeys.POST_SUPPORTS_FILES] = "True";
            MessageList tmpMessageList, returnMessageList;
            returnMessageList = JSONHandling.ParseJSONResponse<MessageList>(await requestSender.SendRequestAsync(Addresses.ADDR_RECEIVE_MESSAGE, parameters));
            await Task.Run(() => { returnMessageList.Decrypt(Username, publicKey, privateKey, oldPublic, oldPrivate); });
            bool more = returnMessageList.More;
            tmpMessageList = returnMessageList;
            while(more)
            {
                tmpMessageList = JSONHandling.ParseJSONResponse<MessageList>(await requestSender.SendRequestAsync(Addresses.ADDR_RECEIVE_MESSAGE, parameters));
                await Task.Run(() => { tmpMessageList.Decrypt(Username, publicKey, privateKey, oldPublic, oldPrivate); });
                returnMessageList.Append(tmpMessageList);
                more = tmpMessageList.More;
            }
            return returnMessageList;
		}
        public async Task<byte[]> DownloadFileAsync(Message message)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_FILE_KEY] = message.FileKey;
            var contents = await requestSender.SendRequestAsync(Addresses.ADDR_DOWNLOAD_FILE, parameters);
            return await Task<byte[]>.Run(() => { return Message.DecryptFile(contents, message); });
        }
        public async Task<Message> UploadFileAsync(string fileName, byte[] fileContents, string peer,
            string mimeType = "application/octet-stream")
        {
            var address = await requestSender.SendRequestAsync(Addresses.ADDR_REQUEST_FILE_UPLOAD, new Dictionary<string, string>());
            var peerKey = await getPublicKeyAsync(peer);
            var fileJson = await Task<string>.Run(()=>{return Message.GenerateFileUploadJSON(fileContents, peerKey, timeDiff);});
            var parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_MSG_PEER] = peer;
            await requestSender.UploadFileAsync(address, fileName, fileJson, mimeType, parameters);
            return new Message(fileName, Username, peer, timeDiff);
        }
        public async Task RegisterWnsAsync(string uri)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_WNS_URI] = uri;
            await requestSender.SendRequestAsync(Addresses.ADDR_REGISTER_WNS, parameters);
        }

        public async Task RenameUser(string newUsername)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters[POSTKeys.POST_NEW_USERNAME] = newUsername;
            await requestSender.SendRequestAsync(Addresses.ADDR_RENAME_USER, parameters);
            username = newUsername;
        }
	}
}
