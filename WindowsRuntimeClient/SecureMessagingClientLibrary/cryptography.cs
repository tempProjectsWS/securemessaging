using System;
using System.Numerics;
using System.Text;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
namespace SecureMessagingClient.Cryptography
{
    public class CryptographyException : SecureMessagingException
    {
        public CryptographyException(string message)
            : base(message)
        {

        }
    }
	public static class TextManipulation
	{
		public static string Encode64(byte[] data)
		{
			return Convert.ToBase64String (data);
		}
		public static byte[] Decode64(string data)
		{
			return Convert.FromBase64String (data);
		}
		public static byte[] EncodeUTF(string s)
		{
			return Encoding.UTF8.GetBytes (s);
		}
		public static string DecodeUTF(byte[] s)
		{
			return Encoding.UTF8.GetString (s,0, s.Length);
		}
	}
    public static class DiffieHellman
    {
            
        public static string generate_DH_Private(uint length=64)
        {
            BigInteger value = 0;
            while (calculate_DH_Public(value) == 1)
            {
                byte[] number;
                CryptographicBuffer.CopyToByteArray(CryptographicBuffer.GenerateRandom(length), out number);
                number[0] |= 128;
                value = NumberConversion.BytesToBigInteger(number);
            }
            return NumberConversion.BigIntegerToBase64(value);
        }
        public static BigInteger calculate_DH_Public(BigInteger exponent)
        {
            BigInteger publicNumber = BigInteger.ModPow(Constants.generator, exponent, Constants.modulus);
            return publicNumber;
        }
        public static string calculate_DH_Public(string privateKey)
        {
            return NumberConversion.BigIntegerToBase64(calculate_DH_Public(NumberConversion.Base64ToBigInteger(privateKey)));
        }
        private static byte[] calculate_DH_Result(string publicKey, string privateKey)
        {
            BigInteger exponent = NumberConversion.Base64ToBigInteger(privateKey);
            BigInteger publicBase = NumberConversion.Base64ToBigInteger(publicKey);
            BigInteger result = BigInteger.ModPow(publicBase, exponent, Constants.modulus);
            return NumberConversion.BigIntegerToBytes(result);
        }
        public static byte[] calculate_DH_AES(string publicKey, string privateKey)
        {
            byte[] keySeed = calculate_DH_Result(publicKey, privateKey);
            byte[] key = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            for (int i = 0; i < keySeed.Length;i++ )
            {
                key[i % 16] ^= keySeed[i];
            }
            return key;
        }
    }
    static class HMAC
    {
        private static readonly MacAlgorithmProvider macProvider = MacAlgorithmProvider.OpenAlgorithm(MacAlgorithmNames.HmacSha256);
        public static byte[] GenerateHMAC(byte[] data, byte[] key)
        {
            var dataBuffer = CryptographicBuffer.CreateFromByteArray(data);
            var keyBuffer = CryptographicBuffer.CreateFromByteArray(key);
            var macKey = macProvider.CreateKey(keyBuffer);
            var macBuffer = CryptographicEngine.Sign(macKey, dataBuffer);
            byte[] mac;
            CryptographicBuffer.CopyToByteArray(macBuffer, out mac);
            return mac;
        }
        public static bool VerifyHMAC(byte[] data, byte[] key, byte[] hmac)
        {
            byte[] ownMac = GenerateHMAC(data, key);
            bool identical = true;
            for (int i = 0; i < ownMac.Length; i++) identical &= ownMac[i] == hmac[i];
            return identical;
        }
    }
    public static class AES
    {
        private static readonly SymmetricKeyAlgorithmProvider aesAlgo = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
        public static string Encrypt(byte[] data, byte[] key)
        {
            var keyBuffer = CryptographicBuffer.CreateFromByteArray(key);
            var dataBuffer = CryptographicBuffer.CreateFromByteArray(data);

            var ivBuffer = CryptographicBuffer.GenerateRandom(aesAlgo.BlockLength);

            var aesKey = aesAlgo.CreateSymmetricKey(keyBuffer);

            var cipherTextBuffer = CryptographicEngine.Encrypt(aesKey, dataBuffer, ivBuffer);

            byte[] iv, ciphertext, hashedPart;
            CryptographicBuffer.CopyToByteArray(ivBuffer, out iv);
            CryptographicBuffer.CopyToByteArray(cipherTextBuffer, out ciphertext);

            hashedPart = new byte[iv.Length + ciphertext.Length];
            Array.Copy(iv, hashedPart, iv.Length);
            Array.Copy(ciphertext, 0, hashedPart, iv.Length, ciphertext.Length);
            byte[] mac = HMAC.GenerateHMAC(hashedPart, key);

            byte[] fullCipherText = new byte[hashedPart.Length + mac.Length];
            Array.Copy(hashedPart, fullCipherText, hashedPart.Length);
            Array.Copy(mac, 0, fullCipherText, hashedPart.Length, mac.Length);
            return TextManipulation.Encode64(fullCipherText);
        }
        public static byte[] Decrypt(string data, byte[] key)
        {
            var realData = TextManipulation.Decode64(data);

            byte[] iv = new byte[16];
            byte[] hashedPart = new byte[realData.Length - 32];
            byte[] mac = new byte[32];
            byte[] cipherText = new byte[realData.Length - 48];

            Array.Copy(realData, iv, 16);
            Array.Copy(realData, hashedPart, hashedPart.Length);
            Array.Copy(realData, hashedPart.Length, mac, 0, 32);
            Array.Copy(realData, 16, cipherText, 0, cipherText.Length);

            var keyBuffer = CryptographicBuffer.CreateFromByteArray(key);
            var macBuffer = CryptographicBuffer.CreateFromByteArray(mac);
            var ivBuffer = CryptographicBuffer.CreateFromByteArray(iv);
            var ciphertextBuffer = CryptographicBuffer.CreateFromByteArray(cipherText);
            if (!HMAC.VerifyHMAC(hashedPart, key, mac))
            {
                throw new CryptographyException("Message invalid");
            
            }
            var aesKey = aesAlgo.CreateSymmetricKey(keyBuffer);
            var decryptedMessageBuffer = CryptographicEngine.Decrypt(aesKey, ciphertextBuffer, ivBuffer);
            byte[] decryptedMessage;
            CryptographicBuffer.CopyToByteArray(decryptedMessageBuffer, out decryptedMessage);
            return decryptedMessage;
        }
    }
	public static class HASH
	{
		public static string SHA256Hash(byte[] data)
		{
			var hashedDataBuffer = CryptographicBuffer.CreateFromByteArray(data);
            var hashBuffer = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256).HashData(hashedDataBuffer);
            return CryptographicBuffer.EncodeToBase64String(hashBuffer);
		}
        public static string SHA512Hash(byte[] data)
        {
            var hashedDataBuffer = CryptographicBuffer.CreateFromByteArray(data);
            var hashBuffer = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha512).HashData(hashedDataBuffer);
            return CryptographicBuffer.EncodeToBase64String(hashBuffer);
        }
	}
    public static class Signatures
    {
        public static string GenerateSignature(string secret, string publickey)
        {
            BigInteger signatureBase = NumberConversion.Base64ToBigInteger(publickey);
            string exponent64 = HASH.SHA512Hash(TextManipulation.EncodeUTF(secret));
            BigInteger exponent = NumberConversion.Base64ToBigInteger(exponent64);
            return NumberConversion.BigIntegerToBase64(BigInteger.ModPow(signatureBase, exponent, Constants.modulus));
        }
        public static bool VerifySignature(string secret, string publickey, string signature)
        {
            return GenerateSignature(secret, publickey) == signature;
        }
    }
}

