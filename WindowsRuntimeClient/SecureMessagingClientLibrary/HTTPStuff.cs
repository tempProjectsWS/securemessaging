using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
namespace SecureMessagingClient
{
	namespace HTTP
	{
		public class RequestSender
		{
            private HttpClient client;
            private const string userAgent = "WinRT Client library";
            public RequestSender()
            {
                client = new HttpClient();
                client.DefaultRequestHeaders.Add("User-Agent", userAgent);
            }
			public async Task<string> SendRequestAsync(string addr, Dictionary<string, string> postargs)
			{

                var content = new HttpFormUrlEncodedContent(postargs);
                string response;
                try
                {
                    var cancelationToken = new CancellationTokenSource();
                    cancelationToken.CancelAfter(100000);
                    var responseObject = (await client.PostAsync(new Uri(Addresses.ADDR_SITE + addr), content).AsTask(cancelationToken.Token));
                    if (!responseObject.IsSuccessStatusCode) throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
                    response = await responseObject.Content.ReadAsStringAsync();
                }
                catch (TimeoutException)
                {
                    throw new SecureMessagingException(Responses.RESPONSE_TIMEOUT);
                }
                catch
                {
                    throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
                }
                if (response==null || response=="")
                {
                    throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
                }
                if (Responses.IsError(response))
                {
                    throw new SecureMessagingException(response);
                }
                return response;
			}

            private static HttpRequestMessage generateUploadRequest(string address,
                string fileName, string fileContents, string mimeType, 
                Dictionary<string, string> parameters)
            {
                var rnd = new Random();
                long boundarySeed = (long)(rnd.NextDouble() * 1000000000);
                string boundary = boundarySeed.ToString();
                string contentType = "multipart/form-data; boundary=" + boundary;
                string partBoundary = "--" + boundary;
                string requestBody = "\r\n" + partBoundary + "\r\n";
                requestBody += "Content-Disposition: form-data; name=\"" + POSTKeys.POST_UPLOADED_FILE + "\"; filename=\"" + fileName + "\"\r\n";
                requestBody += "Content-Type: " + mimeType + "\r\n\r\n";
                requestBody += fileContents + "\r\n";
                foreach (var parameter in parameters)
                {
                    requestBody += partBoundary + "\r\n";
                    requestBody += "Content-Disposition: form-data; name=\"" + parameter.Key + "\"\r\n\r\n";
                    requestBody += parameter.Value + "\r\n";
                }
                requestBody += "--" + boundary + "--";
                HttpRequestMessage uploadRequest = new HttpRequestMessage(HttpMethod.Post, new Uri(address));
                uploadRequest.Content = new HttpStringContent(requestBody);
                uploadRequest.Content.Headers.ContentType = new HttpMediaTypeHeaderValue(contentType);
                uploadRequest.Content.Headers.ContentLength = (ulong?)requestBody.Length;
                return uploadRequest;

            }
            public async Task<string>UploadFileAsync(string address, string fileName, string fileContents, string mimeType, Dictionary<string, string> parameters)
            {
                var request = generateUploadRequest(address, fileName, fileContents, mimeType, parameters);
                string response;
                try
                {
                    var cancelationToken = new CancellationTokenSource();
                    cancelationToken.CancelAfter(100000);
                    var responseObject = await client.SendRequestAsync(request).AsTask(cancelationToken.Token);
                    if (!responseObject.IsSuccessStatusCode) throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
                    response = await responseObject.Content.ReadAsStringAsync();
                }
                catch(TimeoutException)
                {
                    throw new SecureMessagingException(Responses.RESPONSE_TIMEOUT);
                }
                catch
                {
                    throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
                }
                if (response == null || response == "")
                {
                    throw new SecureMessagingException(Responses.RESPONSE_NETWORK_ERROR);
                }
                if (Responses.IsError(response))
                {
                    throw new SecureMessagingException(response);
                }
                return response;
            }
        }
	}

}