using System;


namespace SecureMessagingClient
{
	namespace HTTP
	{
		public static class Addresses
		{
            public const string
                ADDR_SITE = "https://sec-msg.appspot.com",
                ADDR_ADD_USER = "/adduser",
                ADDR_LOGIN = "/login",
                ADDR_SET_PASSWORD = "/passet",
                ADDR_LOGOUT = "/logout",
                ADDR_SEND_MESSAGE = "/send",
                ADDR_RECEIVE_MESSAGE = "/recv",
                ADDR_DELETE_MESSAGE = "/delete",
                ADDR_GET_PUBKEY = "/pubkey",
                ADDR_FORGOT = "/forgot",
                ADDR_RECOVERY = "/recovery",
                ADDR_TIME = "/time",
                ADDR_UPDATE_PUBKEY = "/pubkeyupdate",
                ADDR_AUTHORIZE_NEW_TOKEN = "/authnewtoken",
                ADDR_UNAUTHORIZE = "/unauthorize",
                ADDR_CHECK_NEW_MESSAGES = "/checknew",
                ADDR_REQUEST_FILE_UPLOAD = "/requestupload",
                ADDR_UPLOAD_COMPLETE = "/uploadcomplete",
                ADDR_DOWNLOAD_FILE = "/downloadfile",
                ADDR_REGISTER_WNS = "/registerwns",
                ADDR_RENAME_USER = "/renameuser";
        }
		//Keys for JSON dictionaries sent as responses
		public static class JSONKeys
		{
            public const string
                JSON_MSG_CONTENTS = "CONTENTS",
                JSON_MSG_SENDER = "SENDER",
                JSON_MSG_RECEIVER = "RECEIVER",
                JSON_MSG_TIMESTAMP = "TIME",
                JSON_MESSAGES = "MESSAGES",
                JSON_MORE_MESSAGES = "MORE",
                JSON_MSG_PUBLICKEY_USED = "PUBLICKEYUSED",
                JSON_MSG_PUBLCIKEY_CALCULATED = "PUBLICKEYCALCULATED",
                JSON_MESSAGE_IS_FILE = "ISFILE",
                JSON_FILENAME = "FILENAME",
                JSON_FILE_KEY = "FILEKEY",
                JSON_FILE_MIMETYPE = "MIMETYPE",
                JSON_FILE_ENCRYPTION_KEY = "FILEENCKEY",
                JSON_MSG_SM_SIGNATURE = "SMSIGNATURE";
		}

		//POST Keys
		public static class POSTKeys
		{
            public const string
                POST_MSG_CONTENTS = "CONTENTS",
                POST_MSG_PEER = "PEER",
                POST_MSG_COUNT = "COUNT",
                POST_USER_NAME = "USERNAME",
                POST_USER_PASSWORD = "PASSWORD",
                POST_USER_PUBKEY = "PUBKEY",
                POST_USER_EMAIL = "EMAIL",
                POST_USER_TOKEN = "TOKEN",
                POST_MSG_PUBLICKEY_USED = "PUBLICKEYUSED",
                POST_MSG_PUBLCIKEY_CALCULATED = "PUBLICKEYCALCULATED",
                POST_UPLOADED_FILE = "UPLOADEDFILE",
                POST_FILE_KEY = "FILEKEY",
                POST_SUPPORTS_FILES = "FILESUPPORT",
                POST_WNS_URI = "WNSURI",
                POST_MSG_SM_SIGNATURE = "SMSIGNATURE",
                POST_NEW_USERNAME = "NEW_USERNAME";
        }

		//Responses
		public static class Responses
		{
            public const string
                RESPONSE_MSG_NO_PEER = "BAD PEER",
                RESPONSE_MSG_INVALID = "BAD MSG",
                RESPONSE_USER_NAME_EXISTS = "NAME EXISTS",
                RESPONSE_USER_EMAIL_EXISTS = "EMAIL EXISTS",
                RESPONSE_USER_BAD_USERNAME = "BAD USERNAME",
                RESPONSE_USER_BAD_PASSWORD = "BAD PASSWORD",
                RESPONSE_USER_NOT_LOGGED_IN = "NOT LOGGED IN",
                RESPONSE_USER_NOT_VERIFIED = "NOT VERIFIED",
                RESPONSE_RECOVERY_EMAIL_SENT = "A message has been sent to your address with instructions to recover your password",
                RESPONSE_USER_BAD_EMAIL = "BAD EMAIL",
                RESPONSE_USER_BAD_TOKEN = "BAD TOKEN",
                RESPONSE_NETWORK_ERROR = "NETWORK ERROR",
                RESPONSE_UNHANDLED_ERROR = "UNHANDLED ERROR",
                RESPONSE_MISSING_POST_PARAMETERS = "MISSING POST PARAMETERS",
                RESPONSE_KEY_DECRYPTION_FAILED = "PRIVATE KEY DECRYPTION FAILED",
                RESPONSE_UNAUTHORIZED = "UNAUTHORIZED CLIENT",
                RESPONSE_OK = "OK",
                RESPONSE_PEER_INACTIVE = "INACTIVE",
                RESPONSE_ALREADY_AUTHORIZED = "ALREADY AUTHORIZED",
                RESPONSE_NO_NEW_MESSAGES = "NO MESSAGES",
                RESPONSE_HAS_NEW_MESSAGES = "HAS NEW MESSAGES",
                RESPONSE_BAD_FILE_KEY = "BAD FILE KEY",
                RESPONSE_FILE_CONTENTS_INVALID = "BAD FILE CONTENTS",
                RESPONSE_TIMEOUT = "TIMEOUT",
                RESPONSE_TIMEDIFF_TOO_BIG = "TIMEDIFF_TOO_BIG";

            private static string[] errorResponses = {RESPONSE_MSG_NO_PEER, RESPONSE_MSG_INVALID, 
                  RESPONSE_USER_NAME_EXISTS, RESPONSE_USER_EMAIL_EXISTS, RESPONSE_USER_BAD_USERNAME, 
                  RESPONSE_USER_BAD_PASSWORD, RESPONSE_USER_NOT_LOGGED_IN, RESPONSE_USER_NOT_VERIFIED, 
                  RESPONSE_USER_BAD_EMAIL, RESPONSE_USER_BAD_TOKEN, RESPONSE_NETWORK_ERROR,
                  RESPONSE_UNHANDLED_ERROR, RESPONSE_MISSING_POST_PARAMETERS, RESPONSE_UNAUTHORIZED,
                  RESPONSE_PEER_INACTIVE, RESPONSE_ALREADY_AUTHORIZED, RESPONSE_BAD_FILE_KEY,
                  RESPONSE_FILE_CONTENTS_INVALID, RESPONSE_TIMEOUT,
            };
			private static string[] goodResponses = {RESPONSE_RECOVERY_EMAIL_SENT, 
                RESPONSE_OK, RESPONSE_NO_NEW_MESSAGES, RESPONSE_HAS_NEW_MESSAGES,
			};
			public static bool IsError(string message)
			{
				return Array.IndexOf (errorResponses, message) != -1;
			}
			public static bool IsGood(string message)
			{
				return Array.IndexOf (goodResponses, message) != -1;
			}
		}
	}
}

