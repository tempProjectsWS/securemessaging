using SecureMessagingClient.Cryptography;
using SecureMessagingClient.HTTP;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
namespace SecureMessagingClient
{
	namespace Messaging
	{
        class MessageComparer:IComparer<Message>
        {
            int IComparer<Message>.Compare(Message m1, Message m2)
            {
                if (m1.Timestamp == m2.Timestamp) return 0;
                if (m1.Timestamp < m2.Timestamp) return 1;
                return -1;
            }
        }
        [DataContract]
        public class MessageFile
        {
            [DataMember(Name = JSONKeys.JSON_MSG_CONTENTS)]
            public readonly string Contents;
            [DataMember(Name = JSONKeys.JSON_MSG_PUBLICKEY_USED)]
            public readonly string PublicKeyUsed;
            [DataMember(Name = JSONKeys.JSON_MSG_PUBLCIKEY_CALCULATED)]
            public readonly string PublicKeyCalculated;
            public MessageFile(string contents, string publicKeyUsed, string publicKeyCalculated)
            {
                Contents = contents;
                PublicKeyUsed = publicKeyUsed;
                PublicKeyCalculated = publicKeyCalculated;
            }
        }
		[DataContract]
		public class MessageList
		{
			public Message this [ int index ]
			{
				get {
					return this.messages [index];
				}
			}
			public int Length
			{
				get {
					return this.messages.Length;
				}
			}
			[DataMember (Name = HTTP.JSONKeys.JSON_MESSAGES)]
			private Message[] messages;
			[DataMember (Name = HTTP.JSONKeys.JSON_MORE_MESSAGES)]
			private bool more;
            public bool More
            {
                get
                {
                    return more;
                }
            }
            public void Append(MessageList additional)
            {
                more = additional.More;
                Message[] newList = new Message[additional.Length + Length];
                Array.Copy(messages, newList, Length);
                Array.Copy(additional.messages, 0, newList, Length, additional.Length);
                messages = newList;
                sort();
            }
            public void AddMessage(Message m)
            {
                MessageList addedMessageList = new MessageList();
                Message[] addedMessages = {m,};
                addedMessageList.messages = addedMessages;
                this.Append(addedMessageList);
            }
			public void Decrypt(string username, string public1, string private1, string public2, string private2)
			{
				foreach (Message currentMessage in messages) {
					currentMessage.Decrypt(username, public1, private1, public2, private2);
				}
                sort();
			}
            public void DeleteMessageFromList(int index)
            {
                Message[] newList = new Message[Length - 1];
                Array.Copy(messages, newList, index);
                Array.Copy(messages, index + 1, newList, index, Length - index - 1);
                messages = newList;
            }
            public void Empty()
            {
                this.messages = new Message[0];
            }
            public MessageList()
            {
                messages = new Message[0];
                this.more = false;
            }
            private void sort()
            {
                Array.Sort<Message>(messages, new MessageComparer());
            }
            public string ToJSON()
            {
                return JSONHandling.SerializeObject(this);
            }
            public static MessageList FromJSON(string json)
            {
                return JSONHandling.ParseJSONResponse<MessageList>(json);
            }

		}
		[DataContract]
		public class Message
		{
			[DataMember (Name = HTTP.JSONKeys.JSON_MSG_CONTENTS)]
			private string contents;
			[DataMember (Name = HTTP.JSONKeys.JSON_MSG_RECEIVER)]
			private string receiver;
			[DataMember (Name = HTTP.JSONKeys.JSON_MSG_SENDER)]
			private string sender;
			[DataMember (Name = HTTP.JSONKeys.JSON_MSG_TIMESTAMP)]
            private double timestamp;
            [DataMember(Name = JSONKeys.JSON_MSG_PUBLCIKEY_CALCULATED)]
            private string publicKeyCalculated;
            [DataMember(Name = JSONKeys.JSON_MSG_PUBLICKEY_USED)]
            private string publicKeyUsed;
            [DataMember(Name = JSONKeys.JSON_MESSAGE_IS_FILE)]
            public bool IsFile;
            [DataMember(Name = JSONKeys.JSON_FILENAME)]
            public readonly string FileName;
            [DataMember(Name = JSONKeys.JSON_FILE_KEY)]
            public readonly string FileKey;
            [DataMember(Name = JSONKeys.JSON_FILE_MIMETYPE)]
            private string fileMimeType;
            [DataMember(Name = JSONKeys.JSON_FILE_ENCRYPTION_KEY)]
            private string fileEncryptionKey;
            [DataMember(Name = "IS_READ", IsRequired = false)]
            public bool IsRead = true;
            [DataMember(Name = JSONKeys.JSON_MSG_SM_SIGNATURE, IsRequired = false)]
            private string signature;
            public string Sender
            {
                get
                {
                    return sender;
                }
            }
            public string Receiver
            {
                get
                {
                    return receiver;
                }
            }
            public string Contents
            {
                get
                {
                    return contents;
                }
            }
            public bool Signed
            {
                get
                {
                    return signature != null;
                }
            }
            public long UnixTimestamp
            {
                get
                {
                    return (long)timestamp;
                }

            }
            public DateTime Timestamp
            {
                get
                {
                    DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    epoch = epoch.AddSeconds(timestamp);
                    epoch += TimeZoneInfo.Local.GetUtcOffset(epoch);
                    return epoch;
                }
            }
            public Message(string contents, string sender, string receiever, double timeDiff)
            {
                this.contents = contents;
                this.sender = sender;
                this.timestamp = (DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0))).TotalSeconds+timeDiff;
                this.receiver = receiever;
            }
            public bool VerifySignature(string secret)
            {
                return signature!=null && Signatures.VerifySignature(secret, publicKeyCalculated, signature);
            }
            public string TimestampString(string format)
            {
                return this.Timestamp.ToString(format);
            }
            public void UserRenamed(string oldUsername, string newUsername)
            {
                if (sender == oldUsername) sender = newUsername;
                else if (receiver == oldUsername) receiver = newUsername;
            }
            public static Dictionary<string, string> GeneratePOSTParameters(string contents, string peer, string peerKey, long timeDiff, string signatureSecret=null)
            {
                var timeStamp = DateTime.UtcNow;
                timeStamp = timeStamp.AddSeconds(timeDiff);
                long timeDifference = (long)(timeStamp - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
                string timeStampString = timeDifference.ToString();
                while(timeStampString.Length<18)
                {
                    timeStampString = "0" + timeStampString;
                }
                contents += timeStampString;
                byte[] plaintext = TextManipulation.EncodeUTF(contents);
                string messagePrivateKey = DiffieHellman.generate_DH_Private();
                string publicKey = DiffieHellman.calculate_DH_Public(messagePrivateKey);
                var aesKey = DiffieHellman.calculate_DH_AES(peerKey, messagePrivateKey);
                var ciphertext = AES.Encrypt(plaintext, aesKey);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters[POSTKeys.POST_MSG_PEER] = peer;
                parameters[POSTKeys.POST_MSG_CONTENTS] = ciphertext;
                parameters[POSTKeys.POST_MSG_PUBLCIKEY_CALCULATED] = publicKey;
                parameters[POSTKeys.POST_MSG_PUBLICKEY_USED] = peerKey;
                if(signatureSecret!=null)
                {
                    parameters[POSTKeys.POST_MSG_SM_SIGNATURE] = Signatures.GenerateSignature(signatureSecret, publicKey);
                }
                return parameters;
            }
            public static string GenerateFileUploadJSON(byte[] contents, string peerKey, 
                long timeDiff)
            {
                var timeStamp = DateTime.UtcNow;
                timeStamp = timeStamp.AddSeconds(timeDiff);
                long timeDifference = (long)(timeStamp - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
                string timeStampString = timeDifference.ToString();
                while (timeStampString.Length < 18)
                {
                    timeStampString = "0" + timeStampString;
                }
                byte[] timeStampBytes = TextManipulation.EncodeUTF(timeStampString);
                byte[] plaintext = new byte[contents.Length + timeStampBytes.Length];
                Array.Copy(contents, 0, plaintext, 0, contents.Length);
                Array.Copy(timeStampBytes, 0, plaintext, contents.Length, timeStampBytes.Length);
                string messagePrivateKey = DiffieHellman.generate_DH_Private();
                string publicKey = DiffieHellman.calculate_DH_Public(messagePrivateKey);
                var aesKey = DiffieHellman.calculate_DH_AES(peerKey, messagePrivateKey);
                var ciphertext = AES.Encrypt(plaintext, aesKey);
                var file = new MessageFile(ciphertext, peerKey, publicKey);
                return JSONHandling.SerializeObject(file);
            }
            public static byte[] DecryptFile(string fileContents, Message message)
            {
                MessageFile file = JSONHandling.ParseJSONResponse<MessageFile>(fileContents);
                byte[] key = TextManipulation.Decode64(message.fileEncryptionKey);
                var contents = AES.Decrypt(file.Contents, key);
                if (contents.Length < 18)
                {
                    throw new CryptographyException("Invalid message");
                }
                byte[] timeStampBytes = new byte[18];
                Array.Copy(contents, contents.Length - 18, timeStampBytes, 0, 18);
                string timeStampString = TextManipulation.DecodeUTF(timeStampBytes);
                long timeDifference;
                if (!long.TryParse(timeStampString, out timeDifference))
                {
                    throw new CryptographyException("Bad timestamp");
                }
                if (Math.Abs((message.UnixTimestamp - timeDifference)) > 300)
                {
                    throw new CryptographyException("Bad timestamp");
                }
                byte[] realContents = new byte[contents.Length - 18];
                Array.Copy(contents, 0, realContents, 0, realContents.Length);
                return realContents;
            }
			public void Decrypt(string username, string public1, string private1, string public2, string private2)
			{
                try
                {
                    byte[] aesDecryptionKey;
                    if (public1!=null && public1==publicKeyUsed)
                    {
                        aesDecryptionKey = DiffieHellman.calculate_DH_AES(publicKeyCalculated, private1);
                    }
                    else if (public2 != null && public2 == publicKeyUsed)
                    {
                        aesDecryptionKey = DiffieHellman.calculate_DH_AES(publicKeyCalculated, private2);
                    }
                    else
                    {
                        throw new CryptographyException("Could not find a valid key");
                    }
                    if (!IsFile)
                    {
                        contents = TextManipulation.DecodeUTF(AES.Decrypt(Contents, aesDecryptionKey));
                        if (Contents.Length < 18)
                        {
                            throw new CryptographyException("Invalid message");
                        }
                        string timeStampString = Contents.Substring(Contents.Length - 18);
                        long timeDifference;
                        if (!long.TryParse(timeStampString, out timeDifference))
                        {
                            throw new CryptographyException("Bad timestamp");
                        }
                        if (Math.Abs((timestamp - timeDifference)) > 150)
                        {
                            throw new CryptographyException("Bad timestamp");
                        }
                        contents = contents.Substring(0, Contents.Length - 18);
                    }
                    else
                    {
                        fileEncryptionKey = TextManipulation.Encode64(aesDecryptionKey);
                    }
                }
                catch (CryptographyException e)
                {
                    contents = "Invalid message " + e.Message;
                }
			}
		}
	}
}

