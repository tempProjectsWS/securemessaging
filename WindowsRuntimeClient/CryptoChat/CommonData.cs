﻿using SecureMessagingClient;
using SecureMessagingClient.Messaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;

namespace CryptoChat
{
    public static class CommonData
    {
        public static Session session;
        public static MessageList messages;
        
        public static ConversationPage activeConversationPage = null;
        public static bool unreadMessagesExist = false;
        public static void PopulateConversationsList(ObservableCollection<ConversationListItem> conversationListItems)
        {
            unreadMessagesExist = false;
            HashSet<string> seenPeers = new HashSet<string>();
            conversationListItems.Clear();
            if (messages == null)
            {
                messages = new MessageList();
            }
            for (int i = 0; i < CommonData.messages.Length; i++)
            {
                Message message = CommonData.messages[i];
                unreadMessagesExist = unreadMessagesExist || (!message.IsRead);
                string peer;
                if (message.Sender == session.Username) peer = message.Receiver;
                else peer = message.Sender;
                if (seenPeers.Contains(peer)) continue;
                seenPeers.Add(peer);
                conversationListItems.Add(new ConversationListItem(message));
            }
        }
        public async static Task DeleteMessagesFromPeer(string peer)
        {
            int originalLength = messages.Length;
            int deletedMessages=0;
            for (int i=0;i<originalLength;i++)
            {
                if (messages[i-deletedMessages].Sender==peer || messages[i-deletedMessages].Receiver==peer)
                {
                    messages.DeleteMessageFromList(i - deletedMessages);
                    deletedMessages++;
                }
            }
            await StoreMessagesAsync();
        }
        public async static Task LoadStoredMessagesAsync()
        {
            messages = MessageList.FromJSON(await DataStorage.GetStoredMessagesAsync(session.Username));
        }
        public async static Task StoreMessagesAsync()
        {
            await DataStorage.StoreMessagesAsync(session.Username, messages.ToJSON());
        }
        public async static Task DeleteStoredMessagesAsync()
        {
            messages.Empty();
            await StoreMessagesAsync();
        }
        public class ResourceGetter
        {
            private static ResourceLoader resourceGetter = ResourceLoader.GetForViewIndependentUse();
            public string this[string Name]
            {
                get
                {
                    string resourceValue = resourceGetter.GetString(Name);
                    return resourceValue;
                }
            }
        }
        
        public static ResourceGetter Resources = new ResourceGetter();
        public async static Task MarkAsRead(string peerName=null)
        {
            if (messages == null) return;
            for (int i = 0; i < messages.Length;i++)
            {
                if (peerName == null || messages[i].Sender==peerName || messages[i].Receiver==peerName)
                {
                    messages[i].IsRead = true;
                }
            }
            await StoreMessagesAsync();
        }
        public async static Task MarkAsUnRead(string peerName)
        {
            for (int i = 0; i < messages.Length; i++)
            {
                if (messages[i].Sender == peerName || messages[i].Receiver == peerName)
                {
                    messages[i].IsRead = false;
                    break;
                }
            }
            await StoreMessagesAsync();
        }
    }
}
