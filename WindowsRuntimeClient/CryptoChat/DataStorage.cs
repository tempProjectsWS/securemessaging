﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Security.Credentials;
using Windows.Storage;

namespace CryptoChat
{
    class DataStorage
    {
        public static List<string[]> GetNotifierCredentials()
        {
            List<string[]> returnedCredentials = new List<string[]>();
            var vault = new PasswordVault();
            var credentials = vault.RetrieveAll();
            if (credentials.Count == 0) return returnedCredentials;
            foreach (var credential in credentials)
            {
                if (credential.Resource == "NOTCRED")
                {
                    credential.RetrievePassword();
                    string[] credentialData = { credential.UserName, credential.Password };
                    returnedCredentials.Add(credentialData);
                }
            }
            return returnedCredentials;
        }
        public static void SetNotifierCredential(string username, string password)
        {
            var vault = new PasswordVault();
            var credentials = vault.RetrieveAll();
            foreach (var credential in credentials)
            {
                if (credential.Resource == "NOTCRED" && credential.UserName == username)
                {
                    vault.Remove(credential);
                }
            }
            if (password != null)
            {
                vault.Add(new PasswordCredential("NOTCRED", username, password));
            }
        }
        public static string[] StoredCredentials
        {
            get
            {
                var vault = new PasswordVault();
                var credentials = vault.RetrieveAll();
                if (credentials.Count == 0) return null;
                foreach (var credential in credentials)
                {
                    if (credential.Resource == "CRED")
                    {
                        string[] returnedData = new string[2];
                        credential.RetrievePassword();
                        returnedData[0] = credential.UserName;
                        returnedData[1] = credential.Password;
                        return returnedData;
                    }
                }
                return null;
            }
            set
            {
                var vault = new PasswordVault();
                var credentials = vault.RetrieveAll();
                foreach (var credential in credentials)
                {
                    if (credential.Resource == "CRED") vault.Remove(credential);
                }
                if (value != null)
                {
                    vault.Add(new PasswordCredential("CRED", value[0], value[1]));
                }
            }
        }
        public async static Task<string> GetStoredMessagesAsync(string username)
        {
            var folder = ApplicationData.Current.LocalFolder;
            try
            {
                var file = await folder.GetFileAsync(username);
                string messages = await FileIO.ReadTextAsync(file);
                return messages;
            }
            catch
            {
                return null;
            }
        }
        public static bool ApplicationRunning
        {
            get
            {
                return getStoredCredential("APPRUNNING", "_app_") != null &&
                getStoredCredential("APPRUNNING", "_app_") == "YES";
            }
            set
            {
                if (value)
                {
                    storeCredential("APPRUNNING", "_app_", "YES");
                }
                else
                {
                    storeCredential("APPRUNNING", "_app_", "NO");
                }
            }
        }
        public static string GetStoredToken(string username)
        {
            return getStoredCredential("TOKEN", username);
        }
        public static string GetStoredPublicKey(string username)
        {
            return getStoredCredential("PUBLICKEY", username);
        }
        public static string GetStoredPrivateKey(string username)
        {
            return getStoredCredential("PRIVATEKEY", username);
        }
        public static async Task StoreMessagesAsync(string username, string messages)
        {
            var folder = ApplicationData.Current.LocalFolder;
            if (messages == null || messages == "")
            {
                try
                {
                    var file = await folder.GetFileAsync(username);
                    await file.DeleteAsync(StorageDeleteOption.PermanentDelete);
                }
                catch { }
            }
            else
            {
                try
                {
                    var file = await folder.CreateFileAsync(username, CreationCollisionOption.ReplaceExisting);
                    await FileIO.WriteTextAsync(file, messages);
                }
                catch { }
            }
        }
        public static void StoreToken(string username, string token)
        {
            storeCredential("TOKEN", username, token);
        }
        public static void StorePublicKey(string username, string key)
        {
            storeCredential("PUBLICKEY", username, key);
        }
        public static void StorePrivateKey(string username, string key)
        {
            storeCredential("PRIVATEKEY", username, key);
        }
        private static string getStoredCredential(string resource, string username)
        {
            var vault = new PasswordVault();
            var credentials = vault.RetrieveAll();
            if (credentials.Count == 0) return null;
            foreach (var credential in credentials)
            {
                if (credential.UserName == resource + username)
                {
                    credential.RetrievePassword();
                    return credential.Password;
                }
            }
            return null;
        }
        private static void storeCredential(string resource, string username, string value)
        {
            var vault = new PasswordVault();
            var credentials = vault.RetrieveAll();
            foreach (var credential in credentials)
            {
                if (credential.UserName == resource + username) vault.Remove(credential);
            }
            if (value != null)
            {
                vault.Add(new PasswordCredential(resource, resource + username, value));
            }
        }
    }
}
