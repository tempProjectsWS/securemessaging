﻿using SecureMessagingClient;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace CryptoChat
{
    public sealed partial class SetPasswordPage : Page
    {
        private Session session;

        public SetPasswordPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            session = e.Parameter as Session;
            if (session == null) this.Frame.GoBack();
        }
        private async void setPassword(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password == "" || newPasswordBox.Password == "" ||
                confirmPasswordBox.Password == "")
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["MissingCredentialsErrorText"]);
                return;
            }
            if (passwordBox.Password != session.Password)
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["BadPasswordErrorText"]);
                return;
            }
            if (newPasswordBox.Password != confirmPasswordBox.Password)
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["PasswordsNotMatchingErrorText"]);
                return;
            }
            progressStart();
            try
            {
                await session.SetPasswordAsync(newPasswordBox.Password);
            }
            catch (SecureMessagingException error)
            {
                progressStop();
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                return;
            }
            progressStop();
            await DialogBoxes.ShowMessageBox(CommonData.Resources["SetPasswordSuccessText"]);
        }
        private void progressStart()
        {
            progressIndicator.IsActive = true;
        }
        private void progressStop()
        {
            progressIndicator.IsActive = false;
        }
    }
}
