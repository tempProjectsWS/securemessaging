﻿using System;
using System.IO;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace CryptoChat
{
    public sealed partial class HelpPage : Page
    {
        public HelpPage()
        {
            this.InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            string fileContent;
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(@"ms-appx:///Assets/help.html"));
            using (StreamReader sRead = new StreamReader(await file.OpenStreamForReadAsync()))
                fileContent = await sRead.ReadToEndAsync();
            String textColor = Colors.TextColorHex;
            String backgroundColor = Colors.BackgroundColorHex;
            helpDisplay.NavigateToString(fileContent.Replace("{0}", Colors.TextColorHex).Replace
                                                            ("{1}", Colors.BackgroundColorHex));
        }
    }
}
