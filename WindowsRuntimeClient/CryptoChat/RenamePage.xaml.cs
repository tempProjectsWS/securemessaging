﻿using SecureMessagingClient;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace CryptoChat
{
    public sealed partial class RenamePage : Page
    {
        private Session session;
        public RenamePage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            session = e.Parameter as Session;
            if (session == null) this.Frame.GoBack();
        }
        private void progressStart()
        {
            progressIndicator.IsActive = true;
        }
        private void progressStop()
        {
            progressIndicator.IsActive = false;
        }
        private async void renameUser(object sender, RoutedEventArgs e)
        {
            try
            {
                string newUsername = newUsernameBox.Text;
                string oldUsername = session.Username;
                if (newUsername == "")
                {
                    await DialogBoxes.ShowMessageBox(CommonData.Resources["MissingCredentialsErrorText"]);
                    return;
                }
                progressStart();
                await session.RenameUser(newUsername);
                for(int i=0;i<CommonData.messages.Length;i++)
                {
                    CommonData.messages[i].UserRenamed(oldUsername, newUsername);
                }
                await CommonData.StoreMessagesAsync();
                DataStorage.StoreToken(session.Username, session.Token);
                DataStorage.StorePublicKey(session.Username, session.PublicKey);
                DataStorage.StorePrivateKey(session.Username, session.PrivateKey);
                foreach (var credential in DataStorage.GetNotifierCredentials())
                {
                    if (credential[0] == session.Username)
                    {
                        DataStorage.SetNotifierCredential(oldUsername, null);
                        DataStorage.SetNotifierCredential(newUsername, session.Password);
                    }
                }
                DataStorage.StoreToken(oldUsername, null);
                DataStorage.StorePublicKey(oldUsername, null);
                DataStorage.StorePrivateKey(oldUsername, null);
                await DataStorage.StoreMessagesAsync(oldUsername, null);
                progressStop();
                await DialogBoxes.ShowMessageBox(CommonData.Resources["RenameUserSuccessText"]);
            }
            catch(SecureMessagingException error)
            {
                progressStop();
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                return;
            }
        }
    }
}
