﻿using SecureMessagingClient;
using SecureMessagingClient.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace CryptoChat
{
    public sealed partial class ConversationPage : Page
    {
        private bool actionEnabled;
        private string signatureSecret;
        private int selectedMessage;
        private TextBlock selectedMessageContents;
        private Session session;
        private ObservableCollection<MessageListItem> messageListItems;
        private ObservableCollection<ConversationListItem> conversationListItems;
        private string selectedConversation;
        private bool shiftDown;
        private DispatcherTimer blinkerTimer;

        public ConversationPage()
        {
            this.InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            CommonData.activeConversationPage = this;
            DataStorage.ApplicationRunning = true;
            actionEnabled = true;
            session = CommonData.session;
            if (session == null) this.Frame.GoBack();
            signatureSecret = null;
            shiftDown = false;
            await CommonData.LoadStoredMessagesAsync();
            messageListItems = new ObservableCollection<MessageListItem>();
            conversationListItems = new ObservableCollection<ConversationListItem>();
            conversationsList.DataContext = conversationListItems;
            display.DataContext = messageListItems;
            unreadIndicator.Foreground = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
            blinkerTimer = new DispatcherTimer();
            blinkerTimer.Interval = TimeSpan.FromMilliseconds(500);
            blinkerTimer.Tick += blinkUnreadIndicator;
            blinkerTimer.Stop();
            await refreshConversationList();
            foreach (var credential in DataStorage.GetNotifierCredentials())
            {
                if (credential[0] == session.Username)
                {
                    toggleNotificationsSwitch.IsOn = true;
                    break;
                }
            }
            if (conversationListItems.Count == 0) startNewConversation();
            else selectPeer(conversationListItems[0].Name);
            if (e.NavigationMode == NavigationMode.Forward || e.NavigationMode == NavigationMode.New)
            {
                await refreshMessages();
            }
            closeSideMenu();
        }

        protected override async void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                try
                {
                    if (session != null) await session.LogoutAsync();
                }
                catch (SecureMessagingException) { }
                DataStorage.ApplicationRunning = false;
            }
            CommonData.activeConversationPage = null;
        }

        private async void selectPeer(string selectedPeer)
        {
            peerName.Visibility = Visibility.Visible;
            peerBox.Visibility = Visibility.Collapsed;
            peerBox.IsEnabled = false;
            peerBox.Text = selectedPeer;
            peerName.Text = selectedPeer;
            refreshButton.IsEnabled = true;
            await CommonData.MarkAsRead(selectedPeer);
            displayMessages();
            closeSideMenu();
            await refreshConversationList();
        }
        private void startNewConversation(object sender=null, RoutedEventArgs e=null)
        {
            peerName.Visibility = Visibility.Collapsed;
            peerBox.Visibility = Visibility.Visible;
            peerBox.IsEnabled = true;
            peerBox.Text = "";
            peerName.Text = "";
            sideMenu.IsPaneOpen = false;
            closeSideMenu();
            displayMessages();
        }
        private void displayMessages()
        {
            progressStop();
            if (CommonData.messages == null) return;
            messageListItems.Clear();
            if (peerName.Text == "") return;
            for (int i = CommonData.messages.Length - 1; i >= 0; i--)
            {
                if (CommonData.messages[i].Sender == peerName.Text || CommonData.messages[i].Receiver == peerName.Text)
                {
                    Message current = CommonData.messages[i];
                    messageListItems.Add(new MessageListItem(current, i, current.Sender == CommonData.session.Username, display));
                }
            }
            if (display.Items.Count > 0)
            {
                display.UpdateLayout();

                display.ScrollIntoView(display.Items[display.Items.Count - 1]);
            }
        }

        private async Task refreshConversationList()
        {
            await CommonData.MarkAsRead(peerBox.Text);
            CommonData.PopulateConversationsList(conversationListItems);
            if (CommonData.unreadMessagesExist)
            {
                blinkerTimer.Start();
            }
            else
            {
                blinkerTimer.Stop();
                IndicatorOn = false;
            }
        }
        private bool IndicatorOn
        {
            set
            {
                if (value && actionEnabled)
                {
                    unreadIndicator.Foreground = indicatorOnBrush;
                }
                else unreadIndicator.Foreground = indicatorOffBrush;
            }
            get
            {
                return unreadIndicator.Foreground == indicatorOnBrush;
            }
        }
        private void blinkUnreadIndicator(object sender, object e)
        {
            IndicatorOn = !IndicatorOn;
        }
        private void progressStart()
        {
            actionEnabled = false;
            progressIndicator.IsActive = true;
        }
        private void progressStop()
        {
            actionEnabled = true;
            progressIndicator.IsActive = false;
        }

        private void openConversation(object sender, ItemClickEventArgs e)
        {
            ConversationListItem i = (ConversationListItem)e.ClickedItem;
            selectPeer(i.Name);
            closeSideMenu();
        }

        private void toggleSideMenu(object sender=null, RoutedEventArgs e=null)
        {
            if (sideMenu.IsPaneOpen)
            {
                closeSideMenu();
            }
            else
            {
                openSideMenu();
            }
        }
        private void closeSideMenu()
        {
            sideMenu.IsPaneOpen = false;
            conversationsList.Visibility = Visibility.Collapsed;
            settingsPane.Visibility = Visibility.Collapsed;
        }
        private void openSideMenu()
        {
            sideMenu.IsPaneOpen = true;
            conversationsList.Visibility = Visibility.Visible;
        }

        private async void deleteMessage(object sender, RoutedEventArgs e)
        {
            await DialogBoxes.AskForConfirmation(CommonData.Resources["ConfirmDeleteMessageMessage"], () => { deleteMessageConfirmed(selectedMessage); });
        }

        private async void deleteMessageConfirmed(int messageId)
        {
            CommonData.messages.DeleteMessageFromList(messageId);
            await CommonData.StoreMessagesAsync();
            displayMessages();
        }

        private async void checkMessageSignature(object sender, RoutedEventArgs e)
        {
            progressStart();
            string secret = await DialogBoxes.AskForInput(CommonData.Resources["SignatureSecretInputDialogTitle"],
                                                          CommonData.Resources["SignatureSecretInputDialogText"]);
            if (secret == null)
            {
                progressStop();
                return;
            }
            if (await Task.Run(() => { return CommonData.messages[selectedMessage].VerifySignature(secret); }))
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["SignatureOkText"]);
            }
            else
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["SignatureBadText"]);
            }
            progressStop();
        }

        private async void sendMessage(object sender, RoutedEventArgs e)
        {
            if (!actionEnabled) return;
            string contents = messageBox.Text.Trim();
            if (peerBox.Text == "" || contents == "")
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["MissingRecipientOrMessageErrorText"]);
                return;
            }
            if (peerBox.Text == session.Username)
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["CantSendMessageToSelfErrorText"]);
                return;
            }
            progressStart();
            try
            {
                var sentMessage = await session.SendMessageAsync(peerBox.Text, contents, signatureSecret: signatureSecret);
                CommonData.messages.AddMessage(sentMessage);
                await CommonData.StoreMessagesAsync();
                messageBox.Text = "";
                progressStop();
                selectPeer(peerBox.Text);
                refreshMessages(null, null);
            }
            catch (SecureMessagingException error)
            {
                progressStop();
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
            }
        }

        private void messageBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                if (!shiftDown) sendMessage(null, null);
            }
            else if (e.Key == VirtualKey.Shift)
            {
                shiftDown = false;
            }
        }
        private void messageBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Shift)
            {
                shiftDown = true;
            }
        }

        private void messageListItemLongClick(object sender, HoldingRoutedEventArgs e)
        {
            StackPanel clickedItemContents = (StackPanel)sender;
            TextBlock messageId = (TextBlock)clickedItemContents.Children[0];
            selectedMessageContents = (TextBlock)clickedItemContents.Children[1];
            openMessageContextMenu(sender, int.Parse(messageId.Text));
            e.Handled = true;
        }

        private void openMessageContextMenu(object sender, int messageId)
        {
            selectedMessage = messageId;
            if (CommonData.messages[messageId].Signed)
            {
                messageMenu.Items[1].Visibility = Visibility.Visible;
            }
            else messageMenu.Items[1].Visibility = Visibility.Collapsed;
            messageMenu.ShowAt((FrameworkElement)sender);
        }

        private void messageListItemRightClick(object sender, RightTappedRoutedEventArgs e)
        {
            StackPanel clickedItemContents = (StackPanel)sender;
            TextBlock messageId = (TextBlock)clickedItemContents.Children[0];
            selectedMessageContents = (TextBlock)clickedItemContents.Children[1];
            openMessageContextMenu(sender, Int32.Parse(messageId.Text));
            e.Handled = true;
        }
        private void messageClicked(object sender, TappedRoutedEventArgs e)
        {
            StackPanel clickedItemContents = (StackPanel)sender;
            TextBlock messageId = (TextBlock)clickedItemContents.Children[0];
            downloadMessage(Int32.Parse(messageId.Text));
            e.Handled = true;
        }

        private async void downloadMessage(int messageId)
        {
            if (!CommonData.messages[messageId].IsFile) return;
            try
            {
                progressStart();
                var fileContents = await session.DownloadFileAsync(CommonData.messages[messageId]);
                saveFile(CommonData.messages[messageId].FileName, fileContents);
                progressStop();
            }
            catch (SecureMessagingException error)
            {
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                progressStop();
            }

        }

        public async void refreshMessages(object sender, RoutedEventArgs e)
        {
            await refreshMessages();
        }
        public async Task refreshMessages()
        {
            if (!actionEnabled) return;
            progressStart();
            try
            {
                if (CommonData.messages == null)
                {
                    CommonData.messages = new MessageList();
                }
                CommonData.messages.Append(await session.GetMessagesAsync());
                await refreshConversationList();
                displayMessages();
            }
            catch (SecureMessagingException error)
            {
                progressStop();
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
            }
        }

        private async void sendFile(object sender, RoutedEventArgs e)
        {
            if (peerBox.Text == "")
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["MissingRecipientErrorText"]);
                return;
            }
            if (peerBox.Text == session.Username)
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["CantSendMessageToSelfErrorText"]);
                return;
            }
            FileOpenPicker picker = new FileOpenPicker();
            picker.SuggestedStartLocation = PickerLocationId.Downloads;
            picker.FileTypeFilter.Add("*");
            var file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                await uploadFile(file);
            }
        }
        public async void saveFile(string fileName, byte[] fileContents)
        {
            FileSavePicker picker = new FileSavePicker();
            picker.SuggestedStartLocation = PickerLocationId.Downloads;
            string extension, name;
            extension = name = "";
            bool hasExtension = false;
            for (int i = 0; i < fileName.Length; i++)
            {
                if (hasExtension)
                {
                    extension += fileName[i];
                }
                else
                {
                    if (fileName[i] == '.')
                    {
                        extension += fileName[i];
                        hasExtension = true;
                    }
                    else
                    {
                        name += fileName[i];
                    }
                }
            }
            if (extension == ".") extension += "noex";
            if (extension == "") extension = ".noex";
            picker.FileTypeChoices.Add(CommonData.Resources["SaveDialogFileTypeText"], new List<string>() { extension });
            picker.SuggestedFileName = name;
            var file = await picker.PickSaveFileAsync();
            if (file != null)
            {
                await FileIO.WriteBytesAsync(file, fileContents);
            }
        }

        private async Task uploadFile(StorageFile file)
        {
            try
            {
                progressStart();
                byte[] fileContents = (await FileIO.ReadBufferAsync(file)).ToArray();
                var sentMessage = await session.UploadFileAsync(file.Name, fileContents, peerBox.Text, file.ContentType);
                CommonData.messages.AddMessage(sentMessage);
                await CommonData.StoreMessagesAsync();
                progressStop();
                selectPeer(peerBox.Text);
                await refreshMessages();
            }
            catch (SecureMessagingException error)
            {
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                progressStop();
                return;
            }
        }
        private async void toggleMessageSigning(object sender, RoutedEventArgs e)
        {
            progressStart();
            if (toggleMessageSigningSwitch.IsOn)
            {
                signatureSecret = await DialogBoxes.AskForInput(CommonData.Resources["SignatureSecretInputDialogTitle"],
                                                                CommonData.Resources["SignatureSecretInputDialogText"]);
                if (signatureSecret == null)
                {
                    toggleMessageSigningSwitch.IsOn = false;
                }
            }
            else
            {
                signatureSecret = null;
            }
            progressStop();
        }

        private void openSetPasswordPage(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SetPasswordPage), session);
        }

        private async void unauthorizeClient(object sender, RoutedEventArgs e)
        {
            await DialogBoxes.AskForConfirmation(CommonData.Resources["ConfirmUnauthorizeMessageText"], unauthorizeConfirmed);
        }

        private async void unauthorizeConfirmed()
        {
            try
            {
                await session.UnauthorizeAsync();
            }
            catch (SecureMessagingException error)
            {
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                return;
            }
            await CommonData.DeleteStoredMessagesAsync();
            DataStorage.SetNotifierCredential(session.Username, null);
            DataStorage.StoreToken(session.Username, null);
            DataStorage.StorePublicKey(session.Username, null);
            DataStorage.StorePrivateKey(session.Username, null);
            this.Frame.GoBack();
        }

        private void enableBackgroundNotifier()
        {
            
            DataStorage.SetNotifierCredential(session.Username, session.Password);
        }
        private void disableBackgroundNotifier()
        {
            DataStorage.SetNotifierCredential(session.Username, null);
        }

        private void pageRoot_КеyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.F5) refreshMessages(null, null);
        }

        private void sideMenu_PaneClosed(SplitView sender, object args)
        {
            closeSideMenu();
        }

        private void openConversationContextMenu(object sender, string peerName)
        {
            selectedConversation = peerName;
            conversationContextMenu.ShowAt((FrameworkElement)sender);
        }

        private void conversationListItemRightClick(object sender, RoutedEventArgs e)
        {
            StackPanel clickedItemContents = (StackPanel)sender;
            TextBlock peerName = (TextBlock)clickedItemContents.Children[0];
            openConversationContextMenu(sender, peerName.Text);
            if((e as HoldingRoutedEventArgs)!=null)
            {
                (e as HoldingRoutedEventArgs).Handled = true;
            }else if((e as RightTappedRoutedEventArgs)!=null)
            {
                (e as RightTappedRoutedEventArgs).Handled = true;
            }
        }

        private async void deleteConversation(object sender, RoutedEventArgs e)
        {
            await DialogBoxes.AskForConfirmation(CommonData.Resources["ConfirmDeleteConversationMessageText"],
                async () => {
                    await CommonData.DeleteMessagesFromPeer(selectedConversation);
                    await CommonData.StoreMessagesAsync();
                    await refreshConversationList();
                    startNewConversation();
                });
        }

        private async void markConversationAsRead(object sender, RoutedEventArgs e)
        {
            await CommonData.MarkAsRead(selectedConversation);
            await refreshConversationList();
        }

        private async void markConversationAsUnRead(object sender, RoutedEventArgs e)
        {
            await CommonData.MarkAsUnRead(selectedConversation);
            await refreshConversationList();
        }

        private async void toggleSettingsPane(object sender, RoutedEventArgs e)
        {
            if(settingsPane.Visibility==Visibility.Collapsed)
            {
                openSideMenu();
                settingsPane.Visibility = Visibility.Visible;
                conversationsList.Visibility = Visibility.Collapsed;
                settingsPaneEntrance.Begin();
            }
            else
            {
                settingsPaneExit.Begin();
                await Task.Delay(200);
                settingsPane.Visibility = Visibility.Collapsed;
                conversationsList.Visibility = Visibility.Visible;
            }
        }

        private void toggleNotifications(object sender, RoutedEventArgs e)
        {
            if (toggleNotificationsSwitch.IsOn) enableBackgroundNotifier();
            else disableBackgroundNotifier();
        }

        private void openRenamePage(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RenamePage), session);
        }
    }
}
