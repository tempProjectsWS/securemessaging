﻿using SecureMessagingClient;
using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace CryptoChat
{
    public sealed partial class ForgotPasswordPage : Page
    {
        public ForgotPasswordPage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void sendRecoveryRequest(object sender, RoutedEventArgs e)
        {
            var username = usernameBox.Text.Trim();
            if (username == "" || emailBox.Text == "")
            {
                await (new MessageDialog(CommonData.Resources["MissingCredentialsErrorText"])).ShowAsync();
                return;
            }
            progressStart();
            try
            {
                await UserManagement.RequestPasswordResetAsync(username, emailBox.Text);
            }
            catch (SecureMessagingException error)
            {
                progressStop();
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                return;
            }
            await new MessageDialog(CommonData.Resources["RecoveryRequestSuccessText"]).ShowAsync();
            progressStop();
        }
        private void progressStart()
        {
            progressIndicator.IsActive = true;
        }
        private void progressStop()
        {
            progressIndicator.IsActive = false;
        }
    }
}
