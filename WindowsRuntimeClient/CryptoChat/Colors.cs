﻿using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace CryptoChat
{
    static class Colors
    {

        public static string BackgroundColorHex
        {
            get
            {
                return BackgroundColor.Color.ToString().Substring(3);
            }
        }
        public static string TextColorHex
        {
            get
            {
                return TextColor.Color.ToString().Substring(3);
            }
        }
        public static SolidColorBrush BackgroundColor
        {
            get
            {
                if(Application.Current.RequestedTheme==ApplicationTheme.Dark)
                {
                    return new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0x00, 0x00));
                }
                else
                { 
                    return new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF));
                }
            }
        }

        private static Color darkerColor(Color oldColor)
        {
            Color newColor = new Color();
            newColor.A = 255;
            if (oldColor.R > 80) newColor.R = (byte)(oldColor.R - 32); else newColor.R = 0;
            if (oldColor.G > 80) newColor.G = (byte)(oldColor.G - 32); else newColor.G = 0;
            if (oldColor.B > 80) newColor.B = (byte)(oldColor.B - 32); else newColor.B = 0;
            return newColor;
        }

        public static SolidColorBrush AccentColor
        {
            get
            {
                try
                {
                    return new SolidColorBrush((Color)App.Current.Resources["SystemAccentColor"]);
                }
                catch
                {
                    return new SolidColorBrush(Color.FromArgb(0xFF, 0x87, 0xCE, 0xEB));
                }
            }
        }

        public static SolidColorBrush DarkAccentColor
        {
            get
            {
                try
                {
                    return new SolidColorBrush(darkerColor(AccentColor.Color));
                }
                catch
                {

                    return new SolidColorBrush(Color.FromArgb(0xFF, 0x67, 0xAE, 0xCB));
                }
            }
        }

        public static SolidColorBrush TextColor
        {
            get
            {
                if (Application.Current.RequestedTheme == ApplicationTheme.Dark)
                {
                    return new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF));
                }
                else
                {
                    return new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0x00, 0x00));
                }
            }
        }
    }
}
