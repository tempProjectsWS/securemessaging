﻿using SecureMessagingClient;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;


namespace CryptoChat
{
    public sealed partial class RegistrationPage : Page
    {
        public RegistrationPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
        }

        private async void registerUser(object sender, RoutedEventArgs e)
        {
            if (usernameBox.Text.Trim() == "" || emailBox.Text == "" || passwordBox.Password == "" || confirmPasswordBox.Password == "")
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["MissingCredentialsErrorText"]);
                return;
            }
            if (passwordBox.Password != confirmPasswordBox.Password)
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["PasswordsNotMatchingErrorText"]);
                return;
            }
            progressStart();
            try
            {
                await UserManagement.RegisterNewAccountAsync(usernameBox.Text.Trim(), emailBox.Text, passwordBox.Password);
            }
            catch (SecureMessagingException error)
            {
                progressStop();
                DialogBoxes.ShowMessageBox(CommonData.Resources["GeneralErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                return;
            }
            progressStop();
            await DialogBoxes.ShowMessageBox(CommonData.Resources["RegistrationSuccessText"]);
        }
        private void progressStart()
        {
            progressIndicator.IsActive = true;
        }
        private void progressStop()
        {
            progressIndicator.IsActive = false;
        }
    }
}
