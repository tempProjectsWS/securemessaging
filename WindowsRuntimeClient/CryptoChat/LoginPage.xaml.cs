﻿using SecureMessagingClient;
using System;
using Windows.Networking.PushNotifications;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

namespace CryptoChat
{
    public sealed partial class LoginPage : Page
    {
        private PushNotificationChannel channel;

        public LoginPage()
        {
            this.InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (DataStorage.StoredCredentials != null)
            {
                usernameBox.Text = DataStorage.StoredCredentials[0];
                passwordBox.Password = DataStorage.StoredCredentials[1];
                rememberCredentialsCheckbox.IsChecked = true;
            }
            if (e.NavigationMode == NavigationMode.New ||
                e.NavigationMode == NavigationMode.Forward)
            {
                channel = await WNSHandling.RequestWNSUri();
                await WNSHandling.RegisterNotifier();
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
                SystemNavigationManager.GetForCurrentView().BackRequested -= handleBack;
                SystemNavigationManager.GetForCurrentView().BackRequested += handleBack;
            }
        }

        private void handleBack(object sender, BackRequestedEventArgs e)
        {
            {
                if (this.Frame.CanGoBack)
                {
                    e.Handled = true;
                    this.Frame.GoBack();
                }
                else e.Handled = false;
            }
        }

        private async void logIn(object sender, RoutedEventArgs e)
        {
            var username = usernameBox.Text.Trim();
            if (username == "" || passwordBox.Password == "")
            {
                await DialogBoxes.ShowMessageBox(CommonData.Resources["MissingCredentialsErrorText"]);
                return;
            }
            progressStart();
            Session newSession;
            try
            {
                string token = DataStorage.GetStoredToken(username);
                string publicKey = DataStorage.GetStoredPublicKey(username);
                string privateKey = DataStorage.GetStoredPrivateKey(username); ;
                if (token == null || publicKey == null || privateKey == null)
                {
                    string[] credentials = await Session.RequestNewTokenAsync(username, passwordBox.Password);
                    token = credentials[0];
                    publicKey = credentials[1];
                    privateKey = credentials[2];
                }
                newSession = await Session.StartSessionAsync(username, passwordBox.Password, token, publicKey, privateKey);
                DataStorage.StoreToken(username, newSession.Token);
                DataStorage.StorePublicKey(username, newSession.PublicKey);
                DataStorage.StorePrivateKey(username, newSession.PrivateKey);
                if (channel != null)
                {
                    await newSession.RegisterWnsAsync(channel.Uri);
                }

            }
            catch (SecureMessagingException error)
            {
                progressStop();
                if (error.Message==SecureMessagingClient.HTTP.Responses.RESPONSE_UNAUTHORIZED)
                {
                    DataStorage.StoreToken(username, null);
                    DataStorage.StorePublicKey(username, null);
                    DataStorage.StorePrivateKey(username, null);
                    return;
                }
                DialogBoxes.ShowMessageBox(CommonData.Resources["LoginFailedErrorText"] + ResponseTranslator.UserFriendlyErrorText(error.Message));
                return;
            }
            progressStop();
            if ((bool)rememberCredentialsCheckbox.IsChecked)
            {
                string[] credentials = { username, passwordBox.Password };
                DataStorage.StoredCredentials = credentials;
            }
            CommonData.session = newSession;
            this.Frame.Navigate(typeof(ConversationPage));
        }
        private void forgetCredentials(object sender, RoutedEventArgs e)
        {
            DataStorage.StoredCredentials = null;
        }

        private void openRegistrationPage(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RegistrationPage));
        }

        private void openForgotPasswordPage(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ForgotPasswordPage));
        }
        private void openHelpPage(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(HelpPage));
        }
        private void progressStart()
        {
            loginButton.IsEnabled = true;
            progressIndicator.IsActive = true;
        }
        private void progressStop()
        {
            loginButton.IsEnabled = true;
            progressIndicator.IsActive = false;
        }

        private void usernameBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                passwordBox.Focus(FocusState.Keyboard);
            }
        }

        private void passwordBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                logIn(null, null);
            }
        }
    }
}
