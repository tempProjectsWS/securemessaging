﻿using SecureMessagingClient.Messaging;
using Windows.UI.Text;
using Windows.UI.Xaml;

namespace CryptoChat
{
    public class ConversationListItem
    {
        private Message message;
        public ConversationListItem(Message message)
        {
            this.message = message;
        }
        public FontWeight Weight
        {
            get
            {
                if (message.IsRead)
                {
                    return FontWeights.Normal;
                }
                else
                {
                    return FontWeights.SemiBold;
                }
            }
        }
        public string Name
        {
            get
            {
                if (message.Sender == CommonData.session.Username) return message.Receiver;
                else return message.Sender;
            }
        }
        public string Message
        {
            get
            {
                string content;
                if (message.IsFile)
                {
                    content = message.FileName;
                }
                else
                {
                    content = message.Contents.Split(new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries)[0];
                }
                if (content.Length > 20) content = content.Substring(0, 20);
                return content;
            }
        }
        public string Date
        {
            get
            {
                return message.TimestampString("HH:mm ddd");
            }
        }
        public Visibility Visible
        {
            get
            {
                if (!message.IsRead)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }
    }
}
