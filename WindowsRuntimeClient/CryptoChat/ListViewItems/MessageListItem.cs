﻿using SecureMessagingClient.Messaging;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace CryptoChat
{
    public class MessageListItem
    {
        private string contents;
        private bool sent;
        ListView parent;
        Message message;
        private int messageId;
        public MessageListItem(Message message, int messageId, bool sent, ListView parent)
        {
            if (message.IsFile) this.contents = message.FileName;
            else this.contents = message.Contents;
            this.sent = sent;
            this.parent = parent;
            this.message = message;
            this.messageId = messageId;
        }
        public Brush BackgroundColor
        {
            get
            {
                if (sent) return Colors.DarkAccentColor;
                else return Colors.AccentColor;
            }
        }
        public Thickness Margin
        {
            get
            {
                Thickness thickness = new Thickness();
                thickness.Top = thickness.Bottom = 10;
                thickness.Left = thickness.Right = 10;
                if (sent) thickness.Left = parent.ActualWidth / 5;
                else thickness.Right = parent.ActualWidth / 5;
                return thickness;
            }
        }
        public Brush TextColor
        {
            get
            {
                return Colors.TextColor;
            }
        }
        public string Contents
        {
            get
            {
                return contents;
            }
        }
        public string Date
        {
            get
            {
                return message.TimestampString("f");
            }
        }
        public string MessageID
        {
            get
            {
                return this.messageId.ToString();
            }
        }
        public Visibility FileIndicatorVisible
        {
            get
            {
                if (message.IsFile) return Visibility.Visible;
                else return Visibility.Collapsed;
            }
        }
        public Visibility SignatureIndicatorVisible
        {
            get
            {
                if (message.Signed) return Visibility.Visible;
                else return Visibility.Collapsed;
            }
        }
        public string SignatureIndicatorTooltip
        {
            get
            {
                return CommonData.Resources["SignatureIndicatorTooltip"];
            }
        }
    }
}
