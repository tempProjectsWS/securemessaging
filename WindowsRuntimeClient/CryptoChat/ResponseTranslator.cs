﻿using SecureMessagingClient.HTTP;
using System.Collections.Generic;

namespace CryptoChat
{
    class ResponseTranslator
    {
        public static string UserFriendlyErrorText(string response)
        {
            Dictionary<string, string> responseToDescriptionMapping = new Dictionary<string, string>();
            responseToDescriptionMapping[Responses.RESPONSE_USER_BAD_USERNAME] = CommonData.Resources["BadUsernameErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_BAD_PASSWORD] = CommonData.Resources["BadPasswordErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_NOT_LOGGED_IN] = CommonData.Resources["NotLoggedInErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_NOT_VERIFIED] = CommonData.Resources["NotVerifiedErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_MSG_NO_PEER] = CommonData.Resources["BadPeerErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_UNHANDLED_ERROR] = CommonData.Resources["UnhandledErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_NETWORK_ERROR] = CommonData.Resources["NetworkErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_NAME_EXISTS] = CommonData.Resources["UsernameExistsErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_EMAIL_EXISTS] = CommonData.Resources["EmailExistsErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_NOT_VERIFIED] = CommonData.Resources["NotVerifiedErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_BAD_EMAIL] = CommonData.Resources["BadEmailErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_USER_BAD_TOKEN] = CommonData.Resources["BadTokenErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_ALREADY_AUTHORIZED] = CommonData.Resources["AlreadyAuthorizedErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_UNAUTHORIZED] = CommonData.Resources["UnauthorizedErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_PEER_INACTIVE] = CommonData.Resources["PeerInactiveErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_BAD_FILE_KEY] = CommonData.Resources["BadFileKeyErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_TIMEOUT] = CommonData.Resources["TimeoutErrorText"];
            responseToDescriptionMapping[Responses.RESPONSE_TIMEDIFF_TOO_BIG] = CommonData.Resources["TimediffTooBigErrorText"];
            if (responseToDescriptionMapping.ContainsKey(response))
            {
                return responseToDescriptionMapping[response];
            }
            else return CommonData.Resources["UnknownErrorText"] + " " + response;
        }
    }
}
