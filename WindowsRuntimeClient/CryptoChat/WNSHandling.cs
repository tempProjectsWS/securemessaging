﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.Resources;
using Windows.Data.Xml.Dom;
using Windows.Networking.PushNotifications;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace CryptoChat
{
    class WNSHandling
    {
        public async static Task RegisterNotifier()
        {
            var access = await BackgroundExecutionManager.RequestAccessAsync();

            if (access == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
                access == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity)
            {
                foreach (var task in BackgroundTaskRegistration.AllTasks)
                {
                    if (task.Value.Name == "MessageNotifier")
                    {
                        task.Value.Unregister(true);
                        break;
                    }
                }
                var builder = new BackgroundTaskBuilder();
                builder.Name = "MessageNotifier";
                builder.TaskEntryPoint = "MessageNotifier.MessageNotifier";
                builder.SetTrigger(new PushNotificationTrigger());
                builder.Register();
            }
        }
        public async static Task<PushNotificationChannel> RequestWNSUri()
        {
            try
            {
                var channel =  await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
                channel.PushNotificationReceived += notificationReceived;
                return channel;

            }
            catch(Exception)
            {
                return null;
            }
        }
        private static void notificationReceived(PushNotificationChannel sender, PushNotificationReceivedEventArgs args)
        {
            string receiver = args.RawNotification.Content;

            if (CommonData.session!=null && receiver == CommonData.session.Username)
            {
                if (CommonData.activeConversationPage != null)
                {
                    CommonData.activeConversationPage.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        CommonData.activeConversationPage.refreshMessages(null, null);
                    });
                }
            }
            else
            {
                foreach (string[] credential in DataStorage.GetNotifierCredentials())
                {
                    if (receiver == credential[0])
                    {
                        ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
                        XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
                        var toastTextElements = toastXml.GetElementsByTagName("text");
                        ResourceLoader resourceGetter = new ResourceLoader();
                        string appName = resourceGetter.GetString("ApplicationTitle");
                        string notificationText = resourceGetter.GetString("NotifierNewMessageText") + credential[0];
                        toastTextElements[0].AppendChild(toastXml.CreateTextNode(appName));
                        toastTextElements[1].AppendChild(toastXml.CreateTextNode(notificationText));
                        ToastNotification toast = new ToastNotification(toastXml);
                        ToastNotificationManager.CreateToastNotifier().Show(toast);
                    }
                }
            }
        }
    }
}
