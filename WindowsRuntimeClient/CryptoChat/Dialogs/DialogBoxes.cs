﻿using System;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace CryptoChat
{
    class DialogBoxes
    {
        public async static Task ShowMessageBox(string message)
        {
            await (new MessageDialog(message)).ShowAsync();
        }
        public async static Task<string> AskForInput(string title, string text)
        {
            InputDialog dialog = new InputDialog();

            return await dialog.ShowAsync(title, text);
        }
        public static async Task AskForConfirmation(string message, Action onAccepted)
        {
            MessageDialog dialog = new MessageDialog(message);
            dialog.Commands.Add(new UICommand(CommonData.Resources["YesButtonText"], new UICommandInvokedHandler((a) => { onAccepted(); })));
            dialog.Commands.Add(new UICommand(CommonData.Resources["NoButtonText"]));
            await dialog.ShowAsync();
        }
    }
}
