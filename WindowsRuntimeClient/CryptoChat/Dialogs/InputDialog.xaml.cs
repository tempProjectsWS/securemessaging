﻿using System;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace CryptoChat
{
    public sealed partial class InputDialog : ContentDialog
    {
        private string inputValue;
        public InputDialog()
        {
            this.InitializeComponent();
        }

        public async Task<String> ShowAsync(string title, string text)
        {
            this.Title = title;
            this.text.Text = text;
            this.inputValue = null;
            await this.ShowAsync();
            return inputValue;
        }
        private void okButtonClicked(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            inputValue = inputBox.Text;
        }

        private void cancelButtonClicked(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            inputValue = null;
        }
    }
}
