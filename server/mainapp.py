from webapp2 import WSGIApplication
import datetime

from users import UserAddHandler, LoginHandler, SetPasswordHandler, LogoutHandler, GetPublicKeyHandler, \
    UpdatePublicKeyHandler, AuthorizeNewTokenHandler, UnauthorizeClientHandler, RegisterGCMHandler, VerifyHandler, \
    ForgotPasswordHandler, PasswordRecoveryHandler, RegisterWNSHandler, ReverifyHandler, RegisterChannelHandler, \
    RerecoveryHandler, RenameUserByAdminHandler, RenameUserHandler
from messaging import SendMessageHandler, GetMessagesHandler, CheckMessagesHandler, RequestUploadHandler, \
    UploadCompleteHandler, DownloadFileHandler
from cronjobs import *
from HTTPStrings import *

# Secure cookie key stored separately
from secretkeys import *

config = {
    'webapp2_extras.sessions': {
        "secret_key": cookieKey
    },
    'webapp2_extras.auth': {
        'user_model': 'usermodel.UserModel'
    },
}
epoch = datetime.datetime(year=1970, month=1, day=1, hour=0, minute=0, second=0)


class CurrentTime(RequestHandler):
    def post(self):
        currentTime = datetime.datetime.utcnow()
        unixTime = currentTime - epoch
        self.response.write(str(int(unixTime.total_seconds())))


app = WSGIApplication(routes=[(ADDR_ADD_USER, UserAddHandler),
                              (ADDR_LOGIN, LoginHandler),
                              (ADDR_SET_PASSWORD, SetPasswordHandler),
                              (ADDR_LOGOUT, LogoutHandler),
                              (ADDR_SEND_MESSAGE, SendMessageHandler),
                              (ADDR_RECEIVE_MESSAGE, GetMessagesHandler),
                              (ADDR_GET_PUBKEY, GetPublicKeyHandler),
                              (ADDR_VERIFY, VerifyHandler),
                              (ADDR_FORGOT, ForgotPasswordHandler),
                              (ADDR_RECOVERY, PasswordRecoveryHandler),
                              (CRON_CLEARTOKENS, ClearTokens),
                              (CRON_CLEARUSERS, ClearUsers),
                              (ADDR_TIME, CurrentTime),
                              (ADDR_UPDATE_PUBKEY, UpdatePublicKeyHandler),
                              (ADDR_AUTHORIZE_NEW_TOKEN, AuthorizeNewTokenHandler),
                              (CRON_CLEARMESSAGES, ClearMessages),
                              (ADDR_UNAUTHORIZE, UnauthorizeClientHandler),
                              (ADDR_CHECK_NEW_MESSAGES, CheckMessagesHandler),
                              (ADDR_REQUEST_FILE_UPLOAD, RequestUploadHandler),
                              (ADDR_UPLOAD_COMPLETE, UploadCompleteHandler),
                              (ADDR_DOWNLOAD_FILE, DownloadFileHandler),
                              (ADDR_REGISTER_GCM, RegisterGCMHandler),
                              (ADDR_REGISTER_WNS, RegisterWNSHandler),
                              (ADDR_REGISTER_CHANNEL, RegisterChannelHandler),
                              (ADDR_RENAME_USER, RenameUserHandler),
                              ("/reverify", ReverifyHandler),
                              ("/rerecovery", RerecoveryHandler),
                              ("/renameadmin", RenameUserByAdminHandler)],
                      config=config)
