from google.appengine.api import mail
from google.appengine.ext import ndb
from google.appengine.runtime.apiproxy_errors import DeadlineExceededError
import logging
from webapp2_extras.security import generate_password_hash
from webapp2_extras.appengine.auth.models import User, UserToken, Unique
from HTTPStrings import *

MAX_EMAIL_RETRY_COUNT = 3


class UserModel(User):
    def set_password(self, raw_password):
        self.password = generate_password_hash(raw_password, length=12)
        self.put()

    def getId(self):
        return self.key.id()

    def recoveryToken(self):
        return UserToken.create(self.getId(), "recovery").token

    def deleteRecoveryToken(self, token):
        UserToken.get(self.getId(), "recovery", token).key.delete()

    def getKey(self):
        keys = UserKey.query(ancestor=UserKey.newKey(self.auth_ids[0])).fetch(1)
        if len(keys) > 0:
            return keys[0]
        else:
            key = UserKey(parent=UserKey.newKey(self.auth_ids[0]))
            key.put()
            return key

    def rename(self, newUsername, preserveKeys=False):
        if Unique.create("UserModel.auth_id:" + newUsername):
            oldUsername = self.auth_ids[0]
            Unique.delete_multi(["UserModel.auth_id:" + oldUsername])
            oldKey = self.getKey()
            newKey = UserKey(parent=UserKey.newKey(newUsername))
            if preserveKeys:
                newKey.pubkey = oldKey.pubkey
            else:
                self.loginToken = ""
            self.auth_ids[0] = newUsername
            oldKey.key.delete()
            self.put()
            newKey.put()
        else:
            return RESPONSE_USER_NAME_EXISTS

    def sendVerificationEmail(self):
        retry_count = 0
        while retry_count <= MAX_EMAIL_RETRY_COUNT:
            try:
                username = self.auth_ids[0]
                email = self.email
                token = UserModel.create_signup_token(self.getId())
                emailTemplateHTML = open("static/verify_mail.html", mode="r").read()
                emailTemplateTXT = open("static/verify_mail.txt", mode="r").read()
                emailContentsHTML = emailTemplateHTML.replace("{USERNAME HERE}", username)
                emailContentsHTML = emailContentsHTML.replace("{EMAIL HERE}", email)
                emailContentsHTML = emailContentsHTML.replace("{SITE ADDR}", ADDR_SITE)
                emailContentsHTML = emailContentsHTML.replace("{VERIFICATION ADDR}", ADDR_VERIFY)
                emailContentsHTML = emailContentsHTML.replace("{TOKEN HERE}", token)
                emailContentsTXT = emailTemplateTXT.replace("{USERNAME HERE}", username)
                emailContentsTXT = emailContentsTXT.replace("{EMAIL HERE}", email)
                emailContentsTXT = emailContentsTXT.replace("{SITE ADDR}", ADDR_SITE)
                emailContentsTXT = emailContentsTXT.replace("{VERIFICATION ADDR}", ADDR_VERIFY)
                emailContentsTXT = emailContentsTXT.replace("{TOKEN HERE}", token)
                mail.send_mail("cryptochat.verify.user.noreply@sec-msg.appspotmail.com", email,
                               "CryptoChat verification",
                               emailContentsTXT, html=emailContentsHTML)
                return
            except DeadlineExceededError as error:
                logging.warning("VerifyEmailDeadline: " + str(error))
                retry_count += 1
                if retry_count > MAX_EMAIL_RETRY_COUNT:
                    raise error

    def sendRecoveryEmail(self):
        retry_count = 0
        while retry_count <= MAX_EMAIL_RETRY_COUNT:
            try:
                token = self.recoveryToken()
                emailTemplateHTML = open("static/recovery_mail.html").read()
                emailTemplateTXT = open("static/recovery_mail.txt").read()
                emailContentsHTML = emailTemplateHTML.replace("{TOKEN HERE}", token)
                emailContentsTXT = emailTemplateTXT.replace("{TOKEN HERE}", token)
                emailContentsHTML = emailContentsHTML.replace("{SITE ADDR}", ADDR_SITE)
                emailContentsTXT = emailContentsTXT.replace("{SITE ADDR}", ADDR_SITE)
                emailContentsHTML = emailContentsHTML.replace("{RECOVERY ADDR}", ADDR_RECOVERY)
                emailContentsTXT = emailContentsTXT.replace("{RECOVERY ADDR}", ADDR_RECOVERY)
                mail.send_mail("cryptochat.password.recovery.noreply@sec-msg.appspotmail.com", self.email,
                               "CryptoChat password recovery", emailContentsTXT,
                               html=emailContentsHTML)
                return
            except DeadlineExceededError as error:
                logging.warning("VerifyEmailDeadline: " + str(error))
                retry_count += 1
                if retry_count > MAX_EMAIL_RETRY_COUNT:
                    raise error


class UserKey(ndb.Model):
    pubkey = ndb.TextProperty(indexed=False)

    @classmethod
    def newKey(cls, user):
        return ndb.Key("USERKEY", user)
