from google.appengine.ext.blobstore.blobstore import BlobInfo
import logging
from webapp2 import RequestHandler
from webapp2_extras.appengine.auth.models import UserToken, Unique
import datetime

from HTTPStrings import RESPONSE_OK
from usermodel import UserModel

CRON_CLEARTOKENS = "/cleartokens"
CRON_CLEARUSERS = "/clearusers"
CRON_CLEARMESSAGES = "/clearmessages"


class ClearTokens(RequestHandler):
    def get(self):
        ageLimit = datetime.datetime.now() - datetime.timedelta(days=1)
        targetTokensQuery = UserToken.query(UserToken.created < ageLimit)
        targetTokens = targetTokensQuery.iter()
        for targetToken in targetTokens:
            targetToken.key.delete()
        self.response.write(RESPONSE_OK)


class ClearUsers(RequestHandler):
    def get(self):
        ageLimit = datetime.datetime.now() - datetime.timedelta(days=1)
        targetUsersQuery = UserModel.query(UserModel.updated < ageLimit)
        targetUsers = targetUsersQuery.iter()
        for targetUser in targetUsers:
            if not targetUser.verified:
                try:
                    logging.info("Deleted user: "+str(targetUser.auth_ids[0]))
                    Unique.delete_multi(
                        ["UserModel.auth_id:" + targetUser.auth_ids[0], "UserModel.email:" + targetUser.email])
                    try:
                        targetUser.getKey().key.delete()
                    except IndexError:
                        logging.info("Missing key: " + str(targetUser.auth_ids))
                    targetUser.key.delete()

                except Exception as error:
                    logging.error("Error " + str(type(error)) + " " + str(error) + " " + str(error.message))

        self.response.write(RESPONSE_OK)


class ClearMessages(RequestHandler):
    def get(self):
        """targetMessagesQuery = MessageModel.query(MessageModel.messageRead == True)
        targetMessages = targetMessagesQuery.iter()
        for targetMessage in targetMessages:
            if targetMessage.fileKey is None:
                targetMessage.key.delete()
            elif datetime.datetime.utcnow() - targetMessage.timestamp > datetime.timedelta(1):
                blobstore.BlobInfo.get(targetMessage.fileKey).delete()
                targetMessage.key.delete()"""
        try:
            query = BlobInfo.all()
            blobs = query.fetch(300)
            index = 0
            if len(blobs) > 0:
                for blob in blobs:
                    if datetime.datetime.utcnow() - blob.creation > datetime.timedelta(2):
                        blob.delete()
                    index += 1
            if index == 300:
                self.redirect("/clearmessages")
            self.response.write(RESPONSE_OK)
        except Exception as error:
            logging.error("Error " + str(type(error)) + " " + str(error) + " " + str(error.message))
