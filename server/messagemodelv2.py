import datetime
from google.appengine.ext import ndb
import json
from HTTPStrings import *
from google.appengine.ext.ndb import blobstore

epoch = datetime.datetime.fromtimestamp(0)


class MessageModelV2(ndb.Model):
    jsonData = ndb.TextProperty(indexed=False)

    @staticmethod
    def create(receiverId, sender, contents, publicKeyUsed, publicKeyCalculated, fileKey=None,
               signature=None):
        newMessage = MessageModelV2(parent=MessageModelV2.messageKey(receiverId))
        dataDict = {JSON_MSG_PUBLCIKEY_CALCULATED: publicKeyCalculated,
                    JSON_MSG_PUBLICKEY_USED: publicKeyUsed,
                    JSON_MSG_TIMESTAMP: (datetime.datetime.utcnow() - epoch).total_seconds(),
                    JSON_MSG_SENDER: sender,
                    JSON_MSG_CONTENTS: ""}
        if signature:
            dataDict[JSON_MSG_SM_SIGNATURE] = signature
        if fileKey:
            dataDict[JSON_FILE_KEY] = str(fileKey)
        else:
            dataDict[JSON_MSG_CONTENTS] = contents
        newMessage.jsonData = json.dumps(dataDict)
        newMessage.put()

    @classmethod
    def messageKey(cls, receiver):
        return ndb.Key("MESSAGESV2", receiver)

    @staticmethod
    def getMessages(user, count):
        messages_query = MessageModelV2.query(ancestor=MessageModelV2.messageKey(user.getId()))
        fetched, cursor, more = messages_query.fetch_page(count)
        messages = []
        messagesToDelete = []
        for message in fetched:
            messageDict = json.loads(message.jsonData)
            messageDict[JSON_MSG_RECEIVER] = user.auth_ids[0]
            messageDict[JSON_MSG_ID] = message.key.id()
            if JSON_FILE_KEY in messageDict and (blobstore.BlobInfo.get(messageDict[POST_FILE_KEY])):
                file = blobstore.BlobInfo.get(messageDict[POST_FILE_KEY])
                messageDict[JSON_MESSAGE_IS_FILE] = True
                messageDict[JSON_FILENAME] = file.filename
                messageDict[JSON_FILE_MIMETYPE] = file.content_type
            else:
                messageDict[JSON_FILENAME] = ""
                messageDict[JSON_FILE_KEY] = ""
                messageDict[JSON_FILE_MIMETYPE] = ""
                messageDict[JSON_MESSAGE_IS_FILE] = False
            messagesToDelete.append(message.key)
            messages.append(messageDict)
        if len(messages) < count:
            more = False
        ndb.delete_multi(messagesToDelete)
        return {JSON_MESSAGES: messages, JSON_MORE_MESSAGES: more}

    @staticmethod
    def hasMessages(user):
        messages_query = MessageModelV2.query(ancestor=MessageModelV2.messageKey(user.getId()))
        return messages_query.count() > 0
