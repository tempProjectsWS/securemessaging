from google.appengine.api import urlfetch
import json
import logging

# API keys are not stored in the repository
from secretkeys import *

httpHeaders = {"Authorization": "key=" + GCMapiKey,
               "Content-Type": "application/json"}
gcmServerUrl = "https://gcm-http.googleapis.com/gcm/send"


def GCMNotify(userId, username):
    messageData = {
        "registration_ids": [userId, ],
        "data": {
            "username": username
        },
    }
    try:
        requestPayload = json.dumps(messageData)
        response = urlfetch.fetch(url=gcmServerUrl, payload=requestPayload, headers=httpHeaders, method="POST",
                                  validate_certificate=True)
        if response.status_code != 200:
            logging.warning("GCM error: " + str(response.status_code) + " " + str(response.headers))
    except Exception as error:
        logging.warning("GCM error: " + str(error))
