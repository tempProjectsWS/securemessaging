from urllib import urlencode
from google.appengine.api import urlfetch
import json
import logging
from google.appengine.api import memcache

# API keys are not stored in the repository
from secretkeys import *

wnsServerUrl = "https://login.live.com/accesstoken.srf"

newTokenRequestData = {
    "grant_type": "client_credentials",
    "client_id": WNSclientId,
    "client_secret": WNSclientSecret,
    "scope": "notify.windows.com"
}


def requestNewToken():
    try:
        response = urlfetch.fetch(url=wnsServerUrl, payload=urlencode(newTokenRequestData), method="POST",
                                  validate_certificate=True)
        if response.status_code == 200:
            return json.loads(response.content)["access_token"]
        else:
            logging.warning(
                "WNS error: " + str(response.status_code) + " " + str(response.headers) + " " + str(response.content))
    except Exception as error:
        logging.warning("WNS error: " + str(error))
    return None


def WNSNotify(uri, username):
    token = memcache.get("WNSToken")
    if not token:
        token = requestNewToken()
        if not token:
            return
        if sendWnsNotificationRequest(token, uri, username):
            memcache.add(key="WNSToken", value=token, time=86400)
    else:
        if not sendWnsNotificationRequest(token, uri, username):
            token = requestNewToken()
            if not token:
                return
            if sendWnsNotificationRequest(token, uri, username):
                memcache.set(key="WNSToken", value=token, time=86400)


def sendWnsNotificationRequest(token, uri, username):
    notificationRequestHeaders = {"X-WNS-Type": "wns/raw",
                                  "Authorization": "Bearer " + token,
                                  "Content-Type": "application/octet-stream",
                                  "X-WNS-Cache-Policy": "cache"}
    try:
        response = urlfetch.fetch(url=uri, payload=username, method="POST", headers=notificationRequestHeaders,
                                  validate_certificate=True)
        if response.status_code == 200:
            return True
        else:
            logging.warning(
                "WNS send error: " + str(response.status_code) + " " + str(response.headers) + " " + str(
                    response.content))
    except Exception as error:
        logging.warning("WNS send error: " + str(error))
    return False
