from google.appengine.api.mail_errors import BadRequestError
from google.appengine.runtime.apiproxy_errors import DeadlineExceededError
from google.appengine.api.channel import channel
import logging
import os
import base64
from webapp2_extras.auth import InvalidAuthIdError, InvalidPasswordError

from HTTPStrings import *
from usermodel import UserKey
from basehandler import BaseHandlerUser


def userRequired(handler):
    def checkLogin(self, *args, **kwargs):
        if not self.user:
            self.response.write(RESPONSE_USER_NOT_LOGGED_IN)
        else:
            if not self.user.verified:
                self.response.write(RESPONSE_USER_NOT_VERIFIED)
            else:
                return handler(self, *args, **kwargs)

    return checkLogin


class UserAddHandler(BaseHandlerUser):
    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_PASSWORD,
                                      POST_USER_EMAIL, ):
            return
        username = self.request.POST[POST_USER_NAME]
        email = self.request.POST[POST_USER_EMAIL]
        logging.info("Username: '" + username + "'")
        logging.info("Email: '" + email + "'")
        if (not username.isalnum()) or (len(username) > 20):
            logging.warning("Username contains invalid characters")
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        password = self.request.POST[POST_USER_PASSWORD]
        if len(password) > 100:
            self.response.write(RESPONSE_USER_BAD_PASSWORD)
            return
        if len(email) > 256:
            logging.warning("Bad email")
            self.response.write(RESPONSE_USER_BAD_EMAIL)
            return
        newUser = self.user_model.create_user(username, ["email"], email=email, password_raw=password,
                                              verified=False, loginToken="", GCMKey="", WNSUri="")
        if not newUser[0]:
            if newUser[1][0] == 'auth_id':
                logging.warning("Already registered")
                self.response.write(RESPONSE_USER_NAME_EXISTS)
            else:
                logging.warning("Already registered email")
                self.response.write(RESPONSE_USER_EMAIL_EXISTS)
        else:
            user = newUser[1]
            try:
                user.sendVerificationEmail()
            except DeadlineExceededError as error:
                logging.error("VerifyEmailFail: " + str(error))
            except BadRequestError:
                logging.warning("Bad email")
                self.response.write(RESPONSE_USER_BAD_EMAIL)
                return
            userKeys = UserKey(parent=UserKey.newKey(newUser[1].auth_ids[0]))
            userKeys.pubkey = ""
            userKeys.put()
            self.response.write(RESPONSE_OK)


class UnauthorizeClientHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        self.user.loginToken = ""
        self.user.GCMKey = ""
        self.user.WNSUri = ""
        self.user.put()
        self.response.write(RESPONSE_OK)


class VerifyHandler(BaseHandlerUser):
    def get(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_TOKEN):
            return
        userName = self.request.get(POST_USER_NAME)
        token = self.request.get(POST_USER_TOKEN)
        user = self.user_model.get_by_auth_id(userName)
        if not user:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        if self.user_model.validate_token(self.user_model.get_by_auth_id(userName).getId(), "signup", token):
            user = self.user_model.get_by_auth_id(userName)
            user.verified = True
            user.put()
            self.user_model.delete_signup_token(user.getId(), token)
            self.response.write(RESPONSE_USER_VERIFIED)
        else:
            user = self.user_model.get_by_auth_id(userName)
            if user.verified:
                self.response.write(RESPONSE_USER_ALREADY_VERIFIED)
            else:
                self.response.write(RESPONSE_VERIFY_BAD_TOKEN)


class ForgotPasswordHandler(BaseHandlerUser):
    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_EMAIL):
            return
        username = self.request.POST[POST_USER_NAME]
        email = self.request.POST[POST_USER_EMAIL]
        logging.info("Username: '" + username + "'")
        logging.info("Email: '" + email + "'")
        user = self.user_model.get_by_auth_id(username)
        if not user:
            logging.warning("Bad username")
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        if email == user.email:
            try:
                user.sendRecoveryEmail()
            except DeadlineExceededError as error:
                logging.error("RecoveryEmailFail: " + str(error))
            self.response.write(RESPONSE_RECOVERY_EMAIL_SENT)
        else:
            logging.warning("Bad email")
            self.response.write(RESPONSE_USER_BAD_EMAIL)


class ReverifyHandler(BaseHandlerUser):
    def get(self):
        self.response.write(open("static/reverify.html").read())

    def post(self):
        if not self.requireParameters(POST_USER_NAME):
            return
        username = self.request.POST[POST_USER_NAME]
        user = self.user_model.get_by_auth_id(username)
        if user is None:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        try:
            user.sendVerificationEmail()
        except DeadlineExceededError as error:
            self.response.write(str(error))
            return
        self.response.write(RESPONSE_OK)


class RerecoveryHandler(BaseHandlerUser):
    def get(self):
        self.response.write(open("static/rerecovery.html").read())

    def post(self):
        if not self.requireParameters(POST_USER_NAME):
            return
        username = self.request.POST[POST_USER_NAME]
        user = self.user_model.get_by_auth_id(username)
        if user is None:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        try:
            user.sendRecoveryEmail()
        except DeadlineExceededError as error:
            self.response.write(str(error))
            return
        self.response.write(RESPONSE_OK)


class RenameUserByAdminHandler(BaseHandlerUser):
    def get(self):
        self.response.write(open("static/rename.html").read())

    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_EMAIL, POST_NEW_USERNAME):
            return
        username = self.request.POST[POST_USER_NAME]
        email = self.request.POST[POST_USER_EMAIL]
        newUsername = self.request.POST[POST_NEW_USERNAME]
        user = self.user_model.get_by_auth_id(username)
        if user is None:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        if user.email != email:
            self.response.write(RESPONSE_USER_BAD_EMAIL)
            return
        if (not newUsername.isalnum()) or (len(newUsername) > 20):
            logging.warning("Username contains invalid characters")
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        result = user.rename(newUsername)
        if result:
            self.response.write(result)
        else:
            self.response.write(RESPONSE_OK)


class RenameUserHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if not self.requireParameters(POST_NEW_USERNAME):
            return
        newUsername = self.request.POST[POST_NEW_USERNAME]
        if (not newUsername.isalnum()) or (len(newUsername) > 20):
            logging.warning("Username contains invalid characters")
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        result = self.user.rename(newUsername, preserveKeys=True)
        if not result:
            self.response.write(RESPONSE_OK)
        else:
            self.response.write(result)


class LoginHandler(BaseHandlerUser):
    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_PASSWORD, POST_USER_TOKEN):
            return
        username = self.request.POST[POST_USER_NAME]
        password = self.request.POST[POST_USER_PASSWORD]
        logging.info("Username: '" + username + "'")
        token = self.request.POST[POST_USER_TOKEN]
        try:
            user = self.user_model.get_by_auth_id(username)
            if user is None:
                raise InvalidAuthIdError
            if user.loginToken == None or user.loginToken == "" or user.loginToken != token:
                logging.warning("Bad/no token")
                self.response.write(RESPONSE_UNAUTHORIZED)
                return
            if not user:
                raise InvalidAuthIdError
            if not user.verified:
                logging.warning("Not verified")
                self.response.write(RESPONSE_USER_NOT_VERIFIED)
                return
            self.auth.get_user_by_password(auth_id=username, password=password,
                                           save_session=True, remember=True)
            user.put()
            self.response.write(RESPONSE_OK)
        except InvalidAuthIdError:
            logging.warning("Bad username")
            self.response.write(RESPONSE_USER_BAD_USERNAME)
        except InvalidPasswordError:
            logging.warning("Bad password")
            self.response.write(RESPONSE_USER_BAD_PASSWORD)


class AuthorizeNewTokenHandler(BaseHandlerUser):
    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_PASSWORD, POST_USER_PUBKEY):
            return
        username = self.request.POST[POST_USER_NAME]
        password = self.request.POST[POST_USER_PASSWORD]
        logging.info("Username: '" + username + "'")
        key = self.request.POST[POST_USER_PUBKEY]
        try:
            user = self.user_model.get_by_auth_id(username)
            if not user:
                raise InvalidAuthIdError
            if not user.verified:
                logging.warning("Not verified")
                self.response.write(RESPONSE_USER_NOT_VERIFIED)
                return
            user = self.auth.get_user_by_password(auth_id=username, password=password,
                                                  save_session=False, remember=False)
            user = self.user_model.get_by_id(user["user_id"])
            if user.loginToken:
                logging.warning("Not verified")
                self.response.write(RESPONSE_ALREADY_AUTHORIZED)
                return
            newToken = base64.b64encode(os.urandom(16))
            user.loginToken = newToken
            userKey = user.getKey()
            userKey.pubkey = key
            userKey.put()
            user.put()
            self.response.write(newToken)
        except InvalidAuthIdError:
            logging.warning("Bad username")
            self.response.write(RESPONSE_USER_BAD_USERNAME)
        except InvalidPasswordError:
            logging.warning("Bad password")
            self.response.write(RESPONSE_USER_BAD_PASSWORD)


class PasswordRecoveryHandler(BaseHandlerUser):
    def get(self):
        if not self.requireParameters(POST_USER_TOKEN):
            return
        responseContent = open("static/recovery_page.html").read()
        responseContent = responseContent.replace("{TOKEN HERE}", self.request.get(POST_USER_TOKEN))
        self.response.write(responseContent)

    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_PASSWORD, POST_USER_TOKEN):
            return
        username = self.request.POST[POST_USER_NAME]
        password = self.request.POST[POST_USER_PASSWORD]
        logging.info("Username: '" + username + "'")
        token = self.request.POST[POST_USER_TOKEN]
        if len(password) > 100:
            logging.warning("Bad password format")
            self.response.write(RESPONSE_USER_BAD_PASSWORD)
            return
        user = self.user_model.get_by_auth_id(username)
        if not user:
            logging.warning("Bad username")
            self.response.write(RESPONSE_RECOVERY_BAD_USERNAME)
            return
        if self.user_model.validate_token(user.getId(), "recovery", token):
            user.set_password(password)
            user.deleteRecoveryToken(token)
            user.loginToken = ""
            user.GCMKey = ""
            user.WNSUri = ""
            user.put()
            self.response.write(RESPONSE_PASSWORD_RECOVERY_OK)
        else:
            logging.warning("Bad token")
            self.response.write(RESPONSE_RECOVERY_BAD_TOKEN)


class SetPasswordHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if not self.requireParameters(POST_USER_PASSWORD):
            return
        password = self.request.POST[POST_USER_PASSWORD]
        if len(password) > 100:
            self.response.write(RESPONSE_USER_BAD_PASSWORD)
            return
        self.user.set_password(password)
        self.response.write(RESPONSE_OK)


class LogoutHandler(BaseHandlerUser):
    def post(self):
        self.auth.unset_session()
        self.response.write(RESPONSE_OK)


class UpdatePublicKeyHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if not self.requireParameters(POST_USER_PUBKEY):
            return
        key = self.request.POST[POST_USER_PUBKEY]
        userKeys = self.user.getKey()
        userKeys.pubkey = key
        userKeys.put()
        self.response.write(RESPONSE_OK)


class GetPublicKeyHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if POST_MSG_PEER in self.request.POST:
            userKeys = UserKey.query(ancestor=UserKey.newKey(self.request.POST[POST_MSG_PEER])).fetch(1)
        else:
            userKeys = [self.user.getKey()]
        if len(userKeys) > 0:
            self.response.write(userKeys[0].pubkey)
        else:
            logging.warning("Bad peer '" + self.request.POST[POST_MSG_PEER] + "'")
            self.response.write(RESPONSE_MSG_NO_PEER)


class RegisterGCMHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if not self.requireParameters(POST_GCM_KEY):
            return
        key = self.request.POST[POST_GCM_KEY]
        self.user.GCMKey = key
        self.user.put()
        self.response.write(RESPONSE_OK)


class RegisterWNSHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if not self.requireParameters(POST_WNS_URI):
            return
        uri = self.request.POST[POST_WNS_URI]
        self.user.WNSUri = uri
        self.user.put()
        self.response.write(RESPONSE_OK)


class RegisterChannelHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        userChannel = channel.create_channel(self.user.auth_ids[0], 1440)
        self.user.channelActive = True
        self.user.put()
        self.response.write(userChannel)
