import logging, traceback
from webapp2 import RequestHandler, cached_property
from webapp2_extras import sessions, auth

from usermodel import UserModel
from HTTPStrings import *


class BaseHandlerSession(RequestHandler):
    def dispatch(self):
        self.session_store = sessions.get_store(request=self.request)
        try:
            RequestHandler.dispatch(self)
        except UnicodeDecodeError:
            logging.warning("Username with bad characters")
            self.response.write(RESPONSE_USER_BAD_USERNAME)
        except Exception as error:
            try:
                postArgs = dict(self.request.POST)
                if POST_USER_PASSWORD in postArgs:
                    postArgs.pop(POST_USER_PASSWORD)
                logging.error(
                    "Unhandled error \n" + traceback.format_exc() + str(postArgs))
            except Exception:
                logging.error("Unhandled error, unprintable POST args \n" + traceback.format_exc())
            self.response.write(RESPONSE_UNHANDLED_ERROR)
        finally:
            self.session_store.save_sessions(self.response)

    def requireParameters(self, *args):
        for parameter in args:
            if not ((parameter in self.request.POST) or (parameter in self.request.GET)):
                self.response.write(RESPONSE_MISSING_POST_PARAMETERS)
                logging.warning("MISSING POST "+str(self.request.POST))
                return False
        return True

    @cached_property
    def session(self):
        return self.session_store.get_session(backend="datastore")


class BaseHandlerUser(BaseHandlerSession):
    @cached_property
    def auth(self):
        return auth.get_auth()

    @cached_property
    def user_info(self):
        return self.auth.get_user_by_session()

    @cached_property
    def user_model(self):
        return UserModel

    @cached_property
    def user(self):
        if self.user_info:
            return self.user_model.get_by_id(self.user_info["user_id"])
        else:
            return None
