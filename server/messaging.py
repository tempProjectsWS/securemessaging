import json
from google.appengine.ext.blobstore import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api.channel import channel

from basehandler import BaseHandlerUser
from gcm import GCMNotify
from messagemodelv2 import MessageModelV2
import users
from HTTPStrings import *
from wns import WNSNotify


class SendMessageHandler(BaseHandlerUser):
    @users.userRequired
    def post(self):
        if not self.requireParameters(POST_MSG_PEER, POST_MSG_CONTENTS, POST_MSG_PUBLCIKEY_CALCULATED,
                                      POST_MSG_PUBLICKEY_USED):
            return
        receiver = self.user_model.get_by_auth_id(self.request.POST[POST_MSG_PEER])
        if not receiver:
            self.response.write(RESPONSE_MSG_NO_PEER)
            return
        if not receiver.loginToken:
            self.response.write(RESPONSE_PEER_INACTIVE)
            return
        usedPublicKey = self.request.POST[POST_MSG_PUBLICKEY_USED]
        calculatedPublicKey = self.request.POST[POST_MSG_PUBLCIKEY_CALCULATED]
        contents = self.request.POST[POST_MSG_CONTENTS]
        signature = None
        if POST_MSG_SM_SIGNATURE in self.request.POST:
            signature = self.request.POST[POST_MSG_SM_SIGNATURE]
        MessageModelV2.create(receiver.getId(), self.user.auth_ids[0],
                              contents, usedPublicKey, calculatedPublicKey, signature=signature)
        if hasattr(receiver, "GCMKey"):
            receiverGCM = receiver.GCMKey
        else:
            receiverGCM = None
        if hasattr(receiver, "WNSUri"):
            receiverWNS = receiver.WNSUri
        else:
            receiverWNS = None
        if hasattr(receiver, "channelActive"):
            receiverChannel = receiver.auth_ids[0]
        else:
            receiverChannel = None
        if (receiverGCM is not None) and (receiverGCM != ""):
            GCMNotify(receiverGCM, self.request.POST[POST_MSG_PEER])
        if (receiverWNS is not None) and (receiverWNS != ""):
            WNSNotify(receiverWNS, self.request.POST[POST_MSG_PEER])
        if receiverChannel is not None:
            channel.send_message(receiverChannel, receiverChannel)
        self.response.write(RESPONSE_OK)


class GetMessagesHandler(BaseHandlerUser):
    @users.userRequired
    def post(self):
        if POST_MSG_COUNT in self.request.POST:
            count = int(self.request.POST[POST_MSG_COUNT])
        else:
            count = 5
        if count > 20:
            count = 20
        messages = MessageModelV2.getMessages(self.user, count)
        messageList = json.dumps(messages)
        self.response.write(messageList)


class CheckMessagesHandler(BaseHandlerUser):
    @users.userRequired
    def post(self):
        if MessageModelV2.hasMessages(self.user):
            self.response.write(RESPONSE_HAS_NEW_MESSAGES)
        else:
            self.response.write(RESPONSE_NO_NEW_MESSAGES)


class RequestUploadHandler(BaseHandlerUser):
    @users.userRequired
    def post(self):
        self.response.write(blobstore.create_upload_url(success_path=ADDR_UPLOAD_COMPLETE,
                                                        max_bytes_total=4800000))


class UploadCompleteHandler(blobstore_handlers.BlobstoreUploadHandler, BaseHandlerUser):
    @users.userRequired
    def post(self):
        if not self.requireParameters(POST_MSG_PEER, POST_UPLOADED_FILE):
            return
        file = self.get_uploads(POST_UPLOADED_FILE)[0]
        contents = blobstore.BlobReader(file)
        try:
            jsonContents = json.load(contents)
        except ValueError:
            self.response.write(RESPONSE_FILE_CONTENTS_INVALID)
            return
        contents.close()
        if not (JSON_MSG_PUBLICKEY_USED in jsonContents and JSON_MSG_PUBLCIKEY_CALCULATED in jsonContents
                and JSON_MSG_CONTENTS in jsonContents):
            self.response.write(RESPONSE_FILE_CONTENTS_INVALID)
            return
        publicKeyUsed = jsonContents[JSON_MSG_PUBLICKEY_USED]
        publicKeyCalculated = jsonContents[JSON_MSG_PUBLCIKEY_CALCULATED]
        receiver = self.user_model.get_by_auth_id(self.request.POST[POST_MSG_PEER])
        if not receiver:
            self.response.write(RESPONSE_MSG_NO_PEER)
            return
        if not receiver.loginToken:
            self.response.write(RESPONSE_PEER_INACTIVE)
            return
        MessageModelV2.create(receiver.getId(), self.user.auth_ids[0],
                              None, publicKeyUsed, publicKeyCalculated, file.key())
        if hasattr(receiver, "GCMKey"):
            receiverGCM = receiver.GCMKey
        else:
            receiverGCM = None
        if hasattr(receiver, "WNSUri"):
            receiverWNS = receiver.WNSUri
        else:
            receiverWNS = None
        if (receiverGCM is not None) and (receiverGCM != ""):
            GCMNotify(receiverGCM, self.request.POST[POST_MSG_PEER])
        if (receiverWNS is not None) and (receiverWNS != ""):
            WNSNotify(receiverWNS, self.request.POST[POST_MSG_PEER])
        self.response.write(RESPONSE_OK)


class DownloadFileHandler(blobstore_handlers.BlobstoreDownloadHandler, BaseHandlerUser):
    @users.userRequired
    def post(self):
        if not self.requireParameters(POST_FILE_KEY):
            return
        file = self.request.POST[POST_FILE_KEY]
        fileInfo = blobstore.BlobInfo.get(self.request.POST[POST_FILE_KEY])
        if fileInfo is None:
            self.response.write(RESPONSE_BAD_FILE_KEY)
            return
        self.send_blob(file)
