import datetime
from google.appengine.ext import ndb
from HTTPStrings import *
from google.appengine.ext.ndb import blobstore


class MessageModel(ndb.Model):
    contents = ndb.TextProperty(indexed=False)
    sender = ndb.StringProperty(indexed=False)
    publicKeyUsed = ndb.TextProperty(indexed=False)
    publicKeyCalculated = ndb.TextProperty(indexed=False)
    timestamp = ndb.DateTimeProperty(auto_now_add=True)
    fileKey = ndb.BlobKeyProperty(indexed=False)
    signature = ndb.TextProperty(indexed=False)

    @staticmethod
    def create(receiverId, sender, contents, publicKeyUsed, publicKeyCalculated, fileKey=None,
               signature=None):
        newMessage = MessageModel(parent=MessageModel.messageKey(receiverId))
        newMessage.sender = sender
        newMessage.contents = contents
        newMessage.publicKeyUsed = publicKeyUsed
        newMessage.publicKeyCalculated = publicKeyCalculated
        newMessage.fileKey = fileKey
        newMessage.signature = signature
        newMessage.put()

    @classmethod
    def messageKey(cls, receiver):
        return ndb.Key("MESSAGES", receiver)

    @staticmethod
    def getMessages(user, count, includeFiles=False):
        messages_query = MessageModel.query(ancestor=MessageModel.messageKey(user.getId())).order(
            -MessageModel.timestamp)
        fetched, cursor, more = messages_query.fetch_page(count)
        messages = []
        messagesToDelete = []
        for message in fetched:
            if (not includeFiles) and (message.fileKey is not None):
                continue
            epoch = datetime.datetime.fromtimestamp(0)
            timestamp = message.timestamp - epoch
            messageDict = {JSON_MSG_PUBLCIKEY_CALCULATED: message.publicKeyCalculated,
                           JSON_MSG_PUBLICKEY_USED: message.publicKeyUsed,
                           JSON_MSG_RECEIVER: user.auth_ids[0],
                           JSON_MSG_SENDER: message.sender,
                           JSON_MSG_ID: message.key.id(),
                           JSON_MSG_TIMESTAMP: timestamp.total_seconds(),
                           JSON_FILENAME: "",
                           JSON_FILE_KEY: "",
                           JSON_MSG_CONTENTS: "",
                           }
            if message.signature is not None:
                messageDict[JSON_MSG_SM_SIGNATURE] = message.signature
            if (message.fileKey is not None) and (blobstore.BlobInfo.get(message.fileKey) is not None):
                file = blobstore.BlobInfo.get(message.fileKey)
                messageDict[JSON_MESSAGE_IS_FILE] = True
                messageDict[JSON_FILE_KEY] = str(message.fileKey)
                messageDict[JSON_FILENAME] = file.filename
                messageDict[JSON_FILE_MIMETYPE] = file.content_type
            else:
                messageDict[JSON_MESSAGE_IS_FILE] = False
                messageDict[JSON_MSG_CONTENTS] = message.contents
            messagesToDelete.append(message.key)
            messages.append(messageDict)
        if len(messages) < count:
            more = False
        ndb.delete_multi(messagesToDelete)
        return {JSON_MESSAGES: messages, JSON_MORE_MESSAGES: more}

    @staticmethod
    def hasMessages(user):
        messages_query = MessageModel.query(ancestor=MessageModel.messageKey(user.getId()))
        return messages_query.count() > 0
