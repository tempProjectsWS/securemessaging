from .HTTPStrings import *
from .HTTPStuff import makeOpener, sendRequest
from .cryptography import kdf, RSAKey, SHAHash


def requestRecovery(username, email):
    opener = makeOpener()
    sendRequest(opener, ADDR_FORGOT, {POST_USER_EMAIL: email,
                                      POST_USER_NAME: username})


def setNewPassword(token, username, newPassword):
    opener = makeOpener()
    if type(newPassword) == str:
        newPassword = newPassword.encode("utf-8")
    derivedKey = kdf(newPassword)
    key = RSAKey(derivedKey["KEY"])
    privateKey = key["PRIVATE ENCRYPTED"]
    publicKey = key["PUBLIC"]
    sendRequest(opener, ADDR_RECOVERY, {POST_USER_PASSWORD: SHAHash(newPassword),
                                        POST_USER_PRIVKEY: privateKey,
                                        POST_USER_KEY_SALT: derivedKey["SALT"],
                                        POST_USER_PUBKEY: publicKey,
                                        POST_USER_NAME: username,
                                        POST_USER_TOKEN: token})
