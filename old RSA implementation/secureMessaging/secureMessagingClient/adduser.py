from .HTTPStuff import sendRequest, makeOpener
from .HTTPStrings import *
from .cryptography import RSAKey, SHAHash, kdf


def addUser(name, email, password):
    if type(password) == str:
        password = password.encode("utf-8")
    derivedKey = kdf(password)
    userKey = RSAKey(derivedKey["KEY"])
    data = {POST_USER_NAME: name,
            POST_USER_EMAIL: email,
            POST_USER_PRIVKEY: userKey["PRIVATE ENCRYPTED"],
            POST_USER_PUBKEY: userKey["PUBLIC"],
            POST_USER_PASSWORD: SHAHash(password),
            POST_USER_KEY_SALT: derivedKey["SALT"]}
    sendRequest(makeOpener(), ADDR_ADD_USER, data)