try:
    from urllib.request import build_opener, HTTPCookieProcessor
    from http.cookiejar import CookieJar
    from urllib.parse import urlencode
except ImportError:
    from urllib2 import HTTPCookieProcessor, build_opener
    from urllib import urlencode
    from cookielib import CookieJar

from json import loads

from .HTTPStrings import goodResponses, errorResponses, ADDR_SITE, RESPONSE_NETWORK_ERROR


def makeOpener():
    opener = build_opener(HTTPCookieProcessor(CookieJar()))
    opener.addheaders = [('User-agent', "Python Client library")]
    return opener


class HTTPError(IOError):
    pass


def sendRequest(opener, address, data={}):
    data = urlencode(data).encode("utf-8")
    try:
        response = opener.open(ADDR_SITE + address, data=data).read()
    except OSError:
        response=RESPONSE_NETWORK_ERROR
    if response in errorResponses:
        raise HTTPError("Got an error response", response)
    elif response in goodResponses:
        return response
    else:
        return loads(response.decode("utf-8"))
