import datetime

from .cryptography import RSADecrypt, AESDecrypt, RSAVerify, AESKey, AESEncrypt, RSAEncrypt, RSASign, DecryptionError
from .HTTPStrings import *


class Message:
    def __init__(self, message, myUsername, privateKey):
        try:
            if message[JSON_MSG_SENDER] == myUsername:
                key = RSADecrypt(message[JSON_MSG_KEY1], privateKey)
            else:
                key = RSADecrypt(message[JSON_MSG_KEY2], privateKey)
            signature = message[JSON_MSG_SIGNATURE]
            senderKey = message[JSON_MSG_SENDER_KEY]
            if not RSAVerify(message[JSON_MSG_CONTENTS], signature, senderKey):
                raise DecryptionError("Bad signature")
            timestampString = message[JSON_MSG_CONTENTS][-18:]
            timestamp = message[JSON_MSG_TIMESTAMP]
            if abs(int(timestampString)-timestamp) > 60:
                raise DecryptionError("Bad timestamp")
            self.contents = AESDecrypt(message[JSON_MSG_CONTENTS][:-18], key)
        except DecryptionError as error:
            self.contents = "Invalid message: " + error.message
        self.id = message[JSON_MSG_ID]
        sender = message[JSON_MSG_SENDER]
        self.sender = sender
        timestamp = message[JSON_MSG_TIMESTAMP]
        self.timestamp = datetime.datetime.fromtimestamp(timestamp)

        if sender == myUsername:
            self.messagePeer = message[JSON_MSG_RECEIVER]
        else:
            self.messagePeer = message[JSON_MSG_SENDER]

    @staticmethod
    def generatePOST(contents, peer, peerKey, ownKey, timeDiff):
        key = AESKey()
        encryptedMessage = AESEncrypt(contents, key)
        key1 = RSAEncrypt(key, ownKey)
        key2 = RSAEncrypt(key, peerKey)
        currentTime = datetime.datetime.utcnow()
        epoch = datetime.datetime(1970, 1, 1, 0, 0, 0)
        timestamp = int((currentTime - epoch).total_seconds())+timeDiff
        timestampString = str(timestamp)
        if len(timestampString) < 18:
            timestampString = (18-len(timestampString))*"0"+timestampString
        encryptedMessage += timestampString.encode("ASCII")
        signature = RSASign(encryptedMessage, ownKey)
        return {POST_MSG_CONTENTS: encryptedMessage,
                POST_MSG_KEY1: key1,
                POST_MSG_KEY2: key2,
                POST_MSG_PEER: peer,
                POST_MSG_SIGNATURE: signature}

    def __str__(self):
        return "Mesage from: " + self.sender + " Contents: " + self.contents

    def getTimeStamp(self, timeformat="%Y-%m-%d %H:%M"):
        return self.timestamp.strftime(format=timeformat)

    def __repr__(self):
        return self.__str__()
