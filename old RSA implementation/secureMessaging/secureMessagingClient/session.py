from atexit import register

from .HTTPStuff import makeOpener, sendRequest
from .HTTPStrings import *
from .cryptography import SHAHash, keyEncrypt, kdf, keyDecrypt, DecryptionError
from .message import Message
from datetime import datetime, timedelta


class Session:
    def __init__(self, username, password):
        self.loggedIn = False
        self.opener = makeOpener()
        self.username = username
        self.keySalt = None
        self.privateKey = None
        self.timeDiff = 0
        if type(password) == str:
            self.password = password.encode("utf-8")
        else:
            self.password = password
        self.login()

    def login(self):
        sendRequest(self.opener, ADDR_LOGIN, {POST_USER_NAME: self.username,
                                              POST_USER_PASSWORD: SHAHash(self.password)})
        privateKeyData = self._getPrivateKey()
        self.keySalt = privateKeyData[JSON_KEY_SALT]
        derivedKey = kdf(self.password, self.keySalt)
        try:
            self.privateKey = keyDecrypt(privateKeyData[JSON_KEY], derivedKey["KEY"])
        except DecryptionError:
            privateKeyData = self._getPrivateKey()
            self.keySalt = privateKeyData[JSON_KEY_SALT]
            derivedKey = kdf(self.password, self.keySalt)
            self.privateKey = keyDecrypt(privateKeyData[JSON_KEY], derivedKey["KEY"])
        serverTimeString = sendRequest(self.opener, ADDR_TIME, {})
        serverUnixTime = int(serverTimeString)
        serverTime = datetime(year=1970, month=1, day=1, hour=0, minute=0, second=0) + timedelta(seconds=serverUnixTime)
        localTime = datetime.utcnow()
        self.timeDiff = int((serverTime - localTime).total_seconds())
        self.loggedIn = True
        register(self.logoutOnExit, self)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        if self.loggedIn:
            self.logout()

    @staticmethod
    def logoutOnExit(self):
        if self.loggedIn:
            self.logout()

    def logout(self):
        sendRequest(self.opener, ADDR_LOGOUT)
        self.loggedIn = False

    def getMessages(self, peer=None, count=20, cursor=None):
        POST = {POST_MSG_COUNT: count}
        if peer:
            POST[POST_MSG_PEER] = peer
        if cursor:
            POST[POST_MSG_CURSOR] = cursor
        receivedMessages = []
        data = sendRequest(self.opener, ADDR_RECEIVE_MESSAGE, POST)
        for message in data[JSON_MESSAGES]:
            receivedMessages.append(Message(message, self.username, self.privateKey))
        result = {"MESSAGES": receivedMessages, "CURSOR": data[JSON_MESSAGES_CURSOR], "MORE": data[JSON_MORE_MESSAGES]}
        return result

    def sendMessage(self, contents, peer):
        peerKey = self._getPublicKey(peer)
        parameters = Message.generatePOST(contents, peer, peerKey, self.privateKey, self.timeDiff)
        sendRequest(self.opener, ADDR_SEND_MESSAGE, data=parameters)

    def deleteMessage(self, message):
        sendRequest(self.opener, ADDR_DELETE_MESSAGE, {POST_MSG_PEER: message.messagePeer,
                                                       POST_MSG_ID: message.id})

    def setPassword(self, newPassword):
        if type(newPassword) == str:
            newPassword = newPassword.encode("utf-8")
        derivedKey = kdf(newPassword)
        newKey = keyEncrypt(self.privateKey, derivedKey["KEY"])
        self.password = newPassword
        sendRequest(self.opener, ADDR_SET_PASSWORD, {POST_USER_PASSWORD: SHAHash(self.password),
                                                     POST_USER_PRIVKEY: newKey,
                                                     POST_USER_KEY_SALT: derivedKey["SALT"]})

    def _getPublicKey(self, peer=None):
        if peer:
            key = sendRequest(self.opener, ADDR_GET_PUBKEY, {POST_MSG_PEER: peer})[JSON_KEY]
            return key
        else:
            key = sendRequest(self.opener, ADDR_GET_PUBKEY)[JSON_KEY]
            return key

    def _getPrivateKey(self):
        key = sendRequest(self.opener, ADDR_GET_PRIVATEKEY)
        return key
