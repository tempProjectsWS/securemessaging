import hashlib
import hmac
from base64 import b64encode, b64decode
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto import Random
from Crypto.Protocol.KDF import PBKDF2
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256

class DecryptionError(Exception):
    def __init__(self, message):
        Exception.__init__(self)
        self.message=message


def SHAHash(data):
    hasher = hashlib.sha256()
    hasher.update(data)
    return encode64(hasher.digest())


def AESKey():
    return Random.new().read(16)


def kdf(password, salt=None):
    if salt:
        return {"KEY": PBKDF2(password, count=2000, salt=decode64(salt))}
    else:
        salt = Random.new().read(16)
        return {"KEY": PBKDF2(password, count=2000, salt=salt), "SALT": encode64(salt)}


def RSAKey(passphrase):
    key = RSA.generate(4096)
    keyDict = {"PUBLIC": key.publickey().exportKey(), "PRIVATE": key.exportKey(),
                "PRIVATE ENCRYPTED": keyEncrypt(key.exportKey(), passphrase)}
    return keyDict


def PKCS7Pad(data):
    remainingLength = 16 - (len(data)%16)
    return data + bytes([remainingLength])*remainingLength


def PKCS7Unpad(data):
    return data[:-data[-1]]


def AESEncrypt(data, key):
    if type(data) == str:
        data = data.encode("utf-8")
    data = PKCS7Pad(data)
    hashgen = hmac.new(key, digestmod=hashlib.sha256)
    initializationVector = Random.new().read(16)
    cipher = AES.new(key, AES.MODE_CBC, initializationVector)
    ciphertext=initializationVector + cipher.encrypt(data)
    hashgen.update(ciphertext)
    ciphertext+=hashgen.digest()
    return encode64(ciphertext)


def AESDecrypt(data, key, decode=True):
    data = decode64(data)
    initializationVector = b"1234567890123456"
    cipher = AES.new(key, AES.MODE_CBC, initializationVector)
    hashgen = hmac.new(key, digestmod=hashlib.sha256)
    ciphertext=data[:-hashlib.sha256().digest_size]
    hashgen.update(ciphertext)
    hash = data[-hashlib.sha256().digest_size:]
    try:
        messageValid=hmac.compare_digest(hash, hashgen.digest())
    except AttributeError:
        messageValid = hash==hashgen.digest()
    if not messageValid:
        raise DecryptionError("Damaged message")
    decryptedData = cipher.decrypt(ciphertext)[16:]
    if decode:
        return PKCS7Unpad(decryptedData).decode("utf-8")
    else:
        return PKCS7Unpad(decryptedData)


def RSAEncrypt(data, pubKey):
    key = RSA.importKey(pubKey)
    encryptor = PKCS1_OAEP.new(key)
    return encode64(encryptor.encrypt(data))


def RSASign(data, privateKey):
    key = RSA.importKey(privateKey)
    signer = PKCS1_v1_5.new(key)
    hasher = SHA256.new(data)
    return encode64(signer.sign(hasher))


def RSAVerify(data, signature, publicKey):
    key = RSA.importKey(publicKey)
    data = data.encode("ASCII")
    signature = decode64(signature)
    signer = PKCS1_v1_5.new(key)
    hasher = SHA256.new(data)
    return signer.verify(hasher, signature)


def RSADecrypt(data, privKey):
    try:
        data = decode64(data)
        key = RSA.importKey(privKey)
        decryptor = PKCS1_OAEP.new(key)
        return decryptor.decrypt(data)
    except ValueError:
        raise DecryptionError("Decryption failed")


def keyEncrypt(key, newPassphrase):
    key = AESEncrypt(key, newPassphrase)
    return key


def keyDecrypt(key, password):
    return AESDecrypt(key, password, decode=False)


def encode64(s):
    return b64encode(s)


def decode64(s):
    return b64decode(s)
