from .HTTPStrings import *
from .HTTPStuff import makeOpener, sendRequest


def verifyUser(username, token):
    opener = makeOpener()
    sendRequest(opener, ADDR_VERIFY, {POST_USER_TOKEN: token,
                                      POST_USER_NAME: username})
