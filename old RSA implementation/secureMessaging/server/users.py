import json

from webapp2_extras.auth import InvalidAuthIdError, InvalidPasswordError
from HTTPStrings import *
from google.appengine.api import mail
from usermodel import UserKey
from basehandler import BaseHandlerUser


def userRequired(handler):
    def checkLogin(self, *args, **kwargs):
        if not self.user:
            self.response.write(RESPONSE_USER_NOT_LOGGED_IN)
        else:
            if not self.user.verified:
                self.response.write(RESPONSE_USER_NOT_VERIFIED)
            else:
                return handler(self, *args, **kwargs)

    return checkLogin


class UserAddHandler(BaseHandlerUser):
    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_PASSWORD, POST_USER_PUBKEY, POST_USER_PRIVKEY,
                                      POST_USER_EMAIL, POST_USER_KEY_SALT):
            return
        username = self.request.POST[POST_USER_NAME]
        password = self.request.POST[POST_USER_PASSWORD]
        pubkey = self.request.POST[POST_USER_PUBKEY]
        privateKey = self.request.POST[POST_USER_PRIVKEY]
        email = self.request.POST[POST_USER_EMAIL]
        salt = self.request.POST[POST_USER_KEY_SALT]
        newUser = self.user_model.create_user(username, ["email"], email=email, password_raw=password,
                                              verified=False)
        if not newUser[0]:
            if newUser[1][0] == 'auth_id':
                self.response.write(RESPONSE_USER_NAME_EXISTS)
            else:
                print(newUser)
                print("EMAIL", email)
                self.response.write(RESPONSE_USER_EMAIL_EXISTS)
        else:
            user = newUser[1]
            token = self.user_model.create_signup_token(user.getId())
            emailTemplateHTML = open("static/verify_mail.html", mode="r").read()
            emailTemplateTXT = open("static/verify_mail.txt", mode="r").read()
            emailContentsHTML = emailTemplateHTML.replace("{USERNAME HERE}", username)
            emailContentsHTML = emailContentsHTML.replace("{EMAIL HERE}", email)
            emailContentsHTML = emailContentsHTML.replace("{SITE ADDR}", ADDR_SITE)
            emailContentsHTML = emailContentsHTML.replace("{VERIFICATION ADDR}", ADDR_VERIFY)
            emailContentsHTML = emailContentsHTML.replace("{TOKEN HERE}", token)
            emailContentsTXT = emailTemplateTXT.replace("{USERNAME HERE}", username)
            emailContentsTXT = emailContentsTXT.replace("{EMAIL HERE}", email)
            emailContentsTXT = emailContentsTXT.replace("{SITE ADDR}", ADDR_SITE)
            emailContentsTXT = emailContentsTXT.replace("{VERIFICATION ADDR}", ADDR_VERIFY)
            emailContentsTXT = emailContentsTXT.replace("{TOKEN HERE}", token)
            mail.send_mail("verify.user.noreply@sec-msg.appspotmail.com", email, "Secure Messaging verification",
                           emailContentsTXT, html=emailContentsHTML)
            userKeys = UserKey(parent=UserKey.newKey(newUser[1].auth_ids[0]))
            userKeys.pubkey = pubkey
            userKeys.privateKey = privateKey
            userKeys.keySalt = salt
            userKeys.put()
            self.response.write(RESPONSE_OK)


class VerifyHandler(BaseHandlerUser):
    def post(self):
        self.get()

    def get(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_TOKEN):
            return
        userName = self.request.get(POST_USER_NAME)
        token = self.request.get(POST_USER_TOKEN)
        user = self.user_model.get_by_auth_id(userName)
        if not user:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        if self.user_model.validate_token(self.user_model.get_by_auth_id(userName).getId(), "signup", token):
            user = self.user_model.get_by_auth_id(userName)
            user.verified = True
            user.put()
            self.user_model.delete_signup_token(user.getId(), token)
            self.response.write(RESPONSE_USER_VERIFIED)
        else:
            self.response.write(RESPONSE_USER_BAD_TOKEN)


class ForgotPasswordHandler(BaseHandlerUser):
    def get(self):
        self.response.write(open("static/forgot_page.html").read())

    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_EMAIL):
            return
        username = self.request.POST[POST_USER_NAME]
        email = self.request.POST[POST_USER_EMAIL]
        user = self.user_model.get_by_auth_id(username)
        if not user:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        if email == user.email:
            token = user.recoveryToken()
            emailTemplateHTML = open("static/recovery_mail.html").read()
            emailTemplateTXT = open("static/recovery_mail.txt").read()
            emailContentsHTML = emailTemplateHTML.replace("{TOKEN HERE}", token)
            emailContentsTXT = emailTemplateTXT.replace("{TOKEN HERE}", token)
            mail.send_mail("password.recovery.noreply@sec-msg.appspotmail.com", email,
                           "Secure messaging password recovery", emailContentsTXT,
                           html=emailContentsHTML)
            self.response.write(RESPONSE_RECOVERY_EMAIL_SENT)
        else:
            self.response.write(RESPONSE_USER_BAD_EMAIL)


class LoginHandler(BaseHandlerUser):
    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_PASSWORD):
            return
        username = self.request.POST[POST_USER_NAME]
        password = self.request.POST[POST_USER_PASSWORD]
        try:
            user = self.user_model.get_by_auth_id(username)
            if not user:
                raise InvalidAuthIdError
            if not user.verified:
                self.response.write(RESPONSE_USER_NOT_VERIFIED)
                return
            self.auth.get_user_by_password(auth_id=username, password=password,
                                           save_session=True, remember=True)
            self.response.write(RESPONSE_OK)
        except InvalidAuthIdError:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
        except InvalidPasswordError:
            self.response.write(RESPONSE_USER_BAD_PASSWORD)


class PasswordRecoveryHandler(BaseHandlerUser):
    def post(self):
        if not self.requireParameters(POST_USER_NAME, POST_USER_PASSWORD, POST_USER_TOKEN, POST_USER_PRIVKEY,
                                      POST_USER_PUBKEY, POST_USER_KEY_SALT):
            return
        username = self.request.POST[POST_USER_NAME]
        password = self.request.POST[POST_USER_PASSWORD]
        token = self.request.POST[POST_USER_TOKEN]
        user = self.user_model.get_by_auth_id(username)
        if not user:
            self.response.write(RESPONSE_USER_BAD_USERNAME)
            return
        if self.user_model.validate_token(user.getId(), "recovery", token):
            user.set_password(password)
            userKeys = UserKey.query(ancestor=UserKey.newKey(user.auth_ids[0])).fetch(1)[0]
            userKeys.privateKey = self.request.POST[POST_USER_PRIVKEY]
            userKeys.pubkey = self.request.POST[POST_USER_PUBKEY]
            userKeys.keySalt = self.request.POST[POST_USER_KEY_SALT]
            userKeys.put()
            user.deleteAllMessages()
            user.deleteRecoveryToken(token)
            self.response.write(RESPONSE_OK)
        else:
            self.response.write(RESPONSE_USER_BAD_TOKEN)


class SetPasswordHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if not self.requireParameters(POST_USER_PASSWORD, POST_USER_PRIVKEY, POST_USER_KEY_SALT):
            return
        password = self.request.POST[POST_USER_PASSWORD]
        self.user.set_password(password)
        userKeys = UserKey.query(ancestor=UserKey.newKey(self.user.auth_ids[0])).fetch(1)[0]
        userKeys.privateKey = self.request.POST[POST_USER_PRIVKEY]
        userKeys.keySalt = self.request.POST[POST_USER_KEY_SALT]
        userKeys.put()
        self.response.write(RESPONSE_OK)


class LogoutHandler(BaseHandlerUser):
    @userRequired
    def get(self):
        self.post()

    def post(self):
        self.auth.unset_session()
        self.response.write(RESPONSE_OK)


class GetPublicKeyHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        if POST_MSG_PEER in self.request.POST:
            userKeys = UserKey.query(ancestor=UserKey.newKey(self.request.POST[POST_MSG_PEER])).fetch(1)
        else:
            userKeys = self.user.getKey()
        if len(userKeys) > 0:
            self.response.write(json.dumps({JSON_KEY: userKeys[0].pubkey}))
        else:
            self.response.write(RESPONSE_MSG_NO_PEER)


class GetPrivateKeyHandler(BaseHandlerUser):
    @userRequired
    def post(self):
        userKeys = self.user.getKey()[0]
        self.response.write(json.dumps({JSON_KEY: userKeys.privateKey, JSON_KEY_SALT: userKeys.keySalt}))
