from webapp2 import RequestHandler, cached_property
from webapp2_extras import sessions, auth
from usermodel import UserModel
import logging
from HTTPStrings import *


class BaseHandlerSession(RequestHandler):
    def dispatch(self):
        self.session_store = sessions.get_store(request=self.request)
        try:
            RequestHandler.dispatch(self)
        except Exception as error:
            postArgs = dict(self.request.POST)
            if POST_USER_PASSWORD in postArgs:
                postArgs.pop(POST_USER_PASSWORD)
            logging.error("Unhandled error "+str(type(error))+" "+str(error)+" "+error.message+" "+str(postArgs))
            self.response.write(RESPONSE_UNHANDLED_ERROR)
        finally:
            self.session_store.save_sessions(self.response)

    def requireParameters(self, *args):
        for parameter in args:
            if not((parameter in self.request.POST) or (parameter in self.request.GET)):
                self.response.write(RESPONSE_MISSING_POST_PARAMETERS)
                return False
        return True

    @cached_property
    def session(self):
        return self.session_store.get_session(backend="datastore")


class BaseHandlerUser(BaseHandlerSession):
    @cached_property
    def auth(self):
        return auth.get_auth()

    @cached_property
    def user_info(self):
        return self.auth.get_user_by_session()

    @cached_property
    def user_model(self):
        return UserModel

    @cached_property
    def user(self):
        if self.user_info:
            return self.user_model.get_by_id(self.user_info["user_id"])
        else:
            return None