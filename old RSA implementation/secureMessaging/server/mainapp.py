from webapp2 import WSGIApplication, RequestHandler
from users import UserAddHandler, LoginHandler, SetPasswordHandler, LogoutHandler, GetPrivateKeyHandler, GetPublicKeyHandler
from users import VerifyHandler, ForgotPasswordHandler, PasswordRecoveryHandler
from messaging import SendMessageHandler, GetMessagesHandler, DeleteMessageHandler
from cronjobs import *
from HTTPStrings import *
import datetime

config = {
    'webapp2_extras.sessions': {
        "secret_key": "nJHhFyP4FRs8PPaG"
    },
    'webapp2_extras.auth': {
        'user_model': 'usermodel.UserModel'
    },
}
epoch = datetime.datetime(year=1970, month=1, day=1, hour=0, minute=0, second=0)


class CurrentTime(RequestHandler):
    def post(self):
        currentTime = datetime.datetime.utcnow()
        unixTime = currentTime - epoch
        self.response.write(str(int(unixTime.total_seconds())))


app = WSGIApplication(routes=[(ADDR_ADD_USER, UserAddHandler),
                              (ADDR_LOGIN, LoginHandler),
                              (ADDR_SET_PASSWORD, SetPasswordHandler),
                              (ADDR_LOGOUT, LogoutHandler),
                              (ADDR_SEND_MESSAGE, SendMessageHandler),
                              (ADDR_RECEIVE_MESSAGE, GetMessagesHandler),
                              (ADDR_DELETE_MESSAGE, DeleteMessageHandler),
                              (ADDR_GET_PUBKEY, GetPublicKeyHandler),
                              (ADDR_GET_PRIVATEKEY, GetPrivateKeyHandler),
                              (ADDR_VERIFY, VerifyHandler),
                              (ADDR_FORGOT, ForgotPasswordHandler),
                              (ADDR_RECOVERY, PasswordRecoveryHandler),
                              (CRON_CLEARTOKENS, ClearTokens),
                              (CRON_CLEARUSERS, ClearUsers),
                              (ADDR_TIME, CurrentTime)],
                      config=config)