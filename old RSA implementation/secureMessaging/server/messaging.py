import json

from basehandler import BaseHandlerUser
import users
from HTTPStrings import *
from messagemodel import MessageModel, MessagePointer


class SendMessageHandler(BaseHandlerUser):
    @users.userRequired
    def post(self):
        if not self.requireParameters(POST_MSG_PEER, POST_MSG_KEY1, POST_MSG_KEY2, POST_MSG_CONTENTS,
                                      POST_MSG_SIGNATURE):
            return
        sender = self.user
        receiver = self.user_model.get_by_auth_id(self.request.POST[POST_MSG_PEER])
        if not receiver:
            self.response.write(RESPONSE_MSG_NO_PEER)
            return
        key1 = self.request.POST[POST_MSG_KEY1]
        key2 = self.request.POST[POST_MSG_KEY2]
        contents = self.request.POST[POST_MSG_CONTENTS]
        signature = self.request.POST[POST_MSG_SIGNATURE]
        message = MessageModel(parent=MessageModel.messageKey(sender.getId()))
        message.create(self.user.auth_ids[0], receiver.auth_ids[0], key1, key2, contents, signature)
        sender.addMessage(message, receiver, sender)
        receiver.addMessage(message, sender, sender)
        self.response.write(RESPONSE_OK)


class GetMessagesHandler(BaseHandlerUser):
    @users.userRequired
    def post(self):
        if POST_MSG_PEER in self.request.POST:
            peer = self.request.POST[POST_MSG_PEER]
            peer = self.user_model.get_by_auth_id(peer)
            if not peer:
                self.response.write(RESPONSE_MSG_NO_PEER)
                return
        else:
            peer = None
        if POST_MSG_COUNT in self.request.POST:
            count = int(self.request.POST[POST_MSG_COUNT])
        else:
            count = 5
        if POST_MSG_CURSOR in self.request.POST:
            cursor = self.request.POST[POST_MSG_CURSOR]
        else:
            cursor = None
        messages = self.user.getMessages(peer, count, cursor)
        messageList = json.dumps(messages)
        self.response.write(messageList)


class DeleteMessageHandler(BaseHandlerUser):
    @users.userRequired
    def post(self):
        if not self.requireParameters(POST_MSG_ID, POST_MSG_PEER):
            return
        messagePointerId = long(self.request.POST[POST_MSG_ID])
        other_end = self.user_model.get_by_auth_id(self.request.POST[POST_MSG_PEER])
        if not other_end:
            self.response.write(RESPONSE_MSG_NO_PEER)
            return
        pointer = MessagePointer.get_by_id(messagePointerId,
                                           parent=MessagePointer.messagePointerKey(self.user, other_end))
        if not pointer:
            self.response.write(RESPONSE_MSG_INVALID)
            return
        message = MessageModel.get_by_id(pointer.message_id, parent=MessageModel.messageKey(pointer.sender))
        if pointer.sender == other_end.getId():
            message.pointerReceiver = False
        else:
            message.pointerSender = False
        message.put()
        if not (message.pointerSender or message.pointerReceiver):
            message.key.delete()
        pointer.key.delete()
        self.response.write(RESPONSE_OK)
