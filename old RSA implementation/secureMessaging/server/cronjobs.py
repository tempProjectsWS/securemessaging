from HTTPStrings import RESPONSE_OK
from webapp2 import RequestHandler
from webapp2_extras.appengine.auth.models import UserToken, Unique
from usermodel import UserModel
import datetime

CRON_CLEARTOKENS = "/cleartokens"
CRON_CLEARUSERS = "/clearusers"


class ClearTokens(RequestHandler):
    def get(self):
        ageLimit = datetime.datetime.now() - datetime.timedelta(days=1)
        targetTokensQuery = UserToken.query(UserToken.created < ageLimit)
        targetTokens = targetTokensQuery.iter()
        for targetToken in targetTokens:
            targetToken.key.delete()
        self.response.write(RESPONSE_OK)


class ClearUsers(RequestHandler):
    def get(self):
        ageLimit = datetime.datetime.now() - datetime.timedelta(days=1)
        targetUsersQuery = UserModel.query(UserModel.updated < ageLimit)
        targetUsers = targetUsersQuery.iter()
        for targetUser in targetUsers:
            if not targetUser.verified:
                Unique.delete_multi(
                    ["UserModel.auth_id:" + targetUser.auth_ids[0], "UserModel.email:" + targetUser.email])
                targetUser.deleteAllMessages()
                targetUser.getKey()[0].key.delete()
                targetUser.key.delete()
        self.response.write(RESPONSE_OK)
