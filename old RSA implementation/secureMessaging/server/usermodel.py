import datetime

from google.appengine.ext import ndb
from webapp2_extras.security import generate_password_hash
from webapp2_extras.appengine.auth.models import User, UserToken
from messagemodel import MessagePointer, MessageModel
from google.appengine.datastore.datastore_query import Cursor
from HTTPStrings import *


class UserModel(User):
    def set_password(self, raw_password):
        self.password = generate_password_hash(raw_password, length=12)
        self.put()

    def addMessage(self, message, other_end, sender):
        message_pointer = MessagePointer(parent=MessagePointer.messagePointerKey(self,
                                                                                 other_end))
        message_pointer.message_id = message.key.id()
        message_pointer.sender = sender.getId()
        epoch = datetime.datetime(1970, 1, 1, 0, 0, 0)

        message_pointer.put()

    def getMessages(self, other_end=None, count=20, cursor=None):
        if other_end:
            message_pointers = MessagePointer.query(ancestor=MessagePointer.messagePointerKey(self, other_end)).order(
                -MessagePointer.timestamp)
        else:
            message_pointers = MessagePointer.query(ancestor=MessagePointer.messagePointerKey(self)).order(
                -MessagePointer.timestamp)
        if cursor:
            message_pointers, cursor, more = message_pointers.fetch_page(count, start_cursor=Cursor(urlsafe=cursor))
        else:
            message_pointers, cursor, more = message_pointers.fetch_page(count)
        messages = []
        for pointer in message_pointers:
            message = MessageModel.get_by_id(pointer.message_id, parent=MessageModel.messageKey(pointer.sender))
            epoch = datetime.datetime.fromtimestamp(0)
            timestamp = pointer.timestamp - epoch
            senderKey = UserKey.query(ancestor=UserKey.newKey(message.sender)).fetch(1)[0]
            messages.append({JSON_MSG_CONTENTS: message.contents,
                             JSON_MSG_KEY1: message.key1,
                             JSON_MSG_KEY2: message.key2,
                             JSON_MSG_RECEIVER: message.receiver,
                             JSON_MSG_SENDER: message.sender,
                             JSON_MSG_ID: pointer.key.id(),
                             JSON_MSG_TIMESTAMP: timestamp.total_seconds(),
                             JSON_MSG_SIGNATURE: message.signature,
                             JSON_MSG_SENDER_KEY: senderKey.pubkey, })
        if cursor:
            return {JSON_MESSAGES: messages, JSON_MESSAGES_CURSOR: cursor.urlsafe(), JSON_MORE_MESSAGES: more}
        else:
            return {JSON_MESSAGES: messages, JSON_MESSAGES_CURSOR: None, JSON_MORE_MESSAGES: False}

    def deleteAllMessages(self):
        messagePointers = MessagePointer.query(ancestor=MessagePointer.messagePointerKey(self)).iter()
        for pointer in messagePointers:
            message = MessageModel.get_by_id(pointer.message_id, parent=MessageModel.messageKey(pointer.sender))
            if pointer.sender == self.getId():
                message.pointerSender = False
            else:
                message.pointerReceiver = False
            message.put()
            if not (message.pointerSender or message.pointerReceiver):
                message.key.delete_async()
            pointer.key.delete_async()

    def getId(self):
        return self.key.id()

    def recoveryToken(self):
        return UserToken.create(self.getId(), "recovery").token

    def deleteRecoveryToken(self, token):
        UserToken.get(self.getId(), "recovery", token).key.delete()

    def getKey(self):
        return UserKey.query(ancestor=UserKey.newKey(self.auth_ids[0])).fetch(1)


class UserKey(ndb.Model):
    pubkey = ndb.TextProperty()
    privateKey = ndb.TextProperty()
    keySalt = ndb.StringProperty()

    @classmethod
    def newKey(cls, user):
        return ndb.Key("USERKEY", user)
