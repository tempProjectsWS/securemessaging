from google.appengine.ext import ndb


class MessageModel(ndb.Model):
    contents = ndb.TextProperty()
    key1 = ndb.TextProperty()
    key2 = ndb.TextProperty()
    sender = ndb.StringProperty()
    receiver = ndb.StringProperty()
    pointerSender = ndb.BooleanProperty()
    pointerReceiver = ndb.BooleanProperty()
    signature = ndb.TextProperty()

    def create(self, sender, receiver, key1, key2, contents, signature):
        self.sender = sender
        self.receiver = receiver
        self.key1 = key1
        self.key2 = key2
        self.contents = contents
        self.pointerSender = True
        self.pointerReceiver = True
        self.signature = signature
        self.put()

    @classmethod
    def messageKey(cls, sender):
        return ndb.Key("MESSAGES", sender)


class MessagePointer(ndb.Model):
    message_id = ndb.IntegerProperty()
    sender = ndb.IntegerProperty()
    timestamp = ndb.DateTimeProperty(auto_now_add=True)

    @classmethod
    def messagePointerKey(cls, owner, other_end=None):
        if other_end:
            return ndb.Key("MESSAGEPOINTERS", owner.getId(), "OTHER_END", other_end.getId())
        else:
            return ndb.Key("MESSAGEPOINTERS", owner.getId())
