SITE_UNCOMMENTED = 'ADDR_SITE = "https://sec-msg.appspot.com"'
LOCAL_COMMENTED = '#ADDR_SITE = "http://localhost:8080"'
SITE_COMMENTED = '#ADDR_SITE = "https://sec-msg.appspot.com"'
LOCAL_UNCOMMENTED = 'ADDR_SITE = "http://localhost:8080"'
file = open("secureMessagingClient/HTTPStrings.py", mode="r")
lines = file.readlines()
file = open("secureMessagingClient/HTTPStrings.py", mode="w")
for line in lines:
    if line.rstrip() == SITE_UNCOMMENTED:
        file.write(SITE_COMMENTED+'\n')
    elif line.rstrip() == LOCAL_COMMENTED:
        file.write(LOCAL_UNCOMMENTED+'\n')
    else:
        file.write(line)
file.close()

