from __future__ import print_function
from secureMessaging import addUser, Session
from secureMessaging.HTTPStuff import HTTPError
from random import randint, seed

USER3 = "atest_user3"

USER2 = "atest_user2"

USER1 = "atest_user1"
print("Adding users")
try:
    addUser(USER1, "nikola825@gmail.com", "pass1")
    addUser(USER2, "amail2", "pass2")
    addUser(USER3, "amail3", "pass3")
except HTTPError:
    print("Users Exist, skipping adduser")
else:
    print("OK")

MESSAGES_1TO2 = []
MESSAGES_1TO3 = []
MESSAGES_2TO1 = []
MESSAGES_2TO3 = []
MESSAGES_3TO1 = []
MESSAGES_3TO2 = []

seed()

print("Generating random messages ")
for i in range(20):
    MESSAGES_1TO2.append(str(randint(500000, 4000000000)))
    MESSAGES_1TO3.append(str(randint(500000, 4000000000)))
    MESSAGES_2TO1.append(str(randint(500000, 4000000000)))
    MESSAGES_2TO3.append(str(randint(500000, 4000000000)))
    MESSAGES_3TO1.append(str(randint(500000, 4000000000)))
    MESSAGES_3TO2.append(str(randint(500000, 4000000000)))

print("OK")
print("Logging in ")
s1 = Session(USER1, "pass1")
s2 = Session(USER2, "pass2")
s3 = Session(USER3, "pass3")
print("OK")

print("Sending ")
for i in range(20):
    s1.sendMessage(MESSAGES_1TO2[i], USER2)
    s1.sendMessage(MESSAGES_1TO3[i], USER3)
    s2.sendMessage(MESSAGES_2TO1[i], USER1)
    s2.sendMessage(MESSAGES_2TO3[i], USER3)
    s3.sendMessage(MESSAGES_3TO1[i], USER1)
    s3.sendMessage(MESSAGES_3TO2[i], USER2)
print("OK")

recvOk = True

print("Receiving ")
messageList12 = s1.getMessages(USER2)
messageList13 = s1.getMessages(USER3)
messageList21 = s2.getMessages(USER1)
messageList23 = s2.getMessages(USER3)
messageList31 = s3.getMessages(USER1)
messageList32 = s3.getMessages(USER2)
print("OK")
print("Testing message integrity")


def recvTest(u1, u2, mlist, startList1, startList2):
    b = True
    print("Testting recv", u1, "<>", u2, " ")
    for messageId in range(10):
        b = b and (mlist[messageId * 2 + 1].contents == startList1[-messageId - 1])
        b = b and (mlist[messageId * 2].contents == startList2[-messageId - 1])

    if b:
        print("OK")
    else:
        print("FAIL")
        exit()


recvTest(1, 2, messageList12, MESSAGES_1TO2, MESSAGES_2TO1)
recvTest(1, 3, messageList13, MESSAGES_1TO3, MESSAGES_3TO1)
recvTest(2, 1, messageList21, MESSAGES_1TO2, MESSAGES_2TO1)
recvTest(2, 3, messageList23, MESSAGES_2TO3, MESSAGES_3TO2)
recvTest(3, 1, messageList31, MESSAGES_1TO3, MESSAGES_3TO1)
recvTest(3, 2, messageList32, MESSAGES_2TO3, MESSAGES_3TO2)

print("Changing password")
s1.setPassword("apassword1")
s2.setPassword("apassword2")
s3.setPassword("apassword3")
print("OK")
print("Logging in again with old password")
try:
    s1 = Session(USER1, "pass1")
    s2 = Session(USER2, "pass2")
    s3 = Session(USER3, "pass3")
except HTTPError:
    print("OK - password not accepted")
else:
    print("FAIL - old password accepted")
print("Logging in with new password")
s1 = Session(USER1, "apassword1")
s2 = Session(USER2, "apassword2")
s3 = Session(USER3, "apassword3")
print("OK")
print("Receiving messages again")
messageList12 = s1.getMessages(USER2)
messageList13 = s1.getMessages(USER3)
messageList21 = s2.getMessages(USER1)
messageList23 = s2.getMessages(USER3)
messageList31 = s3.getMessages(USER1)
messageList32 = s3.getMessages(USER2)
print("OK")
print("Testing message integrity")
recvTest(1, 2, messageList12, MESSAGES_1TO2, MESSAGES_2TO1)
recvTest(1, 3, messageList13, MESSAGES_1TO3, MESSAGES_3TO1)
recvTest(2, 1, messageList21, MESSAGES_1TO2, MESSAGES_2TO1)
recvTest(2, 3, messageList23, MESSAGES_2TO3, MESSAGES_3TO2)
recvTest(3, 1, messageList31, MESSAGES_1TO3, MESSAGES_3TO1)
recvTest(3, 2, messageList32, MESSAGES_2TO3, MESSAGES_3TO2)
print("Reverting old password")
s1.setPassword("pass1")
s2.setPassword("pass2")
s3.setPassword("pass3")
print("OK")
print("Logging in again with old password")
try:
    s1 = Session(USER1, "apassword1")
    s2 = Session(USER2, "apassword2")
    s3 = Session(USER3, "apassword3")
except HTTPError:
    print("OK - password not accepted")
else:
    print("FAIL - old password accepted")
print("Logging in with reverted password")
s1 = Session(USER1, "pass1")
s2 = Session(USER2, "pass2")
s3 = Session(USER3, "pass3")
print("OK")
print("Receiving messages again")
messageList12 = s1.getMessages(USER2)
messageList13 = s1.getMessages(USER3)
messageList21 = s2.getMessages(USER1)
messageList23 = s2.getMessages(USER3)
messageList31 = s3.getMessages(USER1)
messageList32 = s3.getMessages(USER2)
print("OK")
print("Testing message integrity")
recvTest(1, 2, messageList12, MESSAGES_1TO2, MESSAGES_2TO1)
recvTest(1, 3, messageList13, MESSAGES_1TO3, MESSAGES_3TO1)
recvTest(2, 1, messageList21, MESSAGES_1TO2, MESSAGES_2TO1)
recvTest(2, 3, messageList23, MESSAGES_2TO3, MESSAGES_3TO2)
recvTest(3, 1, messageList31, MESSAGES_1TO3, MESSAGES_3TO1)
recvTest(3, 2, messageList32, MESSAGES_2TO3, MESSAGES_3TO2)
print("Testing Delete")


def delRandom(u1, u2, mlist, startList1, startList2, session1, session2, username1, username2):
    print("Selecting random message for", u1, "<>", u2)
    mId = randint(0, 19)
    print("Selected", mId)
    selectedMessage = mlist[mId].contents
    if mId % 2 == 0:
        if selectedMessage == startList2[((-mId) // 2) - 1]:
            print("OK - Message exists now")
        else:
            print("Fail - Couldn't find message")
            exit()
    else:
        if selectedMessage == startList1[((-mId) // 2)]:
            print("OK - Message exists now")
        else:
            print("Fail - Couldn't find message")
            exit()
    print("Deleting")
    session1.deleteMessage(mlist[mId])
    print("OK")
    newList1 = session1.getMessages(username2)
    newList2 = session2.getMessages(username1)
    print("Checking for message")
    found = False
    for m in newList1:
        if m.contents == selectedMessage:
            found = True
            break
    if not found:
        print("OK - Message not found in first user")
    else:
        print("FAIL - Message still exists")
        exit()
    for m in newList2:
        if m.contents == selectedMessage:
            found = True
            break
    if found:
        print("OK - Message found in second user")
    else:
        print("FAIL - Message deleted from second user")
        exit()


delRandom(1, 2, messageList12, MESSAGES_1TO2, MESSAGES_2TO1, s1, s2, USER1, USER2)
delRandom(1, 3, messageList13, MESSAGES_1TO3, MESSAGES_3TO1, s1, s3, USER1, USER3)
delRandom(2, 3, messageList23, MESSAGES_2TO3, MESSAGES_3TO2, s2, s3, USER2, USER3)
print("Cleaning up remainging messages")
messages1 = s1.getMessages(count=100)
messages2 = s2.getMessages(count=100)
messages3 = s3.getMessages(count=100)
for message in messages1:
    s1.deleteMessage(message)
for message in messages2:
    s2.deleteMessage(message)
for message in messages3:
    s3.deleteMessage(message)
print("OK")
print("Checking if messages remain")
messages1 = s1.getMessages(count=100)
messages2 = s2.getMessages(count=100)
messages3 = s3.getMessages(count=100)
if (not messages1) and (not messages2) and (not messages2):
    print("OK")
else:
    print("Messages still exist")