# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWin.ui'
#
# Created: Sat May 17 22:20:47 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(346, 410)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.display = QtGui.QTextBrowser(self.centralwidget)
        self.display.setMinimumSize(QtCore.QSize(0, 200))
        self.display.setAutoFillBackground(False)
        self.display.setObjectName(_fromUtf8("display"))
        self.horizontalLayout_2.addWidget(self.display)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.peer = QtGui.QLineEdit(self.centralwidget)
        self.peer.setObjectName(_fromUtf8("peer"))
        self.horizontalLayout.addWidget(self.peer)
        self.refresh = QtGui.QPushButton(self.centralwidget)
        self.refresh.setObjectName(_fromUtf8("refresh"))
        self.horizontalLayout.addWidget(self.refresh)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.message = QtGui.QTextEdit(self.centralwidget)
        self.message.setMinimumSize(QtCore.QSize(0, 100))
        self.message.setObjectName(_fromUtf8("message"))
        self.horizontalLayout_3.addWidget(self.message)
        self.send = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.send.sizePolicy().hasHeightForWidth())
        self.send.setSizePolicy(sizePolicy)
        self.send.setObjectName(_fromUtf8("send"))
        self.horizontalLayout_3.addWidget(self.send)
        self.gridLayout.addLayout(self.horizontalLayout_3, 2, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 346, 23))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuTools = QtGui.QMenu(self.menubar)
        self.menuTools.setObjectName(_fromUtf8("menuTools"))
        MainWindow.setMenuBar(self.menubar)
        self.logout = QtGui.QAction(MainWindow)
        self.logout.setObjectName(_fromUtf8("logout"))
        self.menuTools.addAction(self.logout)
        self.menubar.addAction(self.menuTools.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Secure messaging", None))
        self.peer.setPlaceholderText(_translate("MainWindow", "Peer", None))
        self.refresh.setText(_translate("MainWindow", "Refresh", None))
        self.send.setText(_translate("MainWindow", "Send", None))
        self.menuTools.setTitle(_translate("MainWindow", "Tools", None))
        self.logout.setText(_translate("MainWindow", "Log out", None))

