# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Verify.ui'
#
# Created: Sat May 17 22:20:47 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(254, 113)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.username = QtGui.QLineEdit(self.centralwidget)
        self.username.setObjectName(_fromUtf8("username"))
        self.verticalLayout.addWidget(self.username)
        self.token = QtGui.QLineEdit(self.centralwidget)
        self.token.setObjectName(_fromUtf8("token"))
        self.verticalLayout.addWidget(self.token)
        self.verify = QtGui.QPushButton(self.centralwidget)
        self.verify.setObjectName(_fromUtf8("verify"))
        self.verticalLayout.addWidget(self.verify)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Verify user", None))
        self.username.setPlaceholderText(_translate("MainWindow", "Username", None))
        self.token.setPlaceholderText(_translate("MainWindow", "Token", None))
        self.verify.setText(_translate("MainWindow", "Verify user", None))

