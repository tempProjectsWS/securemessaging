from PyQt4 import QtGui

from secureMessagingClient import Session, HTTPError
from AddUser import AddUserGui
from Verify import VerifyGui
from LoginWinUi import Ui_MainWindow
from ForgotPassword import ForgotPasswordUi
from RecoverPassword import RecoveryGui


class LoginWinGui(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(LoginWinGui, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.login.clicked.connect(self.login)
        self.ui.login.setDefault(True)
        self.ui.adduser.triggered.connect(self.adduser)
        self.ui.verify.triggered.connect(self.verify)
        self.ui.forgot.triggered.connect(self.forgot)
        self.ui.recovery.triggered.connect(self.recovery)
        self.shortcut = QtGui.QShortcut(QtGui.QKeySequence("Return"), self)
        self.shortcut.activated.connect(self.login)
        self.shortcut2 = QtGui.QShortcut(QtGui.QKeySequence("Up"), self)
        self.shortcut2.activated.connect(self.focusUp)
        self.shortcut2 = QtGui.QShortcut(QtGui.QKeySequence("Down"), self)
        self.shortcut2.activated.connect(self.focusDown)
        #self.close.connect(self.closewin)

    def focusUp(self):
        self.ui.username.setFocus()

    def focusDown(self):
        self.ui.password.setFocus()

    def login(self):
        username = self.ui.username.text()
        password = self.ui.password.text()
        if username == "" or password == "":
            messageBox = QtGui.QMessageBox(parent=self, text="Please enter all information")
            messageBox.show()
            return
        try:
            mainWin=MainWinGui(session=Session(username, password))
            mainWin.show()
            self.close()
        except HTTPError as error:
            messageBox = QtGui.QMessageBox(parent=self, text="Error occured during login: " + str(error))
            messageBox.show()

    def adduser(self):
        addUserGui = AddUserGui(parent=self)
        addUserGui.show()

    def verify(self):
        verifyGui = VerifyGui(parent=self)
        verifyGui.show()

    def forgot(self):
        forgotUi=ForgotPasswordUi(parent=self)
        forgotUi.show()

    def recovery(self):
        recoveryUi=RecoveryGui(parent=self)
        recoveryUi.show()
from MainWin import MainWinGui