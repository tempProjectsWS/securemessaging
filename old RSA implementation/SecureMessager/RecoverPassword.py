from PyQt4 import QtGui

from secureMessagingClient import setNewPassword, HTTPError
from RecoverPasswordUi import Ui_MainWindow


class RecoveryGui(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(RecoveryGui, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.setPassword.clicked.connect(self.setPassword)

    def setPassword(self):
        if self.ui.username.text() and self.ui.token.text() and self.ui.password.text():
            try:
                setNewPassword(self.ui.token.text(), self.ui.username.text(), self.ui.password.text())
                self.ui.setPassword.setText("New password set")
            except HTTPError as error:
                messageBox = QtGui.QMessageBox(parent=self, text="An error occurred " + str(error))
                messageBox.show()
        else:
            messageBox = QtGui.QMessageBox(parent=self, text="Fill all fields")
            messageBox.show()