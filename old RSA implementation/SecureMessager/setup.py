import sys

from cx_Freeze import setup, Executable


# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["PyQt4.QtGui", "PyQt4.QtCore", "inspect", "os", "multiprocessing", "sys", "pickle", "atexit", "hashlib", "hmac", "base64", "Crypto", "urllib", "http", "json"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "SecureMessager",
        version = "0.1",
        description = "Secure messaging",
        options = {"build_exe": build_exe_options},
        executables = [Executable("main.py", base=base, shortcutName="Secure Messaging Client")])