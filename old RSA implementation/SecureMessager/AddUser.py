from PyQt4 import QtGui

from secureMessagingClient import addUser, HTTPError
from AddUserUi import Ui_MainWindow


class AddUserGui(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(AddUserGui, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.adduser.clicked.connect(self.adduser)

    def adduser(self):
        name = self.ui.username.text()
        password = self.ui.password.text()
        confirm = self.ui.confirm.text()
        email = self.ui.email.text()
        if name == "" or password == "" or email == "":
            messageBox = QtGui.QMessageBox(parent=self, text="Enter all information")
            messageBox.show()
            return
        if password != confirm:
            messageBox = QtGui.QMessageBox(parent=self, text="Passwords don't match")
            messageBox.show()
            return
        self.ui.info.setText("Adding user")
        try:
            addUser(name, email, password)
        except HTTPError as error:
            messageBox = QtGui.QMessageBox(parent=self, text="An error occured: " + str(error))
            messageBox.show()
            self.ui.info.setText("")
            return
        self.ui.info.setText("User added")
