from PyQt4 import QtGui

from secureMessagingClient import HTTPError, requestRecovery
from ForgotPasswordUi import Ui_MainWindow


class ForgotPasswordUi(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(ForgotPasswordUi, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.forgot.clicked.connect(self.forgot)
        self.ui.forgot.setDefault(True)
        self.shortcut = QtGui.QShortcut(QtGui.QKeySequence("Return"), self)
        self.shortcut.activated.connect(self.forgot)
        self.shortcut2 = QtGui.QShortcut(QtGui.QKeySequence("Up"), self)
        self.shortcut2.activated.connect(self.focusUp)
        self.shortcut2 = QtGui.QShortcut(QtGui.QKeySequence("Down"), self)
        self.shortcut2.activated.connect(self.focusDown)
        #self.close.connect(self.closewin)

    def focusUp(self):
        self.ui.username.setFocus()

    def focusDown(self):
        self.ui.email.setFocus()

    def forgot(self):
        if self.ui.username.text() and self.ui.email.text():
            try:
                requestRecovery(self.ui.username.text(), self.ui.email.text())
                self.ui.forgot.setText("Request sent")
            except HTTPError as error:
                messageBox = QtGui.QMessageBox(parent=self, text="An error occurred " + str(error))
                messageBox.show()
        else:
            messageBox = QtGui.QMessageBox(parent=self, text="Fill all fields")
            messageBox.show()