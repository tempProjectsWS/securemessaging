# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'LoginWin.ui'
#
# Created: Sat May 17 22:20:47 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(278, 136)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.username = QtGui.QLineEdit(self.centralwidget)
        self.username.setObjectName(_fromUtf8("username"))
        self.verticalLayout.addWidget(self.username)
        self.password = QtGui.QLineEdit(self.centralwidget)
        self.password.setEchoMode(QtGui.QLineEdit.Password)
        self.password.setObjectName(_fromUtf8("password"))
        self.verticalLayout.addWidget(self.password)
        self.login = QtGui.QPushButton(self.centralwidget)
        self.login.setAutoDefault(True)
        self.login.setDefault(True)
        self.login.setObjectName(_fromUtf8("login"))
        self.verticalLayout.addWidget(self.login)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 278, 23))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuTools = QtGui.QMenu(self.menubar)
        self.menuTools.setObjectName(_fromUtf8("menuTools"))
        MainWindow.setMenuBar(self.menubar)
        self.adduser = QtGui.QAction(MainWindow)
        self.adduser.setObjectName(_fromUtf8("adduser"))
        self.verify = QtGui.QAction(MainWindow)
        self.verify.setObjectName(_fromUtf8("verify"))
        self.forgot = QtGui.QAction(MainWindow)
        self.forgot.setObjectName(_fromUtf8("forgot"))
        self.recovery = QtGui.QAction(MainWindow)
        self.recovery.setObjectName(_fromUtf8("recovery"))
        self.menuTools.addAction(self.adduser)
        self.menuTools.addAction(self.verify)
        self.menuTools.addAction(self.forgot)
        self.menuTools.addAction(self.recovery)
        self.menubar.addAction(self.menuTools.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Secure Messaging Login", None))
        self.username.setPlaceholderText(_translate("MainWindow", "Username", None))
        self.password.setPlaceholderText(_translate("MainWindow", "Password", None))
        self.login.setText(_translate("MainWindow", "Login", None))
        self.menuTools.setTitle(_translate("MainWindow", "Tools", None))
        self.adduser.setText(_translate("MainWindow", "Add user", None))
        self.verify.setText(_translate("MainWindow", "Verify user", None))
        self.forgot.setText(_translate("MainWindow", "Forgot password", None))
        self.recovery.setText(_translate("MainWindow", "Password recovery", None))

