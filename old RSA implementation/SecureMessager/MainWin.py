from html import escape

from PyQt4 import QtGui

from secureMessagingClient import HTTPError
from MainWinUi import Ui_MainWindow
from LoginWin import LoginWinGui


class MainWinGui(QtGui.QMainWindow):
    def __init__(self, session, parent=None):
        super(MainWinGui, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.session = session
        self.setWindowTitle("Secure messaging: " + self.session.username)
        self.messages = {"MESSAGES": [], "CURSOR": None, "MORE": False}

        self.ui.refresh.clicked.connect(self.refresh)
        self.ui.send.clicked.connect(self.send)
        self.ui.logout.triggered.connect(self.logout)
        self.ui.scrollBar = self.ui.display.verticalScrollBar()
        self.ui.scrollBar.valueChanged.connect(self.drag)

        self.refreshShortcut = QtGui.QShortcut(QtGui.QKeySequence("F5"), self)
        self.refreshShortcut.activated.connect(self.refresh)

        self.setPeerShortcut = QtGui.QShortcut(QtGui.QKeySequence("Return"), self)
        self.setPeerShortcut.activated.connect(self.refresh)
        self.setPeerShortcut.setEnabled(False)

        self.ui.peer.focusInEvent = self.peerFieldFocused
        self.ui.peer.focusOutEvent = self.peerFieldUnfocused

        self.refreshing = False

    def drag(self, e):
        if (not self.refreshing) and self.messages["MORE"] and e == 0:
            self.refreshing = True
            peer = self.ui.peer.text()
            try:
                if peer == "":
                    newData = self.session.getMessages(count=10, cursor=self.messages["CURSOR"])
                else:
                    newData = self.session.getMessages(peer, count=5, cursor=self.messages["CURSOR"])
            except HTTPError as error:
                messageBox = QtGui.QMessageBox(parent=self, text="Error: " + str(error))
                messageBox.show()
                return
            if newData["MESSAGES"]:
                newData["MESSAGES"].reverse()
                self.messages["MESSAGES"] = newData["MESSAGES"] + self.messages["MESSAGES"]
                self.messages["CURSOR"] = newData["CURSOR"]
                self.messages["MORE"] = newData["MORE"]
            self.refreshDisplay()
            self.ui.scrollBar.setValue(0)
            self.refreshing = False

    def peerFieldFocused(self, e):
        self.setPeerShortcut.setEnabled(True)

    def peerFieldUnfocused(self, e):
        self.setPeerShortcut.setEnabled(False)

    def refreshDisplay(self):
        outputText = '<table width="100%" cellpadding="5" border="" style="padding:15px;width:100%;">'
        for message in self.messages["MESSAGES"]:
            contents = message.contents
            contents = escape(contents)
            contents = contents.replace('\n', "<br/>")
            if message.sender == self.session.username:
                outputText += '<tr style="background-color:#cadced;color:#000000"><td>'
            else:
                outputText += '<tr style="background-color:#FFFFFF;color:#000000"><td>'
            outputText += "<b>" + message.sender + "</b><br/>"
            outputText += contents
            outputText += "<br/> <div style='font-size:small 'align='right'><i>" + message.getTimeStamp() + \
                          "</i></div>"
            outputText += "</td></tr>"
        outputText += "</table>"
        opr = self.refreshing
        self.refreshing = True
        self.ui.display.setHtml(outputText)
        self.refreshing = opr

    def refresh(self):
        peer = self.ui.peer.text()
        try:
            if peer == "":
                self.messages = self.session.getMessages(count=10)
            else:
                self.messages = self.session.getMessages(peer, count=5)
        except HTTPError as error:
            messageBox = QtGui.QMessageBox(parent=self, text="Error: " + str(error))
            messageBox.show()
            return
        self.messages["MESSAGES"].reverse()
        self.refreshDisplay()
        self.ui.scrollBar.setValue(self.ui.scrollBar.maximum())

    def send(self):
        sagovornik = self.ui.peer.text()
        sadrzaj = self.ui.message.toPlainText()
        if sagovornik == "" or sadrzaj == "":
            messageBox = QtGui.QMessageBox(parent=self, text="Please choose peer")
            messageBox.show()
            return
        self.session.sendMessage(sadrzaj, sagovornik)
        self.refreshing = True
        self.refresh()
        self.refreshing = False

    def logout(self):
        self.session.logout()
        loginWin = LoginWinGui(parent=self)
        loginWin.show()
        self.hide()
