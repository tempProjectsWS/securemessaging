from PyQt4 import QtGui

from secureMessagingClient import verifyUser, HTTPError
from VerifyUi import Ui_MainWindow


class VerifyGui(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(VerifyGui, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.verify.clicked.connect(self.verify)

    def verify(self):
        name = self.ui.username.text()
        token = self.ui.token.text()
        if name == "" or token == "":
            messageBox = QtGui.QMessageBox(parent=self, text="Enter all information")
            messageBox.show()
        try:
            verifyUser(name, token)
        except HTTPError as error:
            messageBox = QtGui.QMessageBox(parent=self, text="An error occured: " + str(error))
            messageBox.show()
            return
        self.ui.verify.setText("User verified")