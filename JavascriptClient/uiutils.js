var UiUtils={
    handleError: function(e)
    {
        UiUtils.progressStop();
        $("#errorText").text(e);
        $("#errorDialog").dialog({buttons:{"OK":function(){$("#errorDialog").dialog("close");}}});;
    },
    displayMessage: function(e)
    {
        UiUtils.progressStop();
        $("#messageText").text(e);
        $("#messageDialog").dialog({buttons:{"OK":function(){$("#messageDialog").dialog("close");}}});;
    },
    progressStart: function()
    {
        $(".progressBar").progressbar("option", "value", false);
        $(".progressBar").css("visibility", "visible");
    },
    progressStop: function()
    {
        $(".progressBar").progressbar("option", "value", 0);
        $(".progressBar").css("visibility", "hidden");
    },
    progressReport: function(value)
    {
        $(".progressBar").progressbar("option", "value", value);
        $(".progressBar").progressbar("option", "max", 100);
        $(".progressBar").css("visibility", "visible");
    },
    showModalForm: function(name, submitText, callback)
    {
        var dialogButtons =
            {
                "Cancel": function()
                {
                    modalForm.dialog("close");
                },
            };
        dialogButtons[submitText]=callback;
        var modalForm = $(name).dialog(
        {
            width: "auto",
            closeOnEscape: true,
            modal:true,
            buttons:dialogButtons
        });
        modalForm.keyup(function(keyPressed)
        {
            if(keyPressed.which==13) callback();
        });
        return modalForm;
    }
};