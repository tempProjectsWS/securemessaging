var currentSession = null;
var signingKey = null;
var shiftDown = false;
var windowFocused = true;

$(document).ready(function() {
    $(".modalDialog").hide();
    $("button").button();
    $(".progressBar").progressbar({
        value: 0
    });
    UiUtils.progressStop();
    var currentSessionString = sessionStorage.getItem("currentSession");
    if (currentSessionString) {
        currentSession = Session.fromJSON(currentSessionString);
        loggedIn();
    } else {
        showLoginDialog();
    }
    $(window).focusin(function() {
        windowFocused = true
    });
    $(window).focusout(function() {
        windowFocused = false
    });
});
$(document).keydown(function(k) {
    if (k.which == 116) {
        refreshMessages();
        return false;
    }
    return true;
});

function generateActiveConversations() {
    var peerList = [],
        contentList = [],
        isReadList = [];
    for (var i = MessageStorage.messages.length - 1; i >= 0; i--) {
        if (peerList.indexOf(MessageStorage.messages[i].sender) == -1 &&
            MessageStorage.messages[i].sender != currentSession.username) {
            peerList.push(MessageStorage.messages[i].sender);
            contentList.push(MessageStorage.messages[i].contents.substr(0, 20));
            isReadList.push(MessageStorage.messages[i].isRead);
        } else if (peerList.indexOf(MessageStorage.messages[i].receiver) == -1 &&
            MessageStorage.messages[i].receiver != currentSession.username) {
            peerList.push(MessageStorage.messages[i].receiver);
            contentList.push(MessageStorage.messages[i].contents.substr(0, 20));
            isReadList.push(MessageStorage.messages[i].isRead);
        }
    }
    return [peerList, contentList, isReadList];
}

function escapeHTML(s) {
    var k = s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
        .replace(
            "\n", "<br/>");
    while (k.indexOf("\n") != -1) {
        k = k.replace("\n", "<br/>");
    }
    return k;
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function showLoginDialog() {
    $("#loginDialog").dialog({
        closeOnEscape: false,
        modal: true,
        width: "auto",
        buttons: {
            "Register": RegisterDialog.show,
            "Forgot password": ForgotPasswordDialog.show,
            "Log in": function() {
                doLogin();
            }
        }
    }).keyup(function(e) {
        if (e.which == 13) doLogin();
    });
}

function doLogin() {
    UiUtils.progressStart();
    currentSession = new Session();
    currentSession.init($("#username").val(), $("#password").val(), function() {
        $("#loginDialog").dialog("close");
        loggedIn();
    }, UiUtils.handleError);
}

function loggedIn() {
    sessionStorage.setItem("currentSession", JSON.stringify(currentSession));
    MessageStorage.messages = MessageStorage.getMessages(currentSession.username);
    MessageStorage.sortMessages();
    $("#refreshButton").click(refreshMessages);
    $("#sendButton").click(sendMessage);
    $("#logoutButton").click(logout);
    $("#signMessagesButton").click(SigningDialogs.toggleSigning);
    $("#buttons").buttonset();
    $("#conversationList").menu();
    $("#setPasswordButton").click(SetPasswordDialog.show);
    $("#uploadFileButton").click(FileUploadDialog.show);
    currentSession.registerForChannel(function(message) {
        if (message == currentSession.username) refreshMessages();
    }, UiUtils.handleError);
    $("#messageInput").keydown(function(keyPressed) {
        if (keyPressed.which == 16) {
            shiftDown = true;
            return false;
        }
        if (keyPressed.which == 13) {
            return false;
        }
        return true;
    });
    $("#messageInput").keyup(function(keyPressed) {
        if (keyPressed.which == 16) {
            shiftDown = false;
            return false;
        }
        if (keyPressed.which == 13) {
            if (shiftDown) {
                $("#messageInput").val($("#messageInput").val() + "\n");
            } else sendMessage();
            return false;
        }
        return true;
    });
    refreshMessages();

}

function logout() {
    sessionStorage.removeItem("currentSession")
    currentSession.logout();
    location.reload(false);
}

function refreshMessages() {
    if (currentSession != null && currentSession.loggedIn) {
        UiUtils.progressStart();
        currentSession.getMessages(messagesReceived, UiUtils.handleError);
    }
}

function messagesReceived(received) {
    MessageStorage.messages = MessageStorage.messages.concat(received);
    MessageStorage.sortMessages();
    MessageStorage.storeMessages(currentSession.username, MessageStorage.messages);
    displayMessages();
}

function displayMessages(dontScroll) {
    UiUtils.progressStop();
    $("#display").html("");
    if (!$("#peer").val()) {
        $("#peer").prop("disabled", false);
    } else $("#peer").prop("disabled", true);
    for (var i = 0; i < MessageStorage.messages.length; i++) {

        if (MessageStorage.messages[i].receiver == $("#peer").val() ||
            MessageStorage.messages[i].sender == $("#peer").val()) {
            if (windowFocused)
                MessageStorage.messages[i].isRead = true;
            var footerHtml, messageHtml;
            footerHtml = $(Templates.messageFooter.replace("{TIMESTAMP}", new moment(
                MessageStorage.messages[i].time).format("llll")));
            footerHtml.children(".deleteMessageButton").click(i,
                handleDeleteClick);
            if (MessageStorage.messages[i].signature) {
                footerHtml.children(".checkSignatureButton").click(i,
                    checkMessageSignature);
            } else footerHtml.children(".checkSignatureButton").hide();
            if (MessageStorage.messages[i].isFile) {
                messageHtml = $(Templates.receivedFileMessage.replace(
                    "{CONTENT}",
                    escapeHTML(MessageStorage.messages[i].contents)));
                messageHtml.children(".fileLink").click(i, downloadFile);
            } else if (MessageStorage.messages[i].receiver == $("#peer").val()) {
                messageHtml = $(Templates.sentMessage.replace("{CONTENT}",
                    escapeHTML(
                        MessageStorage.messages[i].contents)));
            } else {
                messageHtml = $(Templates.receivedMessage.replace("{CONTENT}",
                    escapeHTML(MessageStorage.messages[i].contents)));
            }
            messageHtml.append(footerHtml);
            $("#display").append(messageHtml);
        }
    }
    refreshMenus();
    MessageStorage.storeMessages(currentSession.username, MessageStorage.messages);
    if (!dontScroll) $("#display").scrollTop($("#display")[0].scrollHeight);
}

function checkMessageSignature(messageId) {
    SigningDialogs.checkMessageSignature(MessageStorage.messages[messageId.data]);
}

function downloadFile(messageId) {
    UiUtils.progressStart();
    currentSession.downloadFile(MessageStorage.messages[messageId.data],
        fileDownloaded, UiUtils.handleError,
        function(val) {
            UiUtils.progressReport(Math.floor(val * 100))
        });
}

function handleDeleteClick(messageId) {
    MessageStorage.deleteMessage(messageId.data);
    MessageStorage.sortMessages();
    MessageStorage.storeMessages(currentSession.username, MessageStorage.messages);
    displayMessages(true);
}

function fileDownloaded(fileContents, fileName) {
    UiUtils.progressStop();
    var fileBytesArray = new Uint8Array(fileContents.length);
    for (var i = 0; i < fileContents.length; i++) {
        fileBytesArray[i] = fileContents.charCodeAt(i);
    }
    var fileBlob = new Blob([fileBytesArray], {
        type: "application/octet-stream"
    });
    saveAs(fileBlob, fileName);
}

function refreshMenus() {
    var conversations = generateActiveConversations();
    $("#peer").autocomplete({
        source: conversations[0]
    });
    $("#conversationList").html("");
    $("#conversationList").menu("destroy");

    var newMenuItem = $(Templates.newConversationMenuItem);
    newMenuItem.click("_", function(peer) {
        filterMessages(peer.data);
    });
    $("#conversationList").append(newMenuItem);

    var unreadCount = 0;
    for (var i = 0; i < conversations[0].length; i++) {
        var newMenuItem = $(Templates.conversationMenuItem.replace("{PEER}",
                conversations[0][i])
            .replace("{CONTENT}", conversations[1][i]));
        newMenuItem.click(conversations[0][i], function(peer) {
            filterMessages(peer.data);
        });
        if (!conversations[2][i]) {
            newMenuItem.css("font-weight", "bold");
            unreadCount++;
        }
        $("#conversationList").append(newMenuItem);
    }
    $("#conversationList").menu();
    showUnreadIndicator(unreadCount);
}

function showUnreadIndicator(unreadCount) {
    if (unreadCount) {
        document.title = "CryptoChat (" + unreadCount + ")";
    } else {
        document.title = "CryptoChat";
    }
}

function filterMessages(peer) {
    if (peer != "_") {
        $("#peer").prop("disabled", true);
        $("#peer").val(peer);
    } else {
        $("#peer").prop("disabled", false);
        $("#peer").val("");
    }
    displayMessages();
}

function sendMessage() {
    if (!$("#peer").val()) {
        UiUtils.handleError("Please, fill in the contact name");
        return;
    }
    if (!$("#messageInput").val()) return;
    UiUtils.progressStart();
    currentSession.sendMessage($("#messageInput").val(), $("#peer").val(),
        signingKey, messageSent, UiUtils.handleError);
}

function messageSent(msg) {
    msg.isRead = true;
    MessageStorage.messages.push(msg);
    $("#messageInput").val("");
    MessageStorage.sortMessages();
    MessageStorage.storeMessages(currentSession.username, MessageStorage.messages);
    displayMessages();
}
