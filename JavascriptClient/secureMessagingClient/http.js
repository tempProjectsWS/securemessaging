var HTTPStuff={
    sendRequest: function(address, parameters, successCallback, errorCallback, doGet, progressCallback)
    {
        var type="POST";
        if(doGet) type="GET";
        $.ajax(HTTPStrings.ADDR_SITE+address,{
            method:type,
            success:function()
            {
                if(HTTPStuff.responseBad(arguments[0])) {if(errorCallback!=null) errorCallback(arguments[0]);}
                else if(successCallback!=null) successCallback(arguments[0]);
            },
            error:function()
            {
                if(errorCallback!=null) errorCallback("NETWORK ERROR");
            },
            data: parameters,
            timeout: 10000,
            dataType: "text",
            xhr: function()
            {
                var xhr = jQuery.ajaxSettings.xhr();
                if(progressCallback)
                {
                    xhr.addEventListener('progress', function (info) {
                        if(info.lengthComputable)
                        {
                            progressCallback(info.loaded/info.total);
                        }

                    }, false); 
                }
                return xhr;
            }
        })           
    },
    sendFileUpload: function(address, data, contentType, successCallback, errorCallback, progressCallback)
    {
        var request = $.ajax(address,{
            method:"POST",
            contentType: contentType,
            success:function()
            {
                if(HTTPStuff.responseBad(arguments[0])) {if(errorCallback!=null) errorCallback(arguments[0]);}
                else if(successCallback!=null) successCallback(arguments[0]);
            },
            error:function()
            {
                if(errorCallback!=null) errorCallback("NETWORK ERROR");
            },
            data: data,
            timeout: 100000,
            dataType: "text",
            xhr: function()
            {
                var xhr = jQuery.ajaxSettings.xhr();
                if(progressCallback)
                {
                    xhr.upload.addEventListener('progress', function (info) {
                        if(info.lengthComputable)
                        {
                            progressCallback(info.loaded/info.total);
                        }

                    }, false);  
                }
                return xhr;
            }
        });   
    },
    responseBad: function(response)
    {
        return HTTPStrings.errorResponses.indexOf(response) != -1;
    },
    generateFileUploadData: function(fileName, fileContents, peer)
    {
        var boundary = Cryptography.randomString(20);
        var contentType = "multipart/form-data; boundary="+boundary;
        var partBoundary = "--"+boundary;
        var formContents = [];
        formContents=formContents.concat(["",
                                          partBoundary,
                                          'Content-Disposition: form-data; name="'+HTTPStrings.POST_UPLOADED_FILE+'"; filename="' + fileName + '"',
                                          "Content-Type: application/octet-stream",
                                          "",
                                          fileContents,
                                          partBoundary
                                          ]);
        formContents=formContents.concat(['Content-Disposition: form-data; name="' +HTTPStrings.POST_MSG_PEER+'"',
                                          "",
                                          peer,
                                          partBoundary]);
        formContents.push("--"+boundary+"--");
        var requestBody = formContents.join("\r\n");
        return [requestBody, contentType];
    }
};