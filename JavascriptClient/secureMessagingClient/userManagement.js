var UserManagement={
    requestNewToken: function(username, password, successCallback, errorCallback)
    {
        var private = Cryptography.generateDHPrivate();
        var public = Cryptography.calculateDHPublic(private);
        var params = {};
        params[HTTPStrings.POST_USER_NAME]=username;
        params[HTTPStrings.POST_USER_PASSWORD]=password;
        params[HTTPStrings.POST_USER_PUBKEY]=public;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_AUTHORIZE_NEW_TOKEN, params, 
                    function(token)
                    {
            successCallback(private, public, token);
        }, errorCallback);
    },
    registerNewUser: function(username, password, email, successCallback, errorCallback)
    {
        var params={};
        params[HTTPStrings.POST_USER_NAME]=username;
        params[HTTPStrings.POST_USER_PASSWORD]= password;
        params[HTTPStrings.POST_USER_EMAIL]=email;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_ADD_USER, params, successCallback, errorCallback);
    },
    forgotPassword: function(username, email, successCallback, errorCallback)
    {
        var params={};
        params[HTTPStrings.POST_USER_NAME]=username;
        params[HTTPStrings.POST_USER_EMAIL]=email;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_FORGOT, params, successCallback, errorCallback);
    }
};