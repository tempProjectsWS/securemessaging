function mergeObjects(mergeFrom, mergeInto)
{
    for(attr in mergeFrom)
    {
        if(!(attr in mergeInto))
        {
            mergeInto[attr]=mergeFrom[attr];
        }
    }
    return mergeInto;
}
var DataStorage={
    storeItem: function(key, item)
    {
        localStorage.setItem(key, item);
    },
    getItem: function(key)
    {
        return localStorage.getItem(key);
    },
    storePublicKey: function(username, publicKey)
    {
        DataStorage.storeItem("PUK_"+username, publicKey);
    },
    storePrivateKey: function(username, privKey)
    {
        DataStorage.storeItem("PRK_"+username, privKey);
    },
    storeToken: function(username, token)
    {
        DataStorage.storeItem("TKN_"+username, token);
    },
    getPublicKey: function(username)
    {
        return DataStorage.getItem("PUK_"+username);
    },
    getPrivateKey: function(username)
    {
        return DataStorage.getItem("PRK_"+username);
    },
    getToken: function(username)
    {
        return DataStorage.getItem("TKN_"+username);
    },
};