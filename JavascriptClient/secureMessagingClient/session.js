var Session = function() {};
Session.prototype = {

    doStore: function() {
        DataStorage.storeToken(this.username, this.token);
        DataStorage.storePublicKey(this.username, this.public);
        DataStorage.storePrivateKey(this.username, this.private);
    },
    login: function(successCallback, errorCallback) {
        var self = this;
        var params = {};
        params[HTTPStrings.POST_USER_NAME] = this.username;
        params[HTTPStrings.POST_USER_PASSWORD] = this.password;
        params[HTTPStrings.POST_USER_TOKEN] = this.token;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_LOGIN, params,
            function() {
                self.updateKeys(successCallback, errorCallback);
            },
            function(response) {
                if (response == HTTPStrings.RESPONSE_UNAUTHORIZED) {
                    self.resetTokenAndRelogin(successCallback,
                        errorCallback);
                } else errorCallback(response);
            });
    },
    resetTokenAndRelogin: function(successCallback, errorCallback) {
        var self = this;
        UserManagement.requestNewToken(this.username, this.password,
            function(private, public, token) {
                self.token = token;
                self.public = public;
                self.private = private;
                self.doStore();
                self.login(successCallback, errorCallback);
            }, errorCallback);
    },
    loginSuccess: function(successCallback, errorCallback) {
        this.loggedIn = true;
        successCallback();
    },
    getTimeDiff: function(successCallback, errorCallback) {
        var self = this;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_TIME, {}, function(time) {
            var serverTime = parseInt(time);
            self.timeDiff = serverTime - Math.round(new Date().getTime() /
                1000);
            self.loginSuccess(successCallback, errorCallback);
        }, errorCallback);
    },
    updateKeys: function(successCallback, errorCallback) {
        var self = this;
        var newPrivate = Cryptography.generateDHPrivate();
        var newPublic = Cryptography.calculateDHPublic(newPrivate);
        var params = {};
        params[HTTPStrings.POST_USER_PUBKEY] = newPublic;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_UPDATE_PUBKEY, params,
            function() {
                self.oldPrivate = self.private;
                self.oldPublic = self.public;
                self.private = newPrivate;
                self.public = newPublic;
                self.doStore();
                self.getTimeDiff(successCallback, errorCallback);
            }, errorCallback);
    },
    init: function(username, password, successCallback, errorCallback) {
        var self = this;
        this.loggedIn = false;
        this.username = username;
        this.password = password;
        if (DataStorage.getToken(username) && DataStorage.getPublicKey(
                username) && DataStorage.getPrivateKey(username)) {
            this.token = DataStorage.getToken(username);
            this.public = DataStorage.getPublicKey(username);
            this.private = DataStorage.getPrivateKey(username);
            this.doStore();
            this.login(successCallback, errorCallback);
        } else {
            UserManagement.requestNewToken(username, password, function(
                private, public, token) {
                self.token = token;
                self.public = public;
                self.private = private;
                self.doStore();
                self.login(successCallback, errorCallback);
            }, errorCallback);
        }
    },
    logout: function(successCallback, errorCallback) {
        this.loggedIn = false;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_LOGOUT, {},
            successCallback, errorCallback);
    },
    unauthorize: function(successCallback, errorCallback) {
        HTTPStuff.sendRequest(HTTPStrings.ADDR_UNAUTHORIZE, {},
            successCallback, errorCallback);
    },
    setPassword: function(newPassword, successCallback, errorCallback) {
        var self = this;
        var params = {};
        params[HTTPStrings.POST_USER_PASSWORD] = newPassword;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_SET_PASSWORD, params,
            function(response) {
                self.password = newPassword;
                successCallback();
            }, errorCallback);
    },
    getPublicKey: function(peer, successCallback, errorCallback) {
        var params = {};
        params[HTTPStrings.POST_MSG_PEER] = peer;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_GET_PUBKEY, params,
            successCallback, errorCallback);
    },
    sendMessage: function(contents, peer, signingKey, successCallback,
        errorCallback) {
        var self = this;
        this.getPublicKey(peer, function(key) {
            var params = MessageHandling.generatePOSTParameters(
                contents, peer, key, self.timeDiff, self.username,
                signingKey);
            HTTPStuff.sendRequest(HTTPStrings.ADDR_SEND_MESSAGE,
                params[0],
                function() {
                    successCallback(params[1]);
                }, errorCallback);
        }, errorCallback);
    },
    getMessages: function(successCallback, errorCallback, alreadyGotten) {
        var self = this;
        var params = {};
        params[HTTPStrings.POST_SUPPORTS_FILES] = "true";
        HTTPStuff.sendRequest(HTTPStrings.ADDR_RECEIVE_MESSAGE, params,
            function(messagesJSON) {
                var messages = JSON.parse(messagesJSON);
                var more = messages[HTTPStrings.JSON_MORE_MESSAGES];
                var messagesList = messages[HTTPStrings.JSON_MESSAGES];
                messages = messagesList;
                if (alreadyGotten) messages = messages.concat(
                    alreadyGotten);
                if (more) {
                    self.getMessages(self, successCallback,
                        errorCallback, messages);
                } else {
                    successCallback(MessageHandling.parseMessageList(
                        messages, self.public, self.private,
                        self.oldPublic, self.oldPrivate));
                }
            }, errorCallback);
    },
    uploadFile: function(fileName, fileContents, peer, successCallback,
        errorCallback, progressCallback) {
        var self = this;
        this.getPublicKey(peer, function(key) {
            self.requestUploadAddress(function(address) {
                var fileJSON = MessageHandling.generateFileUploadJSON(
                    fileContents, peer, key, self.timeDiff
                );
                var requestData = HTTPStuff.generateFileUploadData(
                    fileName, fileJSON, peer);
                HTTPStuff.sendFileUpload(address,
                    requestData[0], requestData[1],
                    successCallback, errorCallback,
                    progressCallback);
            }, errorCallback);
        }, errorCallback);

    },
    requestUploadAddress: function(successCallback, errorCallback) {
        HTTPStuff.sendRequest(HTTPStrings.ADDR_REQUEST_FILE_UPLOAD, {},
            successCallback, errorCallback);
    },
    downloadFile: function(fileMessage, successCallback, errorCallback,
        progressCallback) {
        var params = {};
        params[HTTPStrings.POST_FILE_KEY] = fileMessage.fileKey;
        HTTPStuff.sendRequest(HTTPStrings.ADDR_DOWNLOAD_FILE, params,
            function(fileJSON) {
                successCallback(MessageHandling.decryptMessageFile(
                    fileJSON, fileMessage), fileMessage.fileName);

            }, errorCallback, false, progressCallback);
    },
    registerForChannel: function(messageCallback, errorCallback) {
        HTTPStuff.sendRequest(HTTPStrings.ADDR_REGISTER_CHANNEL, {},
            function(channelToken) {
                var channel = new goog.appengine.Channel(
                    channelToken);
                channel.open().onmessage = function(msg) {
                    messageCallback(msg.data);
                };
            }, errorCallback)
    }
};
Session.fromJSON = function(jsonString) {
    return mergeObjects(new Session(), JSON.parse(jsonString));
};
