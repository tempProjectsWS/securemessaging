var Cryptography = {
    numberMapping: [],
    getRandomBytes: function(length) {
        return forge.random.getBytesSync(length);
    },
    b64Encode: function(bytes) {
        return forge.util.encode64(bytes);
    },
    b64Decode: function(data) {
        return forge.util.decode64(data);
    },
    utf8Encode: function(data) {
        return forge.util.encodeUtf8(data);
    },
    utf8Decode: function(data) {
        return forge.util.decodeUtf8(data);
    },
    numberToBytes: function(number) {
        var bytes = "";
        while (!number.equals(BigInteger.ZERO)) {
            var addedByte = number.mod(Cryptography.numberMapping[256]);
            bytes = String.fromCharCode(addedByte) + bytes;
            number = number.divide(Cryptography.numberMapping[256])
        }
        return bytes;
    },
    bytesToNumber: function(bytes) {
        var number = new BigInteger("0", 10);
        for (var i = 0; i < bytes.length; i++) {
            number = number.multiply(Cryptography.numberMapping[256]);

            number = number.add(Cryptography.numberMapping[bytes.charCodeAt(
                i)]);
        }
        return number;
    },
    stringToNumber: function(text) {
        return Cryptography.bytesToNumber(Cryptography.b64Decode(text));
    },
    numberToString: function(number) {
        return Cryptography.b64Encode(Cryptography.numberToBytes(number));
    },
    DEFAULT_DH_MODULUS: new BigInteger(
        "1044388881413152506679602719846529545831269060992135009022588756444338172022322690710444046669809783930111585737890362691860127079270495454517218673016928427459146001866885779762982229321192368303346235204368051010309155674155697460347176946394076535157284994895284821633700921811716738972451834979455897010306333468590751358365138782250372269117968985194322444535687415522007151638638141456178420621277822674995027990278673458629544391736919766299005511505446177668154446234882665961680796576903199116089347634947187778906528008004756692571666922964122566174582776707332452371001272163776841229318324903125740713574141005124561965913888899753461735347970011693256316751660678950830027510255804846105583465055446615090444309583050775808509297040039680057435342253926566240898195863631588888936364129920059308455669454034010391478238784189888594672336242763795138176353222845524644040094258962433613354036104643881925238489224010194193088911666165584229424668165441688927790460608264864204237717002054744337988941974661214699689706521543006262604535890998125752275942608772174376107314217749233048217904944409836238235772306749874396760463376480215133461333478395682746608242585133953883882226786118030184028136755970045385534758453247",
        10),
    DEFAULT_DH_GENERATOR: new BigInteger("2", 10),
    generateDHPrivate: function() {
        return this.b64Encode(Cryptography.getRandomBytes(64));
    },
    calculateDHPublic: function(privKey) {
        var exponent = Cryptography.stringToNumber(privKey);
        var result = Cryptography.DEFAULT_DH_GENERATOR.modPow(exponent,
            Cryptography.DEFAULT_DH_MODULUS);
        return Cryptography.numberToString(result);
    },
    calculateDHAES: function(publicKey, privKey) {
        var base = Cryptography.stringToNumber(publicKey);
        var exponent = Cryptography.stringToNumber(privKey);
        var result = base.modPow(exponent, Cryptography.DEFAULT_DH_MODULUS);
        var keySeed = Cryptography.numberToBytes(result);
        var keyBytes = [];
        for (var i = 0; i < keySeed.length; i++) {
            keyBytes[i % 16] = keyBytes[i % 16] ^ keySeed.charCodeAt(i);
        }
        var key = "";
        for (var i = 0; i < 16; i++) {
            key += String.fromCharCode(keyBytes[i]);
        }
        return Cryptography.b64Encode(key);
    },
    AESEncrypt: function(data, key, dontEncodeUtf) {
        if (!dontEncodeUtf) data = Cryptography.utf8Encode(data);
        key = Cryptography.b64Decode(key);
        var iv = Cryptography.getRandomBytes(16);
        var cipher = forge.cipher.createCipher("AES-CBC", key);
        cipher.start({
            iv: iv
        });
        cipher.update(forge.util.createBuffer(data));
        cipher.finish();
        var encrypted = cipher.output.getBytes();
        var ciphertext = iv + encrypted;
        var hmac = forge.hmac.create();
        hmac.start("sha256", key);
        hmac.update(ciphertext);
        return Cryptography.b64Encode(ciphertext + hmac.digest().getBytes());
    },
    AESDecrypt: function(data, key, dontDecodeUtf) {
        data = Cryptography.b64Decode(data);
        key = Cryptography.b64Decode(key);
        var decipher = forge.cipher.createDecipher("AES-CBC", key);
        var iv = data.substring(0, 16);
        var ciphertext = data.substring(16, data.length - 32);
        var mac = data.substring(data.length - 32);
        var hmac = forge.hmac.create();
        hmac.start("sha256", key);
        hmac.update(iv + ciphertext);
        var digest = hmac.digest().getBytes();
        if (digest != mac) {
            return null;
        }
        decipher.start({
            iv: iv
        });
        decipher.update(forge.util.createBuffer(ciphertext));
        decipher.finish();
        var plaintext = decipher.output.getBytes();
        if (dontDecodeUtf) return plaintext;
        else return Cryptography.utf8Decode(plaintext);
    },
    generateSignature: function(publicKey, secret) {
        var base = Cryptography.stringToNumber(publicKey);
        var secretDigest = forge.md.sha512.create();
        secretDigest.update(Cryptography.utf8Encode(secret));
        var exponent = Cryptography.bytesToNumber(secretDigest.digest()
            .getBytes());
        var signature = base.modPow(exponent, Cryptography.DEFAULT_DH_MODULUS);
        return Cryptography.numberToString(signature);
    },
    verifySignature: function(publicKey, secret, signature) {
        return signature == Cryptography.generateSignature(publicKey,
            secret);
    },
    randomString: function(length) {
        var rs = "";
        for (var i = 0; i < length; i++) {
            rs += String.fromCharCode(Math.floor(Math.random() * 25 +
                65));
        }
        return rs;
    }

};
for (var i = 0; i <= 256; i += 1) {
    Cryptography.numberMapping.push(new BigInteger(i.toString(), 10));
}
