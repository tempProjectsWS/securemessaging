var Message = function(contents, sender, receiver, timestamp, signature, publicKey)
{
    this.contents = contents;
    this.sender = sender;
    this.receiver = receiver;
    this.timestamp = timestamp;
    this.time = new Date(this.timestamp*1000);
    this.signature = signature;
    this.publicKey = publicKey;
    this.isRead = false;
}
var FileMessage = function(fileName, fileKey, aesKey, sender, receiver, timestamp, publickey)
{
    mergeObjects(new Message(fileName, sender, receiver, timestamp, null, publickey), this);
    this.fileKey = fileKey;
    this.fileName = fileName;
    this.isFile = true;
    this.aesKey = aesKey;
}
var MessageHandling = {
    generatePOSTParameters: function(contents, peer, peerKey, timeDiff, sender, signingKey)
    {
        var timestamp = Math.round(new Date().getTime()/1000);
        timestamp +=timeDiff;
        var messageObject = new Message(contents, sender, peer, timestamp);
        timestamp = timestamp.toString();
        while(timestamp.length<18) timestamp="0"+timestamp;
        contents += timestamp;
        var messagePrivateKey = Cryptography.generateDHPrivate();
        var messagePublicKey = Cryptography.calculateDHPublic(messagePrivateKey);
        var aesKey = Cryptography.calculateDHAES(peerKey, messagePrivateKey);
        var ciphertext = Cryptography.AESEncrypt(contents, aesKey);
        var params = {};
        params[HTTPStrings.POST_MSG_PEER] = peer;
        params[HTTPStrings.POST_MSG_CONTENTS] = ciphertext;
        params[HTTPStrings.POST_MSG_PUBLICKEY_CALCULATED] = messagePublicKey;
        params[HTTPStrings.POST_MSG_PUBLICKEY_USED]=peerKey;
        if(signingKey) params[HTTPStrings.POST_MSG_SM_SIGNATURE]=Cryptography.generateSignature(messagePublicKey, signingKey);
        return [params, messageObject];
    },
    decryptMessageObject: function(messageDict, myPub1, myPriv1, myPub2, myPriv2)
    {
        var publicKeyUsed = messageDict[HTTPStrings.JSON_MSG_PUBLICKEY_USED];
        var privateKeyUsed=null;
        var isFile = messageDict[HTTPStrings.JSON_MESSAGE_IS_FILE];
        if(publicKeyUsed==myPub1) privateKeyUsed = myPriv1;
        if(publicKeyUsed==myPub2) privateKeyUsed = myPriv2;
        if(privateKeyUsed==null) return new Message("Invalid message, no decryption key", 
                                                    messageDict[HTTPStrings.JSON_MSG_SENDER],
                                                    messageDict[HTTPStrings.JSON_MSG_RECEIVER],
                                                    messageDict[HTTPStrings.JSON_MSG_TIMESTAMP]);
        var aesKey = Cryptography.calculateDHAES(messageDict[HTTPStrings.JSON_MSG_PUBLCIKEY_CALCULATED], privateKeyUsed);
        if(!isFile)
        {
            var contents = Cryptography.AESDecrypt(messageDict[HTTPStrings.POST_MSG_CONTENTS], aesKey);
            if(contents ==null)
            {
                return new Message("Invalid message, bad MAC", 
                                   messageDict[HTTPStrings.JSON_MSG_SENDER],
                                   messageDict[HTTPStrings.JSON_MSG_RECEIVER],
                                   messageDict[HTTPStrings.JSON_MSG_TIMESTAMP]);
            }
            var timestamp = parseInt(contents.substr(contents.length-18));
            if(isNaN(timestamp) || Math.abs(timestamp - messageDict[HTTPStrings.JSON_MSG_TIMESTAMP])>150)
            {
                return new Message("Invalid message, bad timestamp", 
                                   messageDict[HTTPStrings.JSON_MSG_SENDER],
                                   messageDict[HTTPStrings.JSON_MSG_RECEIVER],
                                   messageDict[HTTPStrings.JSON_MSG_TIMESTAMP]);
            }
            contents = contents.substr(0, contents.length-18);
            return new Message(contents, 
                               messageDict[HTTPStrings.JSON_MSG_SENDER],
                               messageDict[HTTPStrings.JSON_MSG_RECEIVER],
                               messageDict[HTTPStrings.JSON_MSG_TIMESTAMP],
                               messageDict[HTTPStrings.JSON_MSG_SM_SIGNATURE],
                               messageDict[HTTPStrings.JSON_MSG_PUBLCIKEY_CALCULATED]);
        }
        else
        {
            return new FileMessage(messageDict[HTTPStrings.JSON_FILENAME],
                                   messageDict[HTTPStrings.JSON_FILE_KEY],
                                   aesKey,
                                   messageDict[HTTPStrings.JSON_MSG_SENDER],
                                   messageDict[HTTPStrings.JSON_MSG_RECEIVER],
                                   messageDict[HTTPStrings.JSON_MSG_TIMESTAMP],
                                   messageDict[HTTPStrings.JSON_MSG_PUBLCIKEY_CALCULATED]);
        }
    },
    decryptMessageFile: function(fileJSON, message)
    {
        var fileDict = JSON.parse(fileJSON);
        var aesKey = message.aesKey;
        var contents = Cryptography.AESDecrypt(fileDict[HTTPStrings.JSON_MSG_CONTENTS], aesKey, true);
        if(contents.length<18) return null;
        var timestamp = parseInt(contents.substr(contents.length-18));
        if(isNaN(timestamp) || Math.abs(timestamp - message.timestamp)>150)
        {
            return null;
        }
        return contents.substr(0, contents.length-18);
        
    },
    generateFileUploadJSON: function(contents, peer, peerKey, timeDiff)
    {
        var timestamp = Math.round(new Date().getTime()/1000);
        timestamp +=timeDiff;
        timestamp = timestamp.toString();
        while(timestamp.length<18) timestamp="0"+timestamp;
        contents += timestamp;
        var messagePrivateKey = Cryptography.generateDHPrivate();
        var messagePublicKey = Cryptography.calculateDHPublic(messagePrivateKey);
        var aesKey = Cryptography.calculateDHAES(peerKey, messagePrivateKey);
        var ciphertext = Cryptography.AESEncrypt(contents, aesKey, true);
        var messageObject = {};
        messageObject[HTTPStrings.JSON_MSG_CONTENTS]=ciphertext;
        messageObject[HTTPStrings.JSON_MSG_PUBLICKEY_USED]=peerKey;
        messageObject[HTTPStrings.JSON_MSG_PUBLCIKEY_CALCULATED]=messagePublicKey;
        return JSON.stringify(messageObject);
    },
    parseMessageList: function(messageList, myPub1, myPriv1, myPub2, myPriv2)
    {
        for(var i=0;i<messageList.length;i++)
        {
            messageList[i]=MessageHandling.decryptMessageObject(messageList[i], myPub1, myPriv1, myPub2, myPriv2);
        }
        return messageList;
    },
};