var FileUploadDialog={
    getFileNameFromPath: function(filePath)
    {
        return filePath.split(/(\\|\/)/g).pop()
    },
    doFileUpload: function(peer, dialog)
    {
        if($("#sentFile")[0].files.length==0)
        {
            UiUtils.handleError("Please select a file");
            return;
        }
        var fileReader=new FileReader();
        fileReader.onloadend = function(e)
        {
            currentSession.uploadFile(FileUploadDialog.getFileNameFromPath($("#sentFile").val()), e.target.result, peer, function()
                                      {
                                        dialog.dialog("close");
                                        messageSent(new Message(FileUploadDialog.getFileNameFromPath($("#sentFile").val()),
                                                                currentSession.username, peer, new Date().getTime()/1000));
                                      }, UiUtils.handleError, function(val)
                                      {
                UiUtils.progressReport(Math.floor(val*100))
            });;
        }
        fileReader.readAsBinaryString($("#sentFile")[0].files[0]);
    },
    show: function()
    {
        if(!$("#peer").val())
        {
            UiUtils.handleError("Please select a contact");
            return;
        }
        var dialog = UiUtils.showModalForm("#fileUploadDialog", "Upload",
                                      function()
                                      {
            FileUploadDialog.doFileUpload($("#peer").val(), dialog);
        });
    }
};