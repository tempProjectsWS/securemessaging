var MessageStorage={
    messages: [],
    storeMessages: function(username, messages)
    {
        DataStorage.storeItem("MSG_"+username, JSON.stringify(messages));
    },
    getMessages: function(username)
    {
        if(DataStorage.getItem("MSG_"+username)) return JSON.parse(DataStorage.getItem("MSG_"+username))
        else return [];
    },
    compareMessages: function(m1, m2)
    {
        return m1.timestamp-m2.timestamp;
    },
    sortMessages: function()
    {
        MessageStorage.messages.sort(MessageStorage.compareMessages);
    },
    deleteMessage: function(messageIndex)
    {
        MessageStorage.messages.splice(messageIndex,1);
    }
};