var Templates={
    sentMessage: "<p class='sentMessage ui-corner-all'>{CONTENT}</p>",
    receivedMessage: "<p class='receivedMessage ui-corner-all'>{CONTENT}</p>",
    receivedFileMessage: "<p class='receivedMessage ui-corner-all'><a class='fileLink' href='#'>{CONTENT}</a></p>",
    messageFooter: "<p class='messageFooter'><span class='deleteMessageButton'><a href='#'>Delete</a></span><span class='checkSignatureButton'><a href='#'>Signed</a></span><span class='timestamp'>{TIMESTAMP}<span></p>",
    newConversationMenuItem: "<li style='padding:0.3em;border-bottom-style:groove;border-bottom-width:1px'>New conversation<br/><span></span></li>",
    conversationMenuItem: "<li style='padding:0.3em;border-bottom-style:groove;border-bottom-width:1px'>{PEER}<br/><span style='font-size:x-small;margin-left:1em'>{CONTENT}</span></li>"
};