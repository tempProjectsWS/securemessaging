var RegisterDialog = {
    doRegister: function(username, email, password, confirm, dialog)
    {
        UiUtils.progressStart();
        if(!(username && email && password && confirm)){
            UiUtils.handleError("Please fill in all fields");
            UiUtils.progressStop();
            return;
        }
        if(password!=confirm)
        {
            UiUtils.handleError("Passwords don't match");
            UiUtils.progressStop();
            return;
        }
        if(!IsEmail(email))
        {
            UiUtils.handleError("Invalid email");
            UiUtils.progressStop();
            return;
        }
        UserManagement.registerNewUser(username, password, email, function()
                        {
            UiUtils.progressStop();
            UiUtils.displayMessage("User registered successfully, you will soon receive a verification email");
            dialog.dialog("close");
        }, UiUtils.handleError);
    },
    show: function()
    {
        var dialog = UiUtils.showModalForm("#registerDialog", "Register",
                                      function()
                                      {
            RegisterDialog.doRegister($("#registerUsername").val(), $("#registerEmail").val(),
                       $("#registerPassword").val(), $("#registerConfirmPassword").val(),
                       dialog);
        });
    }
};