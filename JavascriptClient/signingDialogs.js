var SigningDialogs = {
    toggleSigning: function()
    {
        if(signingKey) $("#signKey").val(signingKey);
        var signingDialog =UiUtils.showModalForm("#signingDialog", "Set signing key",
                                     function()
                                    {  
                                        if($("#signKey").val())
                                        {
                                            signingKey = $("#signKey").val();
                                            $("#signMessagesButton").text("Signing enabled");
                                        }
                                        else
                                        {
                                            signingKey = null;
                                            $("#signMessagesButton").text("Signing disabled");
                                        }
                                        signingDialog.dialog("close");
                                    });
    },
    checkMessageSignature: function(message)
    {
        var dialog =UiUtils.showModalForm("#checkSignatureDialog", "Check signature",
                                     function()
                                     {  
                                        if(Cryptography.verifySignature(message.publicKey, $("#checkSignatureKey").val(), message.signature))
                                        {
                                            UiUtils.displayMessage("Signature is correct");
                                        }
                                        else
                                        {
                                            UiUtils.displayMessage("Signature is incorrect");
                                        }
                                        dialog.dialog("close");
                                     });
    }
};