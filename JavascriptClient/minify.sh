#!/bin/sh
#Minifer script, creates a single .js file for in-browser use
cat libs.js secureMessagingClient/cryptography.js secureMessagingClient/httpStrings.js secureMessagingClient/http.js secureMessagingClient/dataStorage.js secureMessagingClient/message.js secureMessagingClient/userManagement.js secureMessagingClient/session.js fileUploadDialog.js forgotPasswordDialog.js messageStorage.js registerDialog.js setPasswordDialog.js signingDialogs.js templates.js uiutils.js    main.js > secMsgClientNoMin.js

closure-compiler --js secMsgClientNoMin.js --js_output_file secMsgClient.js --language_in ECMASCRIPT5 --warning_level QUIET

rm secMsgClientNoMin.js
