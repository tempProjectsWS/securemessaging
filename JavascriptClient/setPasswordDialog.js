var SetPasswordDialog ={
    doSetPassword: function(password, newPassword, confirm, dialog)
    {
        UiUtils.progressStart();
        if(password!=currentSession.password){
            UiUtils.handleError("Old password incorrect.");
            UiUtils.progressStop();
            return;
        }
        if(newPassword!=confirm)
        {
            UiUtils.handleError("New passwords don't match.");
            UiUtils.progressStop();
            return;
        }
        currentSession.setPassword(newPassword, function()
                        {
            UiUtils.progressStop();
            UiUtils.displayMessage("Password set successfully.");
            dialog.dialog("close");
        }, UiUtils.handleError);
    },
    show: function()
    {
        var dialog =UiUtils.showModalForm("#setPasswordDialog", "Set password",
                                     function()
                                    {  
                                        SetPasswordDialog.doSetPassword($("#oldPassword").val(), $("#newPassword").val(),
                                                   $("#newPasswordConfirm").val(), dialog);
                                    });
    }
};