var ForgotPasswordDialog = {
    doForgot: function(username, email, dialog)
    {
        UiUtils.progressStart();
        if(!(username && email)){
            UiUtils.handleError("Please fill in all fields");
            UiUtils.progressStop();
            return;
        }
        if(!IsEmail(email))
        {
            UiUtils.handleError("Invalid email");
            UiUtils.progressStop();
            return;
        }
        UserManagement.forgotPassword(username, email, function()
                        {
            UiUtils.progressStop();
            UiUtils.displayMessage("Request sent successfully, you will soon receive an email with a reset link.");
            dialog.dialog("close");
        }, UiUtils.handleError);
    },
    show: function()
    {
        var dialog = UiUtils.showModalForm("#forgotDialog", "Request password reset",
                                      function()
                                    {  
                                        ForgotPasswordDialog.doForgot($("#forgotUsername").val(), $("#forgotEmail").val(), dialog);
                                    });
    }
};