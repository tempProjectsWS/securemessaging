package nidzo.cryptochat;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import nidzo.securemessagingclient.SecureMessagingException;


public class SetPasswordActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
    }

    public void setNewPassword(View view) {
        findViewById(R.id.progressIndicator).setVisibility(View.VISIBLE);
        findViewById(R.id.setNewPasswordButton).setEnabled(false);
        new SetNewPasswordTask(view.getContext()).execute();
    }

    class SetNewPasswordTask extends BaseAsyncTask {
        boolean errorOccurred;

        public SetNewPasswordTask(Context context) {

            this.context = context;
            errorOccurred = false;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String newPassword = ((EditText) findViewById(R.id.newPasswordBox)).getText()
                    .toString();
            String oldPassword = ((EditText) findViewById(R.id.passwordBox)).getText().toString();
            String passwordConfirmation = ((EditText) findViewById(R.id.confirmPasswordBox))
                    .getText().toString();
            if (SharedData.activeSession == null) {
                displayMessage(R.string.errorBoxTitle, R.string.notLoggedInErrorText);
                errorOccurred = true;
                return null;
            }
            if (!oldPassword.equals(SharedData.activeSession.getPassword())) {
                displayMessage(R.string.errorBoxTitle, R.string.badPasswordErrorText);
                errorOccurred = true;
                return null;
            }
            if (newPassword.equals("")) {
                displayMessage(R.string.errorBoxTitle, R.string.missingFieldsErrorText);
                errorOccurred = true;
                return null;
            }
            if (!passwordConfirmation.equals(newPassword)) {
                displayMessage(R.string.errorBoxTitle, R.string.passwordsDontMatchErrorText);
                errorOccurred = true;
                return null;
            }
            try {
                if (SharedData.activeSession != null) {
                    SharedData.activeSession.setPassword(newPassword);
                    displayMessage(R.string.successBoxTitle, R.string.setPasswordSuccessText);
                }
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse(error.getMessage(), getApplicationContext()));
                errorOccurred = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            showMessage(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!errorOccurred) {
                        finish();
                    }
                }
            });
            findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
            findViewById(R.id.setNewPasswordButton).setEnabled(true);
        }
    }
}
