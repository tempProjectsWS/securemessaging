package nidzo.cryptochat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

public class GCMNotificationReceiver extends WakefulBroadcastReceiver {
    public GCMNotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String receiver = intent.getStringExtra("username");
        if (SharedData.activeSession != null && SharedData.activeSession.getUsername().equals
                (receiver)) {
            if (SharedData.activeConversationActivity != null) {
                SharedData.activeConversationActivity.refreshMessages(null);
            }
            else {
                showNotification(context, receiver);
            }
        }
        else {
            showNotification(context, receiver);
        }
    }

    private int generateNotificationId(String text) {
        int id = 0;
        for (char c : text.toCharArray()) {
            id += c;
            id %= 10000000;
        }
        for (char c : text.toCharArray()) {
            id *= c;
            id %= 10000000;
        }
        return id;
    }

    private void showNotification(Context context, String username) {
        if (DataStorage.getNotifierUsernames(context) != null && DataStorage.getNotifierUsernames(context).contains(username)) {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder
                    (context);

            notificationBuilder.setSmallIcon(R.drawable.notification_icon);
            notificationBuilder.setContentTitle(context.getString(R.string.app_name));
            notificationBuilder.setContentText(context.getString(R.string.notificationText)
                    .replace("{0}", username));
            notificationBuilder.setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat
                    .DEFAULT_VIBRATE);
            notificationBuilder.setAutoCancel(true);
            PendingIntent pi = PendingIntent.getActivity(context.getApplicationContext(), 0,
                    new Intent(context, LoginActivity.class), PendingIntent.FLAG_ONE_SHOT);
            notificationBuilder.setContentIntent(pi);
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify
                    (generateNotificationId(username), notificationBuilder.build());
        }
    }
}
