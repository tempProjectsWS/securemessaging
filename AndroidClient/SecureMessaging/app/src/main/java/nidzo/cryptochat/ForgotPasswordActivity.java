package nidzo.cryptochat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import nidzo.securemessagingclient.SecureMessagingException;
import nidzo.securemessagingclient.UserManagement;


public class ForgotPasswordActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }

    public void reportForgotPassword(View view) {
        findViewById(R.id.progressIndicator).setVisibility(View.VISIBLE);
        findViewById(R.id.forgotPasswordButton).setEnabled(false);
        new ReportForgottenPasswordTask(this).execute();
    }

    class ReportForgottenPasswordTask extends BaseAsyncTask {

        public ReportForgottenPasswordTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String username = ((EditText) findViewById(R.id.usernameBox)).getText().toString().trim();
                String email = ((EditText) findViewById(R.id.emailBox)).getText().toString();
                if (username.equals("") || email.equals("")) {
                    displayMessage(R.string.errorBoxTitle, R.string.missingFieldsErrorText);
                    return null;
                }
                UserManagement.forgotPassword(username, email);
                displayMessage(R.string.successBoxTitle, R.string.reportForgetSucceededText);
            }
            catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse(error.getMessage(), getApplicationContext()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            showMessage();
            findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
            findViewById(R.id.forgotPasswordButton).setEnabled(true);
        }
    }
}
