package nidzo.cryptochat;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import nidzo.securemessagingclient.Message;
import nidzo.securemessagingclient.MessageList;
import nidzo.securemessagingclient.SecureMessagingException;


public class ConversationActivity extends Activity {

    private Menu menu;
    private EditText peerBox, messageContentsBox;
    private ProgressBar progressIndicator;
    private String peerName;
    private MessageListAdapter messagesAdapter;
    private FileTransfers fileTransfers;
    private String signatureSecret;
    private DrawerLayout drawer;
    private ListView conversationList;
    private ActionBarDrawerToggle drawerToggle;
    private ImageView unreadNotification;
    private View unreadNotificationWrapper;
    TickerTimer blinkerTimer = new TickerTimer(this, 500) {
        @Override
        public void onTick() {
            if (unreadNotification.getVisibility() == View.VISIBLE) {
                unreadNotification.setVisibility(View.INVISIBLE);
            } else unreadNotification.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(this,
                drawer,
                R.string.openDrawerDescription,
                R.string.closeDrawerDescription) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (getActionBar() != null) {
                    if (peerName.equals("")) {
                        getActionBar().setTitle(R.string.title_activity_conversation);
                    } else {
                        getActionBar().setTitle(peerName);
                    }
                }
                menu.findItem(R.id.sendFileButton).setEnabled(true);
                menu.findItem(R.id.sendMessageButton).setEnabled(true);
            }

            public void onDrawerOpened(View drawerView) {
                if (getActionBar() != null)
                    getActionBar().setTitle(R.string.title_activity_conversations);
                menu.findItem(R.id.sendFileButton).setEnabled(false);
                menu.findItem(R.id.sendMessageButton).setEnabled(false);
            }
        };
        drawer.setDrawerListener(drawerToggle);
        drawer.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }

        conversationList = (ListView) findViewById(R.id.conversationList);
        SharedData.conversationsAdapter = null;
        unreadNotification = (ImageView) findViewById(R.id.unreadNotification);
        unreadNotificationWrapper = findViewById(R.id.unreadNotificationWrapper);
        peerBox = (EditText) findViewById(R.id.peerBox);
        messageContentsBox = (EditText) findViewById(R.id.messageBox);
        progressIndicator = (ProgressBar) findViewById(R.id.progressIndicator);
        peerName = "";
        ListView display = (ListView) findViewById(R.id.display);
        fileTransfers = new FileTransfers(this);
        signatureSecret = null;

        Display screen = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        screen.getMetrics(metrics);
        double dpHeight = metrics.heightPixels / metrics.density;
        if (dpHeight < 500) {
            findViewById(R.id.adFragment).setVisibility(View.GONE);
        } else findViewById(R.id.adFragment).setVisibility(View.VISIBLE);
        display.setStackFromBottom(true);
        display.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        if (SharedData.activeSession == null) {
            if (!SharedData.loginOnResume(getApplicationContext())) {
                MessageBox.showConfirmationBox(R.string.errorBoxTitle, R.string.networkErrorText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                                System.exit(0);
                            }
                        }, this);
            }
        } else {
            SharedData.storeResumeCredentials(getApplicationContext());
        }

        messagesAdapter = new MessageListAdapter(this, display);
        display.setAdapter(messagesAdapter);
        SharedData.retrieveMessages(this);
        SharedData.activeConversationActivity = this;
        refreshMessages(null);
        startNewConversation();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
        drawer.openDrawer(Gravity.START);
        if (getActionBar() != null) getActionBar().setTitle(R.string.title_activity_conversations);
    }

    @Override
    public void onBackPressed() {
        new LogoutTask().execute();
        SharedData.activeConversationActivity = null;
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        this.menu = menu;
        if (DataStorage.getNotifierUsernames(this) != null && DataStorage.getNotifierUsernames(this).contains(SharedData.activeSession.getUsername())) {
            menu.findItem(R.id.notificationsToggle).setChecked(true);
        } else {
            menu.findItem(R.id.notificationsToggle).setChecked(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    public void progressStart() {
        progressIndicator.setVisibility(View.VISIBLE);
    }

    public void progressStop() {
        progressIndicator.setVisibility(View.INVISIBLE);
    }

    public void clearPeerBox(View view) {
        peerBox.getText().clear();
    }

    public void selectPeer(String peer) {
        peerBox.setVisibility(View.GONE);
        findViewById(R.id.clearButton).setVisibility(View.GONE);
        peerBox.setText(peer);
        peerName = peer;
        drawer.closeDrawer(Gravity.START);
        refreshDisplay();
    }

    public void startNewConversation() {
        peerBox.setVisibility(View.VISIBLE);
        peerName = "";
        peerBox.setText("");
        findViewById(R.id.clearButton).setVisibility(View.VISIBLE);
        drawer.closeDrawer(Gravity.START);
        refreshDisplay();
    }

    public void messageSelected(int messageId) {
        Message selectedMessage = SharedData.messages.messages[messageId];
        if (!selectedMessage.isFile())
            return;

        fileTransfers.downloadFile(selectedMessage);
    }

    public void sendFile(MenuItem item) {
        String peer = peerBox.getText().toString();
        if (peer.equals("")) {
            MessageBox.showMessageBox(R.string.errorBoxTitle, R.string.missingRecipientErrorText,
                    this);
            return;
        }
        if (peer.equals(SharedData.activeSession.getUsername())) {
            MessageBox.showMessageBox(R.string.errorBoxTitle, R.string
                    .cantSendMessageToSelfErrorText, this);
            return;
        }

        fileTransfers.selectFileToSend();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == FileTransfers.FILE_SELECT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                fileTransfers.sendSelectedFile(result, peerBox.getText().toString());
            }
        }
    }

    public void openDrawer(View view) {
        drawer.openDrawer(Gravity.START);
    }

    public void openRenameUser(MenuItem item) {
        Intent renameActivityIntent = new Intent(this, RenameActivity.class);
        startActivity(renameActivityIntent);
    }

    private class LogoutTask extends BaseAsyncTask {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if (SharedData.activeSession != null) SharedData.activeSession.logout();
            } catch (Exception ignored) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            SharedData.activeSession = null;
        }
    }

    public void refreshMessages(MenuItem view) {
        progressIndicator.setVisibility(View.VISIBLE);
        new RefreshTask(peerBox.getContext()).execute();
    }

    public void refreshDisplay() {
        List<Message> messagesList = new ArrayList<>();
        List<Integer> positions = new ArrayList<>();
        String peer = peerBox.getText().toString();
        for (int i = 0; i < SharedData.messages.messages.length; i++) {
            Message current = SharedData.messages.messages[i];
            if (current.getSender().equals(peer) || current.getReceiver().equals(peer)) {
                messagesList.add(current);
                positions.add(i);
            }
        }

        messagesAdapter.reset(messagesList, positions);
        SharedData.markMessagesAsRead(peerBox.getText().toString());
        SharedData.storeMessages(this);
        refreshList();
        progressStop();
    }

    public void deleteAllMessages(MenuItem item) {
        MessageBox.showConfirmationBox(R.string.warningTitleText,
                R.string.reallyDeleteAllMessages,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedData.messages = new MessageList();
                        SharedData.storeMessages(getApplicationContext());
                        startNewConversation();
                    }
                }, this);
    }

    public void sendMessage(MenuItem view) {
        progressStart();
        new SendMessageTask(this).execute();
    }

    public void toggleSigning(MenuItem item) {
        if (signatureSecret == null) {
            final EditText signingKeyInput = new EditText(this);
            signingKeyInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
            MessageBox.showInputBox(R.string.signingKeyInputBoxTitle,
                    R.string.singingKeyInputBoxText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            signatureSecret = signingKeyInput.getText().toString();
                            menu.findItem(R.id.toggleSigningButton).setTitle(R.string.disableSigningButtonText);
                        }
                    }, this, signingKeyInput);
        } else {
            signatureSecret = null;
            menu.findItem(R.id.toggleSigningButton).setTitle(R.string.enableSigningButtonText);
        }
    }

    private class RefreshTask extends BaseAsyncTask {
        private MessageList newMessages;

        public RefreshTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                newMessages = SharedData.activeSession.getMessages();
                return null;
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse
                        (error.getMessage(), getApplicationContext()));
                return null;
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            if (messageShown) {
                progressStop();
                showMessage();
            } else {
                SharedData.messages.append(newMessages);
                refreshDisplay();
            }
        }
    }

    private void refreshList() {
        if (SharedData.refreshConversationsAdapter(this)) {
            unreadNotificationWrapper.setVisibility(View.VISIBLE);
            blinkerTimer.start();
        } else {
            blinkerTimer.stop();
            unreadNotificationWrapper.setVisibility(View.INVISIBLE);
        }
        conversationList.setAdapter(SharedData.conversationsAdapter);
        progressStop();
    }

    private class SendMessageTask extends BaseAsyncTask {
        private Message sentMessage;

        public SendMessageTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String peer = peerBox.getText().toString();
            String messageContents = messageContentsBox.getText().toString();
            if (peer.equals("") || messageContents.equals("")) {
                displayMessage(R.string.errorBoxTitle, R.string.missingRecipientOrMessageErrorText);
                return null;
            }
            if (peer.equals(SharedData.activeSession.getUsername())) {
                displayMessage(R.string.errorBoxTitle, R.string.cantSendMessageToSelfErrorText);
                return null;
            }
            try {
                sentMessage = SharedData.activeSession.sendMessage(messageContents, peer, signatureSecret);
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse
                        (error.getMessage(), getApplicationContext()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (messageShown) {
                progressStop();
                showMessage();
            } else {
                SharedData.messages.insert(sentMessage);
                messageContentsBox.getText().clear();
                selectPeer(peerBox.getText().toString());
                refreshMessages(null);
            }
        }
    }

    public void conversationDeleted(String peer) {
        if (peer.equals(peerName)) startNewConversation();
    }

    public void openSetPassword(MenuItem item) {
        Intent setPasswordActivityIntent = new Intent(this, SetPasswordActivity.class);
        startActivity(setPasswordActivityIntent);
    }

    public void unauthorizeClient(MenuItem item) {
        MessageBox.showConfirmationBox(R.string.warningTitleText,
                R.string.reallyUnauthorize,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new UnauthorizeClientTask(conversationList.getContext()).execute();
                    }
                }, this);
    }

    private class UnauthorizeClientTask extends BaseAsyncTask {
        public UnauthorizeClientTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                SharedData.activeSession.unauthorize();
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse(error.getMessage(), getApplicationContext()));
                return null;
            }
            DataStorage.storeMessages(SharedData.activeSession.getUsername(), new MessageList(),
                    getApplicationContext());
            DataStorage.removeNotifierUsername(SharedData.activeSession.getUsername(),
                    getApplicationContext());
            DataStorage.storeToken(SharedData.activeSession.getUsername(), null,
                    getApplicationContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (messageShown) {
                showMessage();
            } else {
                finish();
            }
        }
    }

    public void markAllAsRead(MenuItem item) {
        SharedData.markMessagesAsRead(null);
        SharedData.storeMessages(this);
        SharedData.refreshConversationsAdapter(this);
    }

    public void toggleNotification(MenuItem item) {
        if (!item.isChecked()) {
            item.setChecked(true);
            DataStorage.storeNotifierUsername(SharedData.activeSession.getUsername(), this);
        } else {
            item.setChecked(false);
            DataStorage.removeNotifierUsername(SharedData.activeSession.getUsername(), this);
        }
    }
}