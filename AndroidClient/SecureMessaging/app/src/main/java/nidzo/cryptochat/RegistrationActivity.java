package nidzo.cryptochat;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import nidzo.securemessagingclient.SecureMessagingException;
import nidzo.securemessagingclient.UserManagement;


public class RegistrationActivity extends Activity {
    EditText usernameBox, emailBox, passwordBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        usernameBox = (EditText) findViewById(R.id.usernameBox);
        passwordBox = (EditText) findViewById(R.id.passwordBox);
        emailBox = (EditText) findViewById(R.id.emailBox);
    }

    public void registerUser(View view) {
        findViewById(R.id.progressIndicator).setVisibility(View.VISIBLE);
        findViewById(R.id.registerUserButton).setEnabled(false);
        new RegisterUserTask(view.getContext()).execute();
    }

    class RegisterUserTask extends BaseAsyncTask {
        boolean errorDisplayed;

        public RegisterUserTask(Context context) {
            this.context = context;
            errorDisplayed = false;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String username = usernameBox.getText().toString().trim(),
                        password = passwordBox.getText().toString(),
                        email = emailBox.getText().toString(),
                        passwordConfirmation = ((EditText) findViewById(R.id.confirmPasswordBox))
                                .getText().toString();
                if (username.equals("") || email.equals("") || password.equals("")) {
                    displayMessage(R.string.errorBoxTitle, R.string.missingFieldsErrorText);
                    errorDisplayed = true;
                    return null;
                }
                if (!passwordConfirmation.equals(password)) {
                    displayMessage(R.string.errorBoxTitle, R.string.passwordsDontMatchErrorText);
                    errorDisplayed = true;
                    return null;
                }
                UserManagement.registerUser(username, email, password);
                displayMessage(R.string.successBoxTitle, R.string.registrationSucceededText);
            }
            catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse(error.getMessage(), getApplicationContext()));
                errorDisplayed = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            showMessage(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!errorDisplayed) {
                        finish();
                    }
                }
            });
            findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
            findViewById(R.id.registerUserButton).setEnabled(true);
        }
    }
}
