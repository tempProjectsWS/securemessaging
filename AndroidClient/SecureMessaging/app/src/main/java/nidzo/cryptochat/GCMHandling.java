package nidzo.cryptochat;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

public class GCMHandling {
    private static final String SenderId = "49766918213";

    public static boolean GCMRegistered(Context context) {
        return DataStorage.getGCMKey(context) != null;
    }

    public static void registerGCM(Context context, Activity activity) {
        if (GCMRegistered(context)) {
            return;
        }
        if (!PlayServicesCheck.PlayServicesAvailable(context)) {
            PlayServicesCheck.DisplayPlayServicesError(activity, context);
        }
        new GCMRegisterTask(context).execute();
    }

    private static class GCMRegisterTask extends BaseAsyncTask {

        private String regId;

        public GCMRegisterTask(Context context) {
            this.context = context;
            regId = null;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
            try {
                regId = gcm.register(SenderId);
            }
            catch (IOException e) {
                regId = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            DataStorage.storeGCMKey(context, regId);
        }
    }
}
