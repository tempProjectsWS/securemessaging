package nidzo.cryptochat;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import nidzo.securemessagingclient.http.HTTPStrings.responses;

public class ResponseTranslator {
    public static String getUserFriendlyResponse(String response, Context context) {
        Map<String, String> responseMap = new HashMap<>();
        responseMap.put(responses.RESPONSE_UNAUTHORIZED, context.getString(R.string.unathorizedClientErrorText));
        responseMap.put(responses.RESPONSE_NETWORK_ERROR, context.getString(R.string.networkErrorText));
        responseMap.put(responses.RESPONSE_ALREADY_AUTHORIZED, context.getString(R.string.alreadyAuthorizedErrorText));
        responseMap.put(responses.RESPONSE_MSG_NO_PEER, context.getString(R.string.badPeerErrorText));
        responseMap.put(responses.RESPONSE_PEER_INACTIVE, context.getString(R.string.peerInactiveErrorText));
        responseMap.put(responses.RESPONSE_USER_BAD_EMAIL, context.getString(R.string.badEmailErrorText));
        responseMap.put(responses.RESPONSE_UNHANDLED_ERROR, context.getString(R.string.serverErrorText));
        responseMap.put(responses.RESPONSE_USER_BAD_PASSWORD, context.getString(R.string.badPasswordErrorText));
        responseMap.put(responses.RESPONSE_USER_BAD_USERNAME, context.getString(R.string.badUsernameErrorText));
        responseMap.put(responses.RESPONSE_USER_NAME_EXISTS, context.getString(R.string.usernameExistsErrorText));
        responseMap.put(responses.RESPONSE_USER_EMAIL_EXISTS, context.getString(R.string.emailExistsErrorText));
        responseMap.put(responses.RESPONSE_USER_NOT_LOGGED_IN, context.getString(R.string.notLoggedInErrorText));
        responseMap.put(responses.RESPONSE_USER_NOT_VERIFIED, context.getString(R.string.notVerifiedErrorText));
        responseMap.put(responses.RESPONSE_USER_BAD_TOKEN, context.getString(R.string.badTokenErrorText));
        responseMap.put(responses.RESPONSE_BAD_FILE_KEY, context.getString(R.string.badFileKeyErrorText));
        responseMap.put(responses.RESPONSE_TIMEDIFF_TOO_BIG, context.getString(R.string.timeDiffToBigErrorText));
        if (responseMap.containsKey(response)) {
            return responseMap.get(response);
        }
        else {
            return context.getString(R.string.unknownErrorText)+": "+response;
        }
    }
}
