package nidzo.cryptochat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import nidzo.securemessagingclient.SecureMessagingException;


public class LoginActivity extends Activity {
    private EditText usernameBox, passwordBox;
    private CheckBox rememberCredentialsCheckbox;
    private ProgressBar progressIndicator;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameBox = (EditText) findViewById(R.id.usernameBox);
        passwordBox = (EditText) findViewById(R.id.passwordBox);
        rememberCredentialsCheckbox = (CheckBox) findViewById(R.id.rememberCredentialsCheckbox);
        progressIndicator = (ProgressBar) findViewById(R.id.progressIndicator);
        loginButton = (Button) findViewById(R.id.loginButton);
        TextView registrationLink = (TextView)findViewById(R.id.registrationLink);
        TextView helpLink = (TextView)findViewById(R.id.helpLink);
        TextView forgotLink = (TextView)findViewById(R.id.openForgotPasswordLink);
        registrationLink.setPaintFlags(registrationLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        helpLink.setPaintFlags(helpLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        forgotLink.setPaintFlags(forgotLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        String[] storedCredentials;
        if ((storedCredentials = DataStorage.getStoredCredentials(getApplicationContext())) !=
                null) {
            usernameBox.setText(storedCredentials[0]);
            passwordBox.setText(storedCredentials[1]);
            rememberCredentialsCheckbox.setChecked(true);
        }
        GCMHandling.registerGCM(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    public void openRegistration(MenuItem item) {
        Intent registrationActivityIntent = new Intent(this, RegistrationActivity.class);
        startActivity(registrationActivityIntent);
    }

    public void rememberCredentialsCheckChanged(View view) {
        if (!rememberCredentialsCheckbox.isChecked()) {
            DataStorage.storeCredentials(null, null, getApplicationContext());
        }
    }

    public void openHelp(MenuItem item) {
        Intent helpActivityIntent = new Intent(this, HelpActivity.class);
        startActivity(helpActivityIntent);
    }

    public void openForgot(MenuItem item) {
        Intent forgotPasswordActivityIntent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(forgotPasswordActivityIntent);
    }

    public void logIn(View view) {
        progressIndicator.setVisibility(View.VISIBLE);
        loginButton.setEnabled(false);
        new LoginTask(view.getContext()).execute();
    }

    public void openRegistrationLink(View view) {
        openRegistration(null);
    }

    public void openHelpLink(View view) {
        openHelp(null);
    }

    public void openForgotPasswordLink(View view) {
        openForgot(null);
    }

    class LoginTask extends BaseAsyncTask {

        public LoginTask(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String password = passwordBox.getText().toString();
                String username = usernameBox.getText().toString().trim();
                if (username.equals("") || password.equals("")) {
                    displayMessage(R.string.errorBoxTitle, R.string.missingCredentialsErrorText);
                    return null;
                }
                SharedData.doLogin(username, password, getApplicationContext());
                if(rememberCredentialsCheckbox.isChecked())
                {
                    DataStorage.storeCredentials(SharedData.activeSession.getUsername(), SharedData.activeSession.getPassword(), getApplicationContext());
                }
                if(GCMHandling.GCMRegistered(getApplicationContext())) {
                    SharedData.activeSession.registerGCM(DataStorage.getGCMKey(getApplicationContext()));

                }
            }
            catch (SecureMessagingException err) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse(err.getMessage(), getApplicationContext()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (messageShown) {
                showMessage();
            }
            else {
                startActivity(new Intent(context, ConversationActivity.class));
            }
            progressIndicator.setVisibility(View.INVISIBLE);
            loginButton.setEnabled(true);
        }
    }
}