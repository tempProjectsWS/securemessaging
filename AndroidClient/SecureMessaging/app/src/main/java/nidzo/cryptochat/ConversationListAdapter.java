package nidzo.cryptochat;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

import nidzo.securemessagingclient.Message;

public class ConversationListAdapter extends ArrayAdapter<Message> {
    private List<Message> messages;
    private LayoutInflater inflater;

    public ConversationListAdapter(Context context, List<Message> messages) {
        super(context, R.layout.conversations_list_item, messages);
        this.messages = messages;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void reset(List<Message> messages) {
        this.clear();
        this.addAll(messages);
        this.messages = messages;
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.conversations_list_item, parent, false);
        } else {
            rowView = convertView;
        }
        TextView nameView = (TextView) rowView.findViewById(R.id.peerName);
        TextView messageContentsView = (TextView) rowView.findViewById(R.id.messageContents);
        TextView dateText = (TextView) rowView.findViewById(R.id.timeAndDate);
        View conversationDetails = rowView.findViewById(R.id.conversationDetails);

        Message currentMessage = messages.get(position);
        String peerName = "";
        String messageContents;

        if (currentMessage != null) {
            if (currentMessage.getSender().equals(SharedData.activeSession.getUsername())) {
                peerName = currentMessage.getReceiver();
            } else {
                peerName = currentMessage.getSender();
            }
            rowView.setPadding(16,8,16,8);
            nameView.setText(peerName);
            messageContents = currentMessage.getContents().split("\n")[0];
            if (messageContents.length() > 20) messageContents = messageContents.substring(0, 20);
            messageContentsView.setText(messageContents);
            dateText.setText(currentMessage.getTimeStampString("HH:mm EE"));
            conversationDetails.setVisibility(View.VISIBLE);
        } else {
            rowView.setPadding(16,20,16,20);
            conversationDetails.setVisibility(View.GONE);
            nameView.setText(this.getContext().getString(R.string.newConversationItemText));
        }

        if (currentMessage != null && !currentMessage.isRead()) {
            nameView.setTypeface(null, Typeface.BOLD_ITALIC);
            messageContentsView.setTypeface(null, Typeface.BOLD_ITALIC);
            nameView.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.presence_online, 0);
        } else {
            nameView.setTypeface(null, Typeface.NORMAL);
            messageContentsView.setTypeface(null, Typeface.NORMAL);
            nameView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        rowView.setOnClickListener(new SelectConversationListener(peerName, this.getContext()));
        rowView.setOnLongClickListener(new ListLongClickListener(peerName, this.getContext(),
                rowView));

        return rowView;
    }

    private class SelectConversationListener implements View.OnClickListener {
        private String name;
        private Context context;

        public SelectConversationListener(String name, Context context) {
            this.name = name;
            this.context = context;
        }

        @Override
        public void onClick(View view) {
            if (name.equals("")) {
                SharedData.activeConversationActivity.startNewConversation();
            } else {
                SharedData.activeConversationActivity.selectPeer(name);
            }
        }
    }

    private class ListLongClickListener implements View.OnLongClickListener {
        private String peerName;
        private Context context;
        private View parent;

        public ListLongClickListener(String peerName, Context context, View parent) {
            this.peerName = peerName;
            this.context = context;
            this.parent = parent;
        }

        @Override
        public boolean onLongClick(View view) {
            if(peerName.equals("")) return true;
            PopupMenu deleteMenu = new PopupMenu(view.getContext(), parent);
            deleteMenu.inflate(R.menu.menu_conversation_context);
            deleteMenu.getMenu().findItem(R.id.deleteConversationMenuItem)
                    .setOnMenuItemClickListener(new DeleteConversationListener(context, peerName));
            deleteMenu.getMenu().findItem(R.id.markConversationAsReadMenuItem)
                    .setOnMenuItemClickListener(new MarkConversationAsReadListener(context,
                            peerName));
            deleteMenu.show();
            return true;
        }
    }

    private class DeleteConversationListener implements MenuItem.OnMenuItemClickListener {
        private Context context;
        private String peerName;

        public DeleteConversationListener(Context context, String peerName) {
            this.context = context;
            this.peerName = peerName;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            MessageBox.showConfirmationBox(R.string.warningTitleText,
                    R.string.reallyDeleteConversation,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedData.deleteMessagesFromPeer(peerName);
                            SharedData.storeMessages(context);
                            SharedData.refreshConversationsAdapter(context);
                            SharedData.activeConversationActivity.conversationDeleted(peerName);
                        }
                    }, context);
            return true;
        }
    }

    private class MarkConversationAsReadListener implements MenuItem.OnMenuItemClickListener {
        private Context context;
        private String peerName;

        public MarkConversationAsReadListener(Context context, String peerName) {
            this.context = context;
            this.peerName = peerName;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            SharedData.markMessagesAsRead(peerName);
            SharedData.storeMessages(context);
            SharedData.refreshConversationsAdapter(context);
            return true;
        }
    }
}
