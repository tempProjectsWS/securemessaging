package nidzo.cryptochat;

import android.app.Activity;

public abstract class TickerTimer {
    private Activity owner;
    private int interval;
    private Thread executorThread;

    public TickerTimer(Activity owner, int interval) {
        synchronized (this) {
            this.owner = owner;
            this.interval = interval;
            this.executorThread = null;
        }
    }

    public synchronized void start() {
        if (executorThread == null) {
            executorThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    runnerThread();
                }
            });
            executorThread.start();
        }
    }

    public synchronized void stop() {
        if (executorThread != null) {
            try {
                executorThread.interrupt();
                executorThread.join();
            } catch (InterruptedException ignored) {

            } finally {
                executorThread = null;
            }
        }
    }

    private void runnerThread() {
        while (true) {
            owner.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onTick();
                }
            });
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public abstract void onTick();
}
