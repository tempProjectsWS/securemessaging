package nidzo.cryptochat;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import nidzo.securemessagingclient.Message;
import nidzo.securemessagingclient.SecureMessagingException;

public class RenameActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rename);
    }

    public void renameUser(View view) {
        findViewById(R.id.progressIndicator).setVisibility(View.VISIBLE);
        findViewById(R.id.renameButton).setEnabled(false);
        new RenameTask(view.getContext()).execute();
    }

    class RenameTask extends BaseAsyncTask {
        boolean errorOccurred;
        private String oldUsername;

        public RenameTask(Context context) {

            this.context = context;
            errorOccurred = false;
            oldUsername = SharedData.activeSession.getUsername();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String newUsername = ((EditText) findViewById(R.id.newUsernameBox)).getText().toString();
            if (newUsername.equals("")) {
                displayMessage(R.string.errorBoxTitle, R.string.missingFieldsErrorText);
                errorOccurred = true;
                return null;
            }
            try {
                if (SharedData.activeSession != null) {
                    SharedData.activeSession.renameUser(newUsername);
                    displayMessage(R.string.successBoxTitle, R.string.renameSuccessText);
                }
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse(error.getMessage(), getApplicationContext()));
                errorOccurred = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (!errorOccurred) {
                DataStorage.storeKeySet(SharedData.activeSession.getUsername(),
                        SharedData.activeSession.getPublicKey(),
                        SharedData.activeSession.getPrivateKey(),
                        context);
                DataStorage.storeToken(SharedData.activeSession.getUsername(),
                        SharedData.activeSession.getToken(),
                        context);
                for (Message message : SharedData.messages.messages) {
                    message.userRenamed(oldUsername, SharedData.activeSession.getUsername());
                }
                SharedData.storeMessages(context);
                if (DataStorage.getNotifierUsernames(context).contains(oldUsername)) {
                    DataStorage.storeNotifierUsername(SharedData.activeSession.getUsername(),
                            context);
                    DataStorage.removeNotifierUsername(oldUsername, context);
                }
                DataStorage.storeToken(oldUsername, null, context);
                DataStorage.storeKeySet(oldUsername, null, null, context);
                DataStorage.storeMessages(oldUsername, null, context);
            }
            showMessage(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!errorOccurred) {
                        finish();
                    }
                }
            });
            findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
            findViewById(R.id.renameButton).setEnabled(true);
        }
    }
}
