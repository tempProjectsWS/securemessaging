package nidzo.cryptochat;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

public abstract class BaseAsyncTask extends AsyncTask<Void, Void, Void> {
    protected boolean messageShown;
    protected String messageContents, messageTitle;
    protected Context context;

    protected void displayMessage(int title, int contents) {
        displayMessage(context.getString(title), context.getString(contents));
    }
    protected void displayMessage(int title, String contents)
    {
        displayMessage(context.getString(title), contents);
    }

    protected void displayMessage(String title, String contents)
    {
        messageShown=true;
        this.messageContents = contents;
        this.messageTitle = title;
    }

    protected void showMessage() {
        MessageBox.showMessageBox(messageTitle, messageContents, context);
    }

    protected void showMessage(DialogInterface.OnClickListener onOkClick) {
        MessageBox.showMessageBox(messageTitle, messageContents, onOkClick, context);
    }

}
