package nidzo.cryptochat;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class HelpActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        WebView helpDisplay = (WebView) findViewById(R.id.helpDisplay);

        helpDisplay.setBackgroundColor(0);
        helpDisplay.getSettings().setDefaultTextEncodingName("utf-8");
        InputStream helpStream = getResources().openRawResource(R.raw.help);
        BufferedReader helpReader = new BufferedReader(new InputStreamReader(helpStream));
        String helpLine, helpContents = "";
        try {
            while ((helpLine = helpReader.readLine()) != null) {
                helpContents += helpLine;
            }
        }
        catch (IOException e) {
            helpContents = "";
        }

        helpDisplay.loadDataWithBaseURL(null, helpContents, "text/html", "utf-8", null);
        helpDisplay.reload();

    }
}