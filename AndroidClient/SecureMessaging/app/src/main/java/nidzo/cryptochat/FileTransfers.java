package nidzo.cryptochat;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import nidzo.securemessagingclient.Message;
import nidzo.securemessagingclient.SecureMessagingException;

public class FileTransfers {
    public static final int FILE_SELECT_REQUEST_CODE = 1;
    private static final int MAX_FILE_LENGTH = 3145729;
    private static final int FILE_BUFFER_LENGTH = 3200000;
    private static final int CHUNK_READ_LENGTH = 1024;

    private ConversationActivity parent;

    public FileTransfers(ConversationActivity parentActivity) {
        parent = parentActivity;
    }

    private static String getFileName(Uri uri, Context context) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private static long getFileSize(Uri uri, Context context) {
        long result = -1;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return result;
    }

    private static File getAvailableFileInDownloadsWithName(String fileName) throws IOException {
        File downloadsFolder = Environment.getExternalStoragePublicDirectory(Environment
                .DIRECTORY_DOWNLOADS);
        if (!downloadsFolder.exists()) {
            if (!downloadsFolder.mkdir()) {
                throw new IOException();
            }
        }
        boolean hasExtension = fileName.contains(".");
        if (hasExtension) {
            String name = fileName.substring(0, fileName.lastIndexOf("."));
            String extension = fileName.substring(fileName.lastIndexOf("."));

            File checkerFile = new File(Environment.getExternalStoragePublicDirectory(Environment
                    .DIRECTORY_DOWNLOADS), fileName);
            int index = 0;
            while (checkerFile.exists()) {
                index++;
                fileName = name + "(" + Integer.toString(index) + ")" + extension;
                checkerFile = new File(Environment.getExternalStoragePublicDirectory(Environment
                        .DIRECTORY_DOWNLOADS), fileName);
            }
            return checkerFile;
        } else {
            File checkerFile = new File(Environment.getExternalStoragePublicDirectory(Environment
                    .DIRECTORY_DOWNLOADS), fileName);
            int index = 0;
            while (checkerFile.exists()) {
                index++;
                fileName = fileName + "(" + Integer.toString(index) + ")";
                checkerFile = new File(Environment.getExternalStoragePublicDirectory(Environment
                        .DIRECTORY_DOWNLOADS), fileName);
            }
            return checkerFile;
        }
    }

    private static boolean checkExternalStorage() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    private static boolean checkAvailableMemoryInDownloads(long amount) {
        return Environment.getExternalStorageDirectory().getFreeSpace() > amount;
    }

    private static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public void selectFileToSend() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            parent.startActivityForResult(Intent.createChooser(intent, parent.getString(R.string
                    .chooserIntentTitle)), FILE_SELECT_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            MessageBox.showMessageBox(R.string.errorBoxTitle, R.string.noFileManagerErrorText,
                    parent);
        }
    }

    public void sendSelectedFile(Intent result, String peerName) {
        Uri fileUri = result.getData();
        try {
            String fileName = FileTransfers.getFileName(fileUri, parent);
            long fileSize = FileTransfers.getFileSize(fileUri, parent);
            if (fileSize > MAX_FILE_LENGTH) {
                MessageBox.showMessageBox(R.string.errorBoxTitle, R.string
                        .fileTooBigErrorText, parent);
                return;
            }
            String fileType = parent.getContentResolver().getType(fileUri);
            InputStream fileStream = parent.getContentResolver().openInputStream(fileUri);

            byte[] buffer = new byte[FILE_BUFFER_LENGTH];
            int fileLength = 0;
            int bytesAdded = 0;
            while (bytesAdded != -1) {
                bytesAdded = fileStream.read(buffer, fileLength, CHUNK_READ_LENGTH);
                fileLength += bytesAdded;
            }
            byte[] fileContents = new byte[fileLength];
            System.arraycopy(buffer, 0, fileContents, 0, fileLength);
            parent.progressStart();
            new SendFileTask(fileName, fileType, peerName, fileContents).execute();
        } catch (FileNotFoundException error) {
            MessageBox.showMessageBox(R.string.errorBoxTitle, R.string
                    .fileNotFoundErrorText, parent);
        } catch (IOException error) {
            MessageBox.showMessageBox(R.string.errorBoxTitle, R.string.IOErrorText, parent);
        }
    }

    public void downloadFile(Message message) {
        parent.progressStart();
        new DownloadFileTask(message).execute();
    }

    private class SendFileTask extends BaseAsyncTask {
        private String fileName, mimeType, peerName;
        private byte[] fileContents;
        private Message sentMessage;

        public SendFileTask(String fileName, String mimeType, String peer, byte[] contents) {
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.fileContents = contents;
            this.peerName = peer;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                sentMessage = SharedData.activeSession.uploadFile(fileName, fileContents,
                        mimeType, peerName);
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse
                        (error.getMessage(), context));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            parent.progressStop();
            if (messageShown) {
                showMessage();
            } else {
                SharedData.messages.insert(sentMessage);
                SharedData.storeMessages(parent.getApplicationContext());
                parent.selectPeer(peerName);
                parent.refreshMessages(null);
            }
        }
    }

    private class DownloadFileTask extends BaseAsyncTask {
        byte[] fileContents;
        private Message fileMessage;
        private File writtenFile;

        public DownloadFileTask(Message message) {
            this.fileMessage = message;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if (!FileTransfers.checkExternalStorage()) {
                    displayMessage(R.string.errorBoxTitle, R.string.noExternalMemoryErrorText);
                    return null;
                }
                fileContents = SharedData.activeSession.downloadFile(fileMessage);
                if (!FileTransfers.checkAvailableMemoryInDownloads(fileContents.length)) {
                    displayMessage(R.string.errorBoxTitle, R.string.notEnoughMemoryErrorText);
                    return null;
                }
                try {
                    writtenFile = FileTransfers.getAvailableFileInDownloadsWithName(fileMessage.fileName());
                    FileOutputStream outStream = new FileOutputStream(writtenFile);
                    outStream.write(fileContents);
                    outStream.flush();
                    outStream.close();
                } catch (IOException e) {
                    displayMessage(R.string.errorBoxTitle, R.string.cantSaveFileErrorText);
                }
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse
                        (error.getMessage(), context));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            parent.progressStop();
            if (messageShown) {
                showMessage();
            } else {
                MessageBox.showConfirmationBox(R.string.successBoxTitle, R.string
                        .fileDownloadedText, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent openFileIntent = new Intent();
                        openFileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        openFileIntent.setAction(Intent.ACTION_VIEW);
                        try {
                            Uri fileUri = Uri.fromFile(writtenFile);
                            String mimeType = FileTransfers.getMimeType(fileUri.toString());
                            if (mimeType == null) {
                                throw new ActivityNotFoundException();
                            }
                            openFileIntent.setDataAndType(fileUri, mimeType);
                            parent.startActivity(openFileIntent);
                        } catch (ActivityNotFoundException error) {
                            MessageBox.showMessageBox(R.string.errorBoxTitle, R.string
                                    .noAvailableApplicationErrorText, parent);
                        }
                    }
                }, parent);
            }
        }
    }
}
