package nidzo.cryptochat;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import nidzo.securemessagingclient.Message;
import nidzo.securemessagingclient.MessageList;
import nidzo.securemessagingclient.SecureMessagingException;
import nidzo.securemessagingclient.Session;
import nidzo.securemessagingclient.http.HTTPStrings;

public class SharedData {
    public static Session activeSession;
    public static MessageList messages;
    public static ConversationListAdapter conversationsAdapter;
    public static ConversationActivity activeConversationActivity;

    public static void doLogin(String username, String password, Context context) throws SecureMessagingException {
        String token = DataStorage.getStoredToken(username, context);
        String[] keys = DataStorage.getStoredKeySet(username, context);
        if (token == null || keys == null) {
            String[] tokenAndKeys = Session.requestNewToken(username, password);
            token = tokenAndKeys[0];
            keys = new String[2];
            keys[0] = tokenAndKeys[1];
            keys[1] = tokenAndKeys[2];
            DataStorage.storeToken(username, token, context);
            DataStorage.storeKeySet(username, keys[0], keys[1], context);
        }
        try {
            activeSession = new Session(username, password, token, keys[0], keys[1]);
            DataStorage.storeKeySet(username, activeSession.getPublicKey(), activeSession.getPrivateKey(), context);
        } catch (SecureMessagingException error) {
            if (error.getMessage().equals(HTTPStrings.responses.RESPONSE_UNAUTHORIZED)) {
                DataStorage.storeToken(username, null, context);
                doLogin(username, password, context);
            } else {
                throw error;
            }
        }
    }

    public static void storeResumeCredentials(Context context) {
        DataStorage.storeResumeUsername(activeSession.getUsername(), context);
        DataStorage.storeResumePassword(activeSession.getPassword(), context);
    }

    public static boolean loginOnResume(Context context) {
        String username = DataStorage.getResumeUsername(context);
        String password = DataStorage.getResumePassword(context);
        if (username == null || password == null)
            return false;
        LoginOnResumeTask loginTask = new LoginOnResumeTask(username, password, context);
        loginTask.execute();
        try {
            loginTask.get();
        } catch (InterruptedException e) {
            return false;
        } catch (ExecutionException e) {
            return false;
        }
        return activeSession != null;
    }

    private static class LoginOnResumeTask extends BaseAsyncTask {
        String username, password;
        Context context;

        public LoginOnResumeTask(String name, String pass, Context context) {
            username = name;
            password = pass;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                doLogin(username, password, context);
            } catch (SecureMessagingException error) {
                displayMessage(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse(error.getMessage(), context));
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            if (messageShown) {
                activeSession = null;
            }
        }
    }

    public static void storeMessages(Context context) {
        DataStorage.storeMessages(activeSession.getUsername(), messages, context);
    }

    public static void retrieveMessages(Context context) {
        messages = DataStorage.getStoredMessages(activeSession.getUsername(), context);
    }

    public static void deleteMessagesFromPeer(String peer) {
        int numberOfDeleted = 0;
        int oldLength = messages.messages.length;
        for (int i = 0; i < oldLength; i++) {
            if (messages.messages[i - numberOfDeleted].getSender().equals(peer) || messages
                    .messages[i - numberOfDeleted].getReceiver().equals(peer)) {
                messages.deleteMessage(i - numberOfDeleted);
                numberOfDeleted++;
            }
        }
    }

    public static boolean refreshConversationsAdapter(Context context) {
        Set<String> seenPeers = new HashSet<>();
        ArrayList<Message> selectedMessages = new ArrayList<>();
        boolean unreadMessagesExist = false;
        selectedMessages.add(null);
        for (int i = messages.messages.length - 1; i >= 0; i--) {
            Message message = messages.messages[i];
            String peer;
            unreadMessagesExist = unreadMessagesExist || (!message.isRead());
            if (message.getSender().equals(activeSession.getUsername())) {
                peer = message.getReceiver();
            } else {
                peer = message.getSender();
            }
            if (seenPeers.contains(peer)) {
                continue;
            }
            seenPeers.add(peer);
            selectedMessages.add(message);
        }

        if (conversationsAdapter != null) {
            conversationsAdapter.reset(selectedMessages);
        } else {
            conversationsAdapter = new ConversationListAdapter(context, selectedMessages);
        }
        return unreadMessagesExist;
    }

    public static void markMessagesAsRead(String peer) {
        for (int i = 0; i < messages.messages.length; i++) {
            if (peer == null || messages.messages[i].getSender().equals(peer) || messages
                    .messages[i].getReceiver().equals(peer)) {
                messages.messages[i].setRead(true);
            }
        }
    }
}
