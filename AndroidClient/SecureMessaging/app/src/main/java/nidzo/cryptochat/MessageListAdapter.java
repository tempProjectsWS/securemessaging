package nidzo.cryptochat;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.text.InputType;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nidzo.securemessagingclient.Message;
import nidzo.securemessagingclient.SecureMessagingException;

public class MessageListAdapter extends ArrayAdapter<Message> {
    private List<Message> messages;
    private LayoutInflater inflater;
    private ListView parent;
    private List<Integer> positions;

    public MessageListAdapter(Context context, ListView parent) {
        super(context, R.layout.conversations_list_item, new ArrayList<Message>());
        this.messages = new ArrayList<>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.parent = parent;
        this.positions = new ArrayList<>();
    }

    public void reset(List<Message> messages, List<Integer> positions) {
        this.clear();
        this.addAll(messages);
        this.messages = messages;
        this.positions = positions;
        this.notifyDataSetChanged();
    }

    private void configureMargins(Message currentMessage, View rootLayout) {
        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(rootLayout
                .getLayoutParams());
        if (currentMessage.getSender().equals(SharedData.activeSession.getUsername())) {
            marginParams.setMargins(this.parent.getWidth() / 5, 0, 0, 0);
            rootLayout.setBackgroundResource(R.drawable.sent_message_background);
        } else {
            marginParams.setMargins(0, 0, this.parent.getWidth() / 5, 0);
            rootLayout.setBackgroundResource(R.drawable.received_message_background);
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(marginParams);
        rootLayout.setLayoutParams(layoutParams);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.message_list_item, parent, false);
        } else {
            rowView = convertView;
        }
        TextView messageContentsView = (TextView) rowView.findViewById(R.id.messageContents);
        TextView dateText = (TextView) rowView.findViewById(R.id.timeAndDate);
        View rootLayout = rowView.findViewById(R.id.rootLayout);

        Message currentMessage = messages.get(position);

        String messageContents = currentMessage.getContents();
        messageContentsView.setText(messageContents);

        configureMargins(currentMessage, rootLayout);

        if (currentMessage.isFile()) {
            messageContentsView.setPaintFlags(messageContentsView.getPaintFlags() | Paint
                    .UNDERLINE_TEXT_FLAG);
        } else {
            messageContentsView.setPaintFlags(messageContentsView.getPaintFlags() & (~Paint
                    .UNDERLINE_TEXT_FLAG));
        }

        if (currentMessage.isSigned()) {
            rowView.findViewById(R.id.signedIndicator).setVisibility(View.VISIBLE);
        } else rowView.findViewById(R.id.signedIndicator).setVisibility(View.GONE);

        String timeStampString = currentMessage.GetTimeStampString(DateFormat.getLongDateFormat
                (getContext()));
        timeStampString += " ";
        timeStampString += currentMessage.GetTimeStampString(DateFormat.getTimeFormat(getContext
                ()));
        dateText.setText(timeStampString);

        rootLayout.setOnLongClickListener(new MessageLongClickListener(positions.get(position),
                getContext(), rootLayout));
        rootLayout.setOnClickListener(new MessageClickListener(positions.get(position)));

        rowView.setOnLongClickListener(null);
        return rowView;
    }

    private class MessageLongClickListener implements View.OnLongClickListener {
        private int position;
        private Context context;
        private View parent;

        public MessageLongClickListener(int position, Context context, View parent) {
            this.position = position;
            this.context = context;
            this.parent = parent;
        }

        @Override
        public boolean onLongClick(View view) {
            PopupMenu deleteMenu = new PopupMenu(view.getContext(), parent);
            deleteMenu.inflate(R.menu.menu_message_context);
            deleteMenu.getMenu().findItem(R.id.deleteMessageMenuItem).setOnMenuItemClickListener
                    (new DeleteMessageListener(position));
            deleteMenu.getMenu().findItem(R.id.copyMessageMenuItem).setOnMenuItemClickListener
                    (new CopyMessageListener(position, context));
            if (SharedData.messages.messages[position].isSigned()) {
                deleteMenu.getMenu().findItem(R.id.verifyMessageSignatureMenuItem).setVisible(true);
                deleteMenu.getMenu().findItem(R.id.verifyMessageSignatureMenuItem).setOnMenuItemClickListener(
                        new VerifyMessageSignatureListener(position, context));
            } else {
                deleteMenu.getMenu().findItem(R.id.verifyMessageSignatureMenuItem).setVisible(false);
            }
            deleteMenu.show();
            return true;
        }
    }

    private class DeleteMessageListener implements MenuItem.OnMenuItemClickListener {
        int position;

        public DeleteMessageListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            SharedData.messages.deleteMessage(position);
            SharedData.activeConversationActivity.refreshDisplay();
            return true;
        }
    }

    private class CopyMessageListener implements MenuItem.OnMenuItemClickListener {
        int position;
        Context context;

        public CopyMessageListener(int position, Context context) {
            this.position = position;
            this.context = context;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            ClipData copyData = ClipData.newPlainText("CryptoChat", SharedData.messages
                    .messages[position].getContents());
            ((ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE))
                    .setPrimaryClip(copyData);
            return true;
        }
    }

    private class VerifyMessageSignatureListener implements MenuItem.OnMenuItemClickListener {
        int position;
        Context context;

        public VerifyMessageSignatureListener(int position, Context context) {
            this.position = position;
            this.context = context;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            final EditText signingKeyInput = new EditText(context);
            signingKeyInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
            MessageBox.showInputBox(R.string.signingKeyInputBoxTitle,
                    R.string.singingKeyInputBoxText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String secret = signingKeyInput.getText().toString();
                            try {
                                if (SharedData.messages.messages[position].verifySignature(secret)) {
                                    MessageBox.showMessageBox(R.string.successBoxTitle, R.string.signatureOkText,
                                            context);
                                } else {
                                    MessageBox.showMessageBox(R.string.errorBoxTitle, R.string.signatureBadText,
                                            context);
                                }
                            } catch (SecureMessagingException error) {
                                MessageBox.showMessageBox(R.string.errorBoxTitle, ResponseTranslator.getUserFriendlyResponse
                                        (error.getMessage(), context), context);
                            }
                        }
                    }, context, signingKeyInput);
            return true;
        }
    }

    private class MessageClickListener implements View.OnClickListener {
        private int position;

        public MessageClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            SharedData.activeConversationActivity.messageSelected(position);
        }
    }
}
