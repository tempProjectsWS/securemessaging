package nidzo.cryptochat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

public class MessageBox {

    public static void showMessageBox(int title, int text, Context context) {
        showMessageBox(context.getString(title), context.getString(text), context);
    }
    public static void showMessageBox(int title, String text, Context context) {
        showMessageBox(context.getString(title), text, context);
    }
    public static void showMessageBox(String title, String text, Context context)
    {
        showMessageBox(title, text, null, context);
    }
    public static void showMessageBox(
            int title, int text, DialogInterface.OnClickListener onOkClick, Context context) {
        showMessageBox(context.getString(title), context.getString(text), onOkClick, context);
    }

    public static void showMessageBox(
            String title, String text, DialogInterface.OnClickListener onOkClick, Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(text);
        dialog.setTitle(title);
        dialog.setPositiveButton(context.getString(R.string.okButtonText), onOkClick);
        dialog.setCancelable(true);
        dialog.create().show();
    }

    public static void showConfirmationBox(
            int title, int text, DialogInterface.OnClickListener onOkClick, Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(text);
        dialog.setTitle(title);
        dialog.setPositiveButton(R.string.okButtonText, onOkClick);
        dialog.setNegativeButton(R.string.noButtonText, null);
        dialog.setCancelable(true);
        dialog.create().show();
    }

    public static void showInputBox(
            int title, int text, DialogInterface.OnClickListener onOkClick, Context context, EditText input) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(text);
        dialog.setTitle(title);
        dialog.setPositiveButton(R.string.okButtonText, onOkClick);
        dialog.setNegativeButton(R.string.noButtonText, null);
        dialog.setView(input);
        dialog.setCancelable(true);
        dialog.create().show();
    }

}
