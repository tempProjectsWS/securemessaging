package nidzo.cryptochat;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import nidzo.securemessagingclient.MessageList;
import nidzo.securemessagingclient.SecureMessagingException;

public class DataStorage {
    private final static String storedPassword = "StoredPassword";
    private final static String prefsName = "SMGCredentials";
    private final static String storedUsername = "StoredUsername";
    private final static String storedToken = "StoredToken_";
    private final static String storedPublicKey = "StoredPubkey_";
    private final static String storedPrivateKey = "StoredPrivkey_";
    private final static String storedMessages = "StoredMessages_";
    private final static String notifierUsername = "notifierUsernames_";
    private final static String resumeUsername = "resumeUsername_";
    private final static String resumePassword = "resumePassword_";
    private final static String gcmKey = "GCMKey";

    public static String getResumeUsername(Context context)
    {
        return getStoredItem(resumeUsername, context);
    }
    public static String getResumePassword(Context context)
    {
        return getStoredItem(resumePassword, context);
    }
    public static void storeResumeUsername(String username, Context context)
    {
        storeItem(resumeUsername, username, context);
    }
    public static void storeResumePassword(String password, Context context)
    {
        storeItem(resumePassword, password, context);
    }

    public static String getGCMKey(Context context) {
        return getStoredItem(gcmKey, context);
    }

    public static void storeGCMKey(Context context, String key) {
        storeItem(gcmKey, key, context);
    }

    public static Set<String> getNotifierUsernames(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        return prefs.getStringSet(notifierUsername, null);
    }

    public static void removeNotifierUsername(String username, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Set<String> usernameSet = prefs.getStringSet(notifierUsername,
                new CopyOnWriteArraySet<String>());
        usernameSet.remove(username);
        prefsEditor.putStringSet(notifierUsername, usernameSet);
        prefsEditor.apply();
    }

    public static void storeNotifierUsername(String username, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Set<String> usernameSet = prefs.getStringSet(notifierUsername,
                new CopyOnWriteArraySet<String>());
        usernameSet.add(username);
        prefsEditor.putStringSet(notifierUsername, usernameSet);
        prefsEditor.apply();
    }

    public static void storeMessages(String username, MessageList messages, Context context) {
        try {
            if (messages!=null) {
                storeDataToFile(storedMessages + username, messages.toJSON(), context);
            }
            else
            {
                storeDataToFile(storedMessages + username, null, context);
            }
        } catch (SecureMessagingException ignored) {

        }
    }

    public static MessageList getStoredMessages(String username, Context context) {
        String messagesJson = readDataFromFile(storedMessages + username, context);
        if (messagesJson == null || messagesJson.equals("")) {
            return new MessageList();
        }
        try {
            return MessageList.getFromJSON(messagesJson, null, null, null, null, false);
        }
        catch (SecureMessagingException error) {
            return new MessageList();
        }
    }

    public static void storeCredentials(String username, String password, Context context) {
        storeItem(storedPassword, password, context);
        storeItem(storedUsername, username, context);
    }

    public static String[] getStoredCredentials(Context context) {
        String[] credentials = new String[2];
        credentials[0] = getStoredItem(storedUsername, context);
        credentials[1] = getStoredItem(storedPassword, context);
        if (credentials[0] == null || credentials[1] == null) {
            return null;
        }
        else {
            return credentials;
        }
    }

    public static void storeToken(String username, String token, Context context) {
        storeItem(storedToken + username, token, context);
    }

    public static String getStoredToken(String username, Context context) {
        return getStoredItem(storedToken + username, context);
    }

    public static void storeKeySet(
            String username, String pubkey, String privkey, Context context) {
        storeItem(storedPublicKey + username, pubkey, context);
        storeItem(storedPrivateKey + username, privkey, context);
    }

    public static String[] getStoredKeySet(String username, Context context) {
        String[] keySet = new String[2];
        keySet[0] = getStoredItem(storedPublicKey + username, context);
        keySet[1] = getStoredItem(storedPrivateKey + username, context);
        if (keySet[0] == null || keySet[1] == null) {
            return null;
        }
        else {
            return keySet;
        }
    }

    private static void storeItem(String name, String value, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if(value==null) prefsEditor.remove(name);
        else prefsEditor.putString(name, value);
        prefsEditor.apply();
    }

    private static String getStoredItem(String name, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
        return prefs.getString(name, null);
    }

    private static void storeDataToFile(String name, String data, Context context) {
        try {
            if(data!=null) {
                FileOutputStream outputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
                outputStream.write(data.getBytes());
                outputStream.close();
            }
            else context.deleteFile(name);
        }
        catch (IOException ignored) {

        }
    }

    private static String readDataFromFile(String name, Context context) {
        try {
            FileInputStream inputStream = context.openFileInput(name);
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream));
            String line, contents = "";
            while ((line = fileReader.readLine()) != null) {
                contents += line;
            }
            return contents;
        }
        catch (IOException e) {
            return null;
        }
    }
}
