package nidzo.securemessagingclient;

import org.json.JSONException;
import org.json.JSONObject;

import nidzo.securemessagingclient.cryptography.AES;
import nidzo.securemessagingclient.cryptography.CryptographyException;
import nidzo.securemessagingclient.cryptography.DiffieHellman;
import nidzo.securemessagingclient.http.HTTPStrings;

public class MessageFile
{
    public String Contents, PublicKeyUsed, PublicKeyCalculated;

    public static MessageFile generateUploadFile(byte[] contents, String peerKey, long timeDiff) throws CryptographyException {
        String messagePrivateKey = DiffieHellman.generatePrivate();
        String calculatedPublicKey = DiffieHellman.calculatePublic(messagePrivateKey);
        String aesKey = DiffieHellman.calculateAESKey(messagePrivateKey, peerKey);
        long currentTime = System.currentTimeMillis() / 1000 + timeDiff;
        String timeStampString = String.valueOf(currentTime);

        while (timeStampString.length() < 18) {
            timeStampString = "0" + timeStampString;
        }
        byte[] timeStampBytes = timeStampString.getBytes();
        byte[] plaintext=new byte[contents.length+timeStampBytes.length];

        System.arraycopy(contents, 0, plaintext, 0, contents.length);
        System.arraycopy(timeStampBytes, 0, plaintext, contents.length, timeStampBytes.length);

        String ciphertext = AES.encrypt(aesKey, plaintext);
        return new MessageFile(ciphertext, peerKey, calculatedPublicKey);
    }

    public MessageFile(String contents, String publicKeyUsed, String publicKeyCalculated)
    {
        Contents=contents;
        PublicKeyCalculated = publicKeyCalculated;
        PublicKeyUsed = publicKeyUsed;
    }
    public String toJSON() throws SecureMessagingException {
        JSONObject fileJSON = new JSONObject();
        try {
            fileJSON.put(HTTPStrings.POSTKeys.POST_MSG_PUBLICKEY_USED, PublicKeyUsed);
            fileJSON.put(HTTPStrings.POSTKeys.POST_MSG_PUBLCIKEY_CALCULATED, PublicKeyCalculated);
            fileJSON.put(HTTPStrings.POSTKeys.POST_MSG_CONTENTS, Contents);
            return fileJSON.toString();
        }
        catch (JSONException e) {
            throw new SecureMessagingException("JSON Creation error");
        }
    }
    public static byte[] getFileContentsFromJSON(String fileJsonString, Message fileMessage) throws SecureMessagingException {
        try {
            JSONObject fileJson = new JSONObject(fileJsonString);
            String fileContents = fileJson.getString(HTTPStrings.JSONKeys.JSON_MSG_CONTENTS);
            byte[] contents = AES.decrypt(fileMessage.fileEncryptionKey(), fileContents);
            byte[] timeStampContents = new byte[18];
            System.arraycopy(contents, contents.length - 18, timeStampContents, 0, 18);
            String timeStampString = new String(timeStampContents);
            while (timeStampString.startsWith("0")) {
                timeStampString = timeStampString.substring(1);
            }
            long timeDifference = Long.decode(timeStampString);
            if (Math.abs(fileMessage.getTimestamp() - timeDifference) > 150) {
                throw new CryptographyException("Bad timestamp");
            }
            byte[] realContents = new byte[contents.length-18];
            System.arraycopy(contents, 0, realContents, 0, realContents.length);
            return realContents;
        }
        catch (JSONException e) {
            throw new SecureMessagingException("Bad message contents");
        }

    }
}