package nidzo.securemessagingclient;

import nidzo.securemessagingclient.http.HTTPStrings;
import nidzo.securemessagingclient.http.RequestParameters;
import nidzo.securemessagingclient.http.RequestSender;

public class UserManagement {
    public static void registerUser(
            String username, String email, String password) throws SecureMessagingException {
        RequestParameters parameters = new RequestParameters();
        parameters.addItem(HTTPStrings.POSTKeys.POST_USER_NAME, username);
        parameters.addItem(HTTPStrings.POSTKeys.POST_USER_EMAIL, email);
        parameters.addItem(HTTPStrings.POSTKeys.POST_USER_PASSWORD, password);
        new RequestSender().sendRequest(HTTPStrings.addresses.ADDR_ADD_USER, parameters);
    }

    public static void forgotPassword(String username, String email) throws SecureMessagingException {
        RequestParameters parameters = new RequestParameters();
        parameters.addItem(HTTPStrings.POSTKeys.POST_USER_NAME, username);
        parameters.addItem(HTTPStrings.POSTKeys.POST_USER_EMAIL, email);
        new RequestSender().sendRequest(HTTPStrings.addresses.ADDR_FORGOT, parameters);
    }
}
