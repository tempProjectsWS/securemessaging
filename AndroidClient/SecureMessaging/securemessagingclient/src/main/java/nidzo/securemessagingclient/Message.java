package nidzo.securemessagingclient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import nidzo.securemessagingclient.cryptography.AES;
import nidzo.securemessagingclient.cryptography.CryptographyException;
import nidzo.securemessagingclient.cryptography.DiffieHellman;
import nidzo.securemessagingclient.cryptography.Signatures;
import nidzo.securemessagingclient.http.HTTPStrings.JSONKeys;
import nidzo.securemessagingclient.http.HTTPStrings.POSTKeys;
import nidzo.securemessagingclient.http.RequestParameters;

public class Message {
    private String contents;
    private String publicKeyUsed;
    private String publicKeyCalculated;
    private String sender;
    private String receiver;
    private String id;
    private double timestamp;
    private boolean isRead;
    private boolean isFile;
    private String fileKey;
    private String fileName;
    private String fileMimeType;
    private String fileEncryptionKey;
    private String signature;




    public Message(String contents, String sender, String receiver, long timeDiff) {
        this.contents = contents;
        this.sender = sender;
        this.receiver = receiver;
        timestamp = System.currentTimeMillis() / 1000 + timeDiff;
        this.isRead = true;
    }

    public Message() {
        this.isRead = true;
    }

    public static Message getFromJSON(
            String json, String publicKey1, String privateKey1, String publicKey2,
            String privateKey2) throws SecureMessagingException {
        return getFromJSON(json, publicKey1, privateKey1, publicKey2, privateKey2, true);
    }



    public static Message getFromJSON(
            String json, String publicKey1, String privateKey1, String publicKey2,
            String privateKey2, boolean decrypt) throws SecureMessagingException {
        try {
            JSONObject messageObject = new JSONObject(json);
            Message newMessage = new Message();
            newMessage.contents = messageObject.getString(JSONKeys.JSON_MSG_CONTENTS);
            newMessage.publicKeyUsed = messageObject.getString(JSONKeys.JSON_MSG_PUBLICKEY_USED);
            newMessage.publicKeyCalculated = messageObject.getString(JSONKeys
                    .JSON_MSG_PUBLCIKEY_CALCULATED);
            newMessage.sender = messageObject.getString(JSONKeys.JSON_MSG_SENDER);
            newMessage.receiver = messageObject.getString(JSONKeys.JSON_MSG_RECEIVER);
            newMessage.id = messageObject.getString(JSONKeys.JSON_MSG_ID);
            newMessage.timestamp = messageObject.getDouble(JSONKeys.JSON_MSG_TIMESTAMP);
            if(messageObject.has(JSONKeys.JSON_MSG_SM_SIGNATURE))
            {
                newMessage.signature = messageObject.getString(JSONKeys.JSON_MSG_SM_SIGNATURE);
            }
            else newMessage.signature=null;
            newMessage.isFile = messageObject.has(JSONKeys.JSON_MESSAGE_IS_FILE) && messageObject
                    .getBoolean(JSONKeys.JSON_MESSAGE_IS_FILE);
            if (newMessage.isFile)
            {
                newMessage.fileName = messageObject.getString(JSONKeys.JSON_FILENAME);
                newMessage.fileMimeType = messageObject.getString(JSONKeys.JSON_FILE_MIMETYPE);
                newMessage.fileKey = messageObject.getString(JSONKeys.JSON_FILE_KEY);
                if (messageObject.has(JSONKeys.JSON_FILE_ENCRYPTION_KEY))
                {
                    newMessage.fileEncryptionKey = messageObject.getString(JSONKeys.JSON_FILE_ENCRYPTION_KEY);
                }
            }
            else
            {
                newMessage.fileName=newMessage.fileMimeType="";
            }
            if (decrypt) {
                newMessage.decrypt(publicKey1, privateKey1, publicKey2, privateKey2);
                newMessage.isRead = false;
            }
            else {
                newMessage.isRead = !messageObject.has("IS_READ") || messageObject.getBoolean("IS_READ");

            }
            return newMessage;
        }
        catch (JSONException e) {
            throw new SecureMessagingException("Bad message contents");
        }
    }

    public static RequestParameters generatePOST(
            String contents, String peer, String peerKey, long timeDiff, String signatureSecret) throws CryptographyException {
        String messagePrivateKey = DiffieHellman.generatePrivate();
        String calculatedPublicKey = DiffieHellman.calculatePublic(messagePrivateKey);
        String aesKey = DiffieHellman.calculateAESKey(messagePrivateKey, peerKey);
        long currentTime = System.currentTimeMillis() / 1000 + timeDiff;
        String timeStampString = String.valueOf(currentTime);

        while (timeStampString.length() < 18) {
            timeStampString = "0" + timeStampString;
        }
        contents += timeStampString;
        byte[] plaintext;
        try {
            plaintext = contents.getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException error) {
            plaintext = contents.getBytes();
        }
        String ciphertext = AES.encrypt(aesKey, plaintext);
        RequestParameters parameters = new RequestParameters();
        parameters.addItem(POSTKeys.POST_MSG_PEER, peer);
        parameters.addItem(POSTKeys.POST_MSG_CONTENTS, ciphertext);
        parameters.addItem(POSTKeys.POST_MSG_PUBLCIKEY_CALCULATED, calculatedPublicKey);
        parameters.addItem(POSTKeys.POST_MSG_PUBLICKEY_USED, peerKey);
        if(signatureSecret!=null)
        {
            String signature = Signatures.generateSignature(calculatedPublicKey, signatureSecret);
            parameters.addItem(POSTKeys.POST_MSG_SM_SIGNATURE, signature);
        }
        return parameters;
    }

    public JSONObject toJSONObject() throws SecureMessagingException {
        try {
            JSONObject messageObject = new JSONObject();
            messageObject.put(JSONKeys.JSON_MSG_CONTENTS, contents);
            messageObject.put(JSONKeys.JSON_MSG_PUBLICKEY_USED, "");
            messageObject.put(JSONKeys.JSON_MSG_PUBLCIKEY_CALCULATED, "");
            if (publicKeyCalculated!=null) messageObject.put(JSONKeys.JSON_MSG_PUBLCIKEY_CALCULATED, publicKeyCalculated);
            messageObject.put(JSONKeys.JSON_MSG_SENDER, sender);
            messageObject.put(JSONKeys.JSON_MSG_RECEIVER, receiver);
            messageObject.put(JSONKeys.JSON_MSG_ID, 0);
            messageObject.put(JSONKeys.JSON_MSG_TIMESTAMP, timestamp);
            messageObject.put(JSONKeys.JSON_MESSAGE_IS_FILE, isFile);
            messageObject.put(JSONKeys.JSON_FILE_KEY, fileKey);
            messageObject.put(JSONKeys.JSON_FILENAME, fileName);
            messageObject.put(JSONKeys.JSON_FILE_MIMETYPE, fileMimeType);
            messageObject.put(JSONKeys.JSON_FILE_ENCRYPTION_KEY, fileEncryptionKey);
            if(signature!=null) messageObject.put(JSONKeys.JSON_MSG_SM_SIGNATURE, signature);
            messageObject.put("IS_READ", isRead);
            return messageObject;
        }
        catch (JSONException error) {
            throw new SecureMessagingException("JSON Creation error");
        }
    }

    private void decrypt(
            String publicKey1, String privateKey1, String publicKey2, String privateKey2) {
        try {
            String aesKey;
            if (publicKey1 != null && publicKey1.equals(publicKeyUsed)) {
                aesKey = DiffieHellman.calculateAESKey(privateKey1, publicKeyCalculated);
            }
            else if (publicKey2 != null && publicKey2.equals(publicKeyUsed)) {
                aesKey = DiffieHellman.calculateAESKey(privateKey2, publicKeyCalculated);
            }
            else {
                throw new CryptographyException("No valid key");
            }
            if(!isFile)
            {
                contents = new String(AES.decrypt(aesKey, contents));
                String timeStampString = contents.substring(contents.length() - 18);
                while (timeStampString.startsWith("0")) {
                    timeStampString = timeStampString.substring(1);
                }
                long timeDifference = Long.decode(timeStampString);
                if (Math.abs(timestamp - timeDifference) > 150) {
                    throw new CryptographyException("Bad timestamp");
                }
                contents = contents.substring(0, contents.length() - 18);
                fileEncryptionKey="";
            }
            else
            {
                contents=fileName;
                fileEncryptionKey = aesKey;
            }
        }
        catch (CryptographyException error) {
            contents = "Invalid message " + error.getMessage();
        }
    }

    public Date getTimeStamp() {
        return new Date((long) (timestamp) * 1000);
    }

    public String GetTimeStampString(java.text.DateFormat dateFormat) {
        return dateFormat.format(getTimeStamp());
    }

    public String getTimeStampString() {
        return getTimeStampString("yyyy-MM-dd HH:mm:ss");
    }

    public String getTimeStampString(String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        return GetTimeStampString(format);
    }

    public String getContents() {
        return contents;
    }

    public String getPublicKeyUsed() {
        return publicKeyUsed;
    }

    public String getPublicKeyCalculated() {
        return publicKeyCalculated;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getId() {
        return id;
    }

    public double getTimestamp() {
        return timestamp;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    public boolean isFile()
    {
        return isFile;
    }
    public String fileKey() {
        return fileKey;
    }

    public String fileEncryptionKey() {
        return fileEncryptionKey;
    }

    public String fileName()
    {
        return fileName;
    }

    public boolean isSigned()
    {
        return signature!=null;
    }

    public boolean verifySignature(String secret) throws CryptographyException {
        return signature != null && Signatures.verifySignature(publicKeyCalculated, secret, signature);
    }

    public void userRenamed(String oldName, String newName)
    {
        if(sender.equals(oldName)) sender=newName;
        else if (receiver.equals(oldName)) receiver=newName;
    }

    public String fileMimeType()
    {
        return fileMimeType;
    }
}
