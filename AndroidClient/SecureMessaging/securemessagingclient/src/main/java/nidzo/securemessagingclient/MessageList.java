package nidzo.securemessagingclient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Comparator;

import nidzo.securemessagingclient.http.HTTPStrings.JSONKeys;

public class MessageList {
    public Message[] messages;
    public boolean more;

    public MessageList() {
        messages = new Message[0];
        more = false;
    }

    public static MessageList getFromJSON(
            String json, String publicKey1, String privateKey1, String publicKey2,
            String privateKey2) throws SecureMessagingException {
        return getFromJSON(json, publicKey1, privateKey1, publicKey2, privateKey2, true);
    }

    public static MessageList getFromJSON(
            String json, String publicKey1, String privateKey1, String publicKey2,
            String privateKey2, boolean decrypt) throws SecureMessagingException {
        try {
            JSONObject messageListObject = new JSONObject(json);
            MessageList newMessageList = new MessageList();
            newMessageList.more = messageListObject.getBoolean(JSONKeys.JSON_MORE_MESSAGES);
            JSONArray messagesArray = messageListObject.getJSONArray(JSONKeys.JSON_MESSAGES);
            newMessageList.messages = new Message[messagesArray.length()];
            for (int i = 0; i < messagesArray.length(); i++) {
                Message createdMessage = Message.getFromJSON(messagesArray.getJSONObject(i)
                        .toString(), publicKey1, privateKey1, publicKey2, privateKey2, decrypt);
                newMessageList.messages[i] = createdMessage;
            }
            newMessageList.Sort();
            return newMessageList;
        }
        catch (JSONException error) {
            throw new SecureMessagingException("Bad message format");
        }
    }

    public String toJSON() throws SecureMessagingException {
        try {
            JSONObject messageListObject = new JSONObject();
            messageListObject.put(JSONKeys.JSON_MORE_MESSAGES, more);
            JSONArray messagesArray = new JSONArray();
            for (Message message : messages) {
                messagesArray.put(message.toJSONObject());
            }
            messageListObject.put(JSONKeys.JSON_MESSAGES, messagesArray);
            return messageListObject.toString();
        }
        catch (JSONException error) {
            throw new SecureMessagingException("JSON Creation error");
        }

    }

    public void append(MessageList listToAppend) {
        Message[] newMessages = new Message[messages.length + listToAppend.messages.length];
        System.arraycopy(messages, 0, newMessages, 0, messages.length);
        System.arraycopy(listToAppend.messages, 0, newMessages, messages.length,
                listToAppend.messages.length);
        messages = newMessages;
        Sort();
    }

    public void insert(Message messageToAppend) {
        Message[] newMessages = new Message[messages.length + 1];
        System.arraycopy(messages, 0, newMessages, 0, messages.length);
        newMessages[newMessages.length - 1] = messageToAppend;
        messages = newMessages;
        Sort();
    }

    public void deleteMessage(int id) {
        Message[] newMessages = new Message[messages.length - 1];
        System.arraycopy(messages, 0, newMessages, 0, id);
        System.arraycopy(messages, id + 1, newMessages, id + 1 - 1, messages.length - (id + 1));
        messages = newMessages;
    }

    public void Sort() {
        Arrays.sort(messages, new MessageComparator());
    }

    class MessageComparator implements Comparator<Message> {

        @Override
        public int compare(Message message, Message message2) {
            if (message.getTimestamp() < message2.getTimestamp()) {
                return -1;
            }
            else {
                return 1;
            }
        }
    }
}
