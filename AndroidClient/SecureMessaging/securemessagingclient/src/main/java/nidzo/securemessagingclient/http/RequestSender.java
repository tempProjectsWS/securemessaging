package nidzo.securemessagingclient.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.config.RequestConfig;
import ch.boye.httpclientandroidlib.client.entity.UrlEncodedFormEntity;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.ContentType;
import ch.boye.httpclientandroidlib.entity.mime.MultipartEntityBuilder;
import ch.boye.httpclientandroidlib.impl.client.CloseableHttpClient;
import ch.boye.httpclientandroidlib.impl.client.HttpClientBuilder;
import nidzo.securemessagingclient.SecureMessagingException;

public class RequestSender {
    private static final String userAgent = "Android Client library";
    private CloseableHttpClient client;

    public RequestSender() {
        RequestConfig.Builder configBuilder = RequestConfig.custom();
        configBuilder.setConnectionRequestTimeout(60000);
        configBuilder.setConnectTimeout(60000);
        configBuilder.setSocketTimeout(60000);
        HttpClientBuilder builder = HttpClientBuilder.create();
        builder.setDefaultRequestConfig(configBuilder.build());
        client = builder.build();
    }

    public String sendRequest(String address, RequestParameters parameters) throws
            SecureMessagingException {
        String responseContents = "";
        try {
            HttpPost post = new HttpPost(HTTPStrings.addresses.ADDR_SITE + address);
            post.addHeader("User-Agent", userAgent);
            post.setEntity(new UrlEncodedFormEntity(parameters.getParameters()));
            HttpResponse response = client.execute(post);
            int status = response.getStatusLine().getStatusCode()/100;
            if(status==4 || status==5) throw new SecureMessagingException(HTTPStrings.responses.RESPONSE_NETWORK_ERROR);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity()
                    .getContent()));
            String line;
            while ((line = reader.readLine()) != null) {
                responseContents += line;
            }
        }
        catch (Exception e) {
            throw new SecureMessagingException(HTTPStrings.responses.RESPONSE_NETWORK_ERROR);
        }
        if (responseContents.equals("") || HTTPStrings.responses.isError(responseContents)) {
            throw new SecureMessagingException(responseContents);
        }
        return responseContents;
    }

    public String sendRequest(String address) throws SecureMessagingException {
        return sendRequest(address, new RequestParameters());
    }

    public String uploadFile(String address, String fileName, String fileContents,
                             String mimeType, RequestParameters parameters) throws SecureMessagingException {
        HttpPost post= new HttpPost(address);
        post.addHeader("User-Agent", userAgent);
        //post.addHeader("Content-type", "multipart/form-data");
        MultipartEntityBuilder dataEntityBuilder= MultipartEntityBuilder.create();
        byte[] fileContentBytes = fileContents.getBytes();
        dataEntityBuilder.addBinaryBody(HTTPStrings.POSTKeys.POST_UPLOADED_FILE, fileContentBytes, ContentType.create(mimeType), fileName);
        for(int i=0;i<parameters.getParameters().size();i++)
        {
            String key = parameters.getParameters().get(i).getName();
            String value = parameters.getParameters().get(i).getValue();
            dataEntityBuilder.addTextBody(key, value);
        }
        post.setEntity(dataEntityBuilder.build());
        String responseContents = "";
        try{
            HttpResponse response = client.execute(post);
            int status = response.getStatusLine().getStatusCode()/100;
            if(status==4 || status==5) throw new SecureMessagingException(HTTPStrings.responses.RESPONSE_NETWORK_ERROR);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity()
                    .getContent()));
            String line;
            while ((line = reader.readLine()) != null) {
                responseContents += line;
            }
        }
        catch (Exception e) {
            throw new SecureMessagingException(HTTPStrings.responses.RESPONSE_NETWORK_ERROR);
        }
        if (responseContents.equals("") || HTTPStrings.responses.isError(responseContents)) {
            throw new SecureMessagingException(responseContents);
        }
        return responseContents;
    }
}