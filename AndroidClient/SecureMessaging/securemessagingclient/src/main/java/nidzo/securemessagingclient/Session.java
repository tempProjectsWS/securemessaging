package nidzo.securemessagingclient;

import android.location.Address;

import ch.boye.httpclientandroidlib.protocol.HTTP;
import nidzo.securemessagingclient.cryptography.DiffieHellman;
import nidzo.securemessagingclient.http.HTTPStrings;
import nidzo.securemessagingclient.http.HTTPStrings.addresses;
import nidzo.securemessagingclient.http.HTTPStrings.POSTKeys;
import nidzo.securemessagingclient.http.RequestParameters;
import nidzo.securemessagingclient.http.RequestSender;

public class Session {
    private String username;
    private String password;
    private String token;
    private String oldPublic;
    private String oldPrivate;
    private String publicKey;
    private String privateKey;
    private RequestSender requestSender;
    private long timeDiff;
    private boolean loggedIn;
    private static final int MAX_TIME_DIFF = 3600;

    public Session(
            String username, String password, String token, String publicKey,
            String privateKey) throws SecureMessagingException {
        this(username, password, token, publicKey, privateKey, true);
    }

    public Session(
            String username, String password, String token, String publicKey, String privateKey,
            boolean updateKey) throws SecureMessagingException {
        this.username = username;
        this.password = password;
        this.token = token;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.oldPrivate = this.oldPublic = null;
        requestSender = new RequestSender();
        loggedIn = false;
        this.login();
        if (updateKey) {
            this.updateKey();
        }
    }

    public static String[] requestNewToken(String username, String password) throws
            SecureMessagingException {
        String privateKey = DiffieHellman.generatePrivate();
        String publicKey = DiffieHellman.calculatePublic(privateKey);
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_USER_NAME, username);
        params.addItem(POSTKeys.POST_USER_PASSWORD, password);
        params.addItem(POSTKeys.POST_USER_PUBKEY, publicKey);
        String token = new RequestSender().sendRequest(addresses.ADDR_AUTHORIZE_NEW_TOKEN, params);
        return new String[]{token, publicKey, privateKey};
    }

    private void login() throws SecureMessagingException {
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_USER_NAME, username);
        params.addItem(POSTKeys.POST_USER_PASSWORD, password);
        params.addItem(POSTKeys.POST_USER_TOKEN, token);
        String response = requestSender.sendRequest(addresses.ADDR_LOGIN, params);
        if (!response.equals(HTTPStrings.responses.RESPONSE_OK)) {
            throw new SecureMessagingException(HTTPStrings.responses.RESPONSE_NETWORK_ERROR);
        }
        long serverTime;
        try {
            serverTime = Long.decode(requestSender.sendRequest(addresses.ADDR_TIME));
        } catch (NumberFormatException error) {
            throw new SecureMessagingException(HTTPStrings.responses.RESPONSE_NETWORK_ERROR);
        }
        timeDiff = serverTime - (System.currentTimeMillis() / 1000);
        if (timeDiff > MAX_TIME_DIFF) {
            throw new SecureMessagingException(HTTPStrings.responses.RESPONSE_TIMEDIFF_TOO_BIG);
        }
        loggedIn = true;
    }

    private void updateKey() throws SecureMessagingException {
        oldPrivate = privateKey;
        oldPublic = publicKey;
        privateKey = DiffieHellman.generatePrivate();
        publicKey = DiffieHellman.calculatePublic(privateKey);
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_USER_PUBKEY, publicKey);
        requestSender.sendRequest(HTTPStrings.addresses.ADDR_UPDATE_PUBKEY, params);
    }

    public void logout() throws SecureMessagingException {
        if (loggedIn) {
            requestSender.sendRequest(addresses.ADDR_LOGOUT);
            loggedIn = false;
        }
    }

    private String getPublicKey(String peer) throws SecureMessagingException {
        RequestParameters params = new RequestParameters();
        if (peer != null) {
            params.addItem(POSTKeys.POST_MSG_PEER, peer);
        }
        return requestSender.sendRequest(addresses.ADDR_GET_PUBKEY, params);
    }

    public void unauthorize() throws SecureMessagingException {
        requestSender.sendRequest(addresses.ADDR_UNAUTHORIZE);
    }

    public boolean checkNewMessages() throws SecureMessagingException {
        return requestSender.sendRequest(addresses.ADDR_CHECK_NEW_MESSAGES).equals(HTTPStrings
                .responses.RESPONSE_HAS_NEW_MESSAGES);
    }

    public Message sendMessage(String contents, String peer, String signatureSecret) throws SecureMessagingException {
        String peerKey = getPublicKey(peer);
        RequestParameters params = Message.generatePOST(contents, peer, peerKey, timeDiff, signatureSecret);
        requestSender.sendRequest(addresses.ADDR_SEND_MESSAGE, params);
        return new Message(contents, username, peer, timeDiff);
    }

    public void setPassword(String newPassword) throws SecureMessagingException {
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_USER_PASSWORD, newPassword);
        requestSender.sendRequest(addresses.ADDR_SET_PASSWORD, params);
        password = newPassword;
    }

    public void renameUser(String newUsername) throws SecureMessagingException {
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_NEW_USERNAME, newUsername);
        requestSender.sendRequest(addresses.ADDR_RENAME_USER, params);
        username = newUsername;
    }

    public MessageList getMessages() throws SecureMessagingException {
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_MSG_COUNT, "10");
        params.addItem(POSTKeys.POST_SUPPORTS_FILES, "True");
        MessageList tmpMessageList, returnMessageList;
        String messagesJson = requestSender.sendRequest(addresses.ADDR_RECEIVE_MESSAGE, params);
        returnMessageList = MessageList.getFromJSON(messagesJson, publicKey, privateKey,
                oldPublic, oldPrivate);
        boolean more = returnMessageList.more;
        while (more) {
            messagesJson = requestSender.sendRequest(addresses.ADDR_RECEIVE_MESSAGE, params);
            tmpMessageList = MessageList.getFromJSON(messagesJson, publicKey, privateKey,
                    oldPublic, oldPrivate);
            returnMessageList.append(tmpMessageList);
            more = tmpMessageList.more;
        }
        return returnMessageList;
    }

    public void registerGCM(String key) throws SecureMessagingException {
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_GCM_KEY, key);
        requestSender.sendRequest(addresses.ADDR_REGISTER_GCM, params);
    }

    public Message uploadFile(String fileName, byte[] fileContents, String mimeType, String peer) throws SecureMessagingException {
        String address = requestSender.sendRequest(addresses.ADDR_REQUEST_FILE_UPLOAD);
        String publicKey = getPublicKey(peer);
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_MSG_PEER, peer);
        MessageFile file = MessageFile.generateUploadFile(fileContents, publicKey, timeDiff);
        requestSender.uploadFile(address, fileName, file.toJSON(), mimeType, params);
        return new Message(fileName, username, peer, timeDiff);
    }

    public byte[] downloadFile(Message fileMessage) throws SecureMessagingException {
        RequestParameters params = new RequestParameters();
        params.addItem(POSTKeys.POST_FILE_KEY, fileMessage.fileKey());
        String response = requestSender.sendRequest(addresses.ADDR_DOWNLOAD_FILE, params);
        return MessageFile.getFileContentsFromJSON(response, fileMessage);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public String getToken() {
        return token;
    }
}
