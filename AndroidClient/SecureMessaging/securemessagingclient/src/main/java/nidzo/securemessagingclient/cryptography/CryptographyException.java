package nidzo.securemessagingclient.cryptography;

public class CryptographyException extends nidzo.securemessagingclient.SecureMessagingException {

    /**
     *
     */
    private static final long serialVersionUID = -3253957290840272144L;

    public CryptographyException(String message) {
        super(message);
    }
}
