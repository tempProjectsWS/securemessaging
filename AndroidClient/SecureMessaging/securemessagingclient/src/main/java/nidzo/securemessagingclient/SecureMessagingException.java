package nidzo.securemessagingclient;

public class SecureMessagingException extends Exception {

    private static final long serialVersionUID = -5571595357252581326L;

    /**
     *
     */
    public SecureMessagingException(String message) {
        super(message);
    }

}
