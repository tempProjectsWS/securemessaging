package nidzo.securemessagingclient.http;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ch.boye.httpclientandroidlib.NameValuePair;
import ch.boye.httpclientandroidlib.message.BasicNameValuePair;

public class RequestParameters {
    public String address;
    List<String> keys;
    List<String> values;

    public RequestParameters() {
        keys = new LinkedList<String>();
        values = new LinkedList<String>();
        address = "";
    }

    public void addItem(String key, String value) {
        keys.add(key);
        values.add(value);
    }

    public List<NameValuePair> getParameters() {
        ArrayList<NameValuePair> returnedList = new ArrayList<NameValuePair>(keys.size());
        for (int i = 0; i < keys.size(); i++) {
            returnedList.add(new BasicNameValuePair(keys.get(i), values.get(i)));
        }
        return returnedList;
    }
}
