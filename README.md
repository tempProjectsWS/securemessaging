SecureMessaging
===============

A web application for secure, encrypted messaging.
If you want to try the application, see the section named *For possible users*.
If you want to help with the development, please know that the code is currently messy and uncomented, so you should probably contact me first. For the general idea of the project structure, see the section named *Contents of the repository*

For possible users
------------------
Be advised that this project is far from finished and it might be **unstable or insecure**, it currently doesn't provide any kind of installer, and the Windows Runtime and Android clients must be compiled manually. If you still wish to use it, follow this instructions:
####Python client (works on any system that has Python 3, PyQt and PyCrypto installed)
* Clone the repository
* In the `SecureMessagingPythonClient` folder, there is a `secureMessagingClient` folder which contains the package needed by the client, you can copy this package anywhere on the Python path, or you can just copy it to the `QtClient` folder
* Go to the `QtClient` folder and run `main.py`
* Go to File -> Register and fill in the information, you will receive a verification email, after verification you can log in

####Windows runtime client (works on Windows 8.1 and Windows Phone 8.1)
* Clone the repository
* Open the solution in `SecureMessagingWindowsRuntimeClient` with Visual Studio
* Compile it and run it

###Android client
* Clone the repository
* Compile with Gradle/Android studio
* You can also get the application from the Play Store, under the name CryptoChat

Contents of the repository
------
* The `server` directory contains the server code which is supposed to run on Google App Engine, it is already started on      https://sec-msg.appspot.com.
* The `Old RSA implementation` contains client and server code for an outdated version which used RSA instead of Diffie Hellman for key exchange (the server for this version is no longer running)
* The `SecureMessagingWindowsRuntimeClient` contains a client which runs on Windows Runtime with clients available for Windows 8.1 and Windows Phone 8.1. The contents of this folder can be loaded as a solution in Visual Studio 2013. It contains a library that can be used to write other clients, a Windows Phone 8.1 client and a Windows 8.1 client.
* The `SecureMessagingPythonClient` contains a Python 3 package which can be used to write actual clients, and it contains a client written in Python which uses Qt for it's GUI.
* The `SecureMessagingAndroidClient` contains the `securemessagingclient` library and the actual application
