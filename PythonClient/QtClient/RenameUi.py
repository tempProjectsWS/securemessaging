# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Rename.ui'
#
# Created: Sat Sep 12 19:56:42 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(254, 112)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.newUsername = QtGui.QLineEdit(self.centralwidget)
        self.newUsername.setEchoMode(QtGui.QLineEdit.Normal)
        self.newUsername.setObjectName(_fromUtf8("newUsername"))
        self.verticalLayout.addWidget(self.newUsername)
        self.renameUser = QtGui.QPushButton(self.centralwidget)
        self.renameUser.setObjectName(_fromUtf8("renameUser"))
        self.verticalLayout.addWidget(self.renameUser)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusBar = QtGui.QStatusBar(MainWindow)
        self.statusBar.setObjectName(_fromUtf8("statusBar"))
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Rename user", None))
        self.newUsername.setPlaceholderText(_translate("MainWindow", "New username", None))
        self.renameUser.setText(_translate("MainWindow", "Set new username", None))

