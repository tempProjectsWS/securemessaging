from secureMessagingClient import SecureMessagingException
from threading import Thread

from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QProgressBar

from RenameUi import Ui_MainWindow
from baseWindow import BaseWindow
from responseTranslation import responseTranslations
import sharedData
from storage import storeMessages, storeToken, storeKey
from stringresources import strings


class RenameGui(BaseWindow, QtGui.QMainWindow):
    renameCompleted = QtCore.pyqtSignal()

    def __init__(self, session, parent=None):
        super(RenameGui, self).__init__(parent)

        self.session = session

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.renameUser.clicked.connect(self.renameUser)

        self.progressIndicator = QProgressBar(self.ui.statusBar)
        self.progressIndicator.setMinimumHeight(5)
        self.progressIndicator.setVisible(False)
        self.progressIndicator.setMaximum(0)
        self.progressIndicator.setMinimum(0)
        self.ui.statusBar.addWidget(self.progressIndicator)

        self.renameCompleted.connect(self.renameCompletedSlot)

        self.uiTranslate()

    def uiTranslate(self):
        self.setWindowTitle(strings["renameWindowTitle"])
        self.ui.newUsername.setPlaceholderText(strings["newUsernameBoxHint"])
        self.ui.renameUser.setText(strings["renameUserButtonText"])

    def renameCompletedSlot(self):
        self.progressIndicator.setVisible(False)
        self.showMessageBox.emit(strings["renameSuccessText"])

    def renameUser(self):
        self.progressIndicator.setVisible(True)

        def renameThread():
            newUsername = self.ui.newUsername.text()
            if newUsername == "":
                self.showMessageBox.emit(strings["missingFieldsErrorText"])
                return
            try:
                oldUsername = self.session.username
                self.session.renameUser(newUsername)
                for message in sharedData.messages:
                    if message.sender == oldUsername:
                        message.sender = newUsername
                    elif message.receiver == oldUsername:
                        message.receiver = newUsername
                storeMessages(self.session.username, sharedData.messages)
                storeToken(self.session.username, self.session.token)
                storeKey(self.session.username, self.session.public, self.session.private)
                storeMessages(oldUsername, None)
                storeToken(oldUsername, None)
                storeKey(oldUsername, None, None)
                self.renameCompleted.emit()
            except SecureMessagingException as error:
                self.showMessageBox.emit(strings["errorText"] + responseTranslations[str(error)])
                return

        Thread(target=renameThread).start()
