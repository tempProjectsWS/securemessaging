from secureMessagingClient import SecureMessagingException
from threading import Thread
from datetime import datetime

from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QListWidgetItem, QProgressBar

from ConversationsWinUi import Ui_MainWindow
from MainWin import MainWinGui
from Rename import RenameGui
from SetPassword import SetPasswordGui
from baseWindow import BaseWindow
from responseTranslation import responseTranslations
import sharedData
from storage import getMessages, storeMessages, storeToken, storeKey
from stringresources import strings


def messageSort(message):
    epoch = datetime(1970, 1, 1, 0, 0, 0)
    delta = message.timestamp - epoch
    return delta.total_seconds()


class ConversationsWinGui(BaseWindow, QtGui.QMainWindow):
    refreshList = QtCore.pyqtSignal()
    loggedOut = QtCore.pyqtSignal()
    windowClosed = QtCore.pyqtSignal()

    def __init__(self, session, parent=None):
        super(ConversationsWinGui, self).__init__(parent=parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.activeConversationWindows = {}
        self.newConversationWindow = None

        self.session = session
        sharedData.messages = getMessages(self.session.username) or []

        self.ui.refresh.clicked.connect(self.refresh)
        self.ui.logout.triggered.connect(self.logout)

        self.refreshShortcut = QtGui.QShortcut(QtGui.QKeySequence("F5"), self)
        self.refreshShortcut.activated.connect(self.refresh)

        self.setPeerShortcut = QtGui.QShortcut(QtGui.QKeySequence("Return"), self)
        self.setPeerShortcut.activated.connect(self.refresh)
        self.setPeerShortcut.setEnabled(False)

        self.refreshList.connect(self.refreshListSlot)

        self.ui.newConversation.clicked.connect(self.startNewConversation)

        self.ui.conversationsList.itemClicked.connect(self.conversationSelected)

        self.ui.unauthorizeClient.triggered.connect(self.unauthorize)

        self.ui.deleteMessages.triggered.connect(self.clearMessages)

        self.ui.setPassword.triggered.connect(self.setPassword)

        self.ui.markAllAsRead.triggered.connect(self.markAllAsRead)

        self.ui.renameUser.triggered.connect(self.renameUser)

        self.progressIndicator = QProgressBar(self.ui.statusBar)
        self.progressIndicator.setMinimumHeight(5)
        self.progressIndicator.setVisible(False)
        self.progressIndicator.setMaximum(0)
        self.progressIndicator.setMinimum(0)
        self.ui.statusBar.addWidget(self.progressIndicator)

        self.uiTranslate()

        self.refresh()

    def uiTranslate(self):
        self.ui.refresh.setText(strings["refreshButtonText"])
        self.ui.logout.setText(strings["logoutButtonText"])
        self.ui.unauthorizeClient.setText(strings["unauthorizeClientButtonText"])
        self.ui.deleteMessages.setText(strings["deleteAllMessagesButtonText"])
        self.ui.setPassword.setText(strings["setPasswordWindowTitle"])
        self.setWindowTitle(strings["appName"] + ": " + self.session.username)
        self.ui.newConversation.setText(strings["startNewConversationButtonText"])
        self.ui.menuTools.setTitle(strings["toolsMenuText"])
        self.ui.markAllAsRead.setText(strings["markAllAsReadButtonText"])
        self.ui.renameUser.setText(strings["renameUserButtonText"])

    def progressStart(self):
        self.progressIndicator.setVisible(True)

    def askForConfirmation(self, message):
        confirmationBox = QtGui.QMessageBox(parent=self, text=message)
        confirmationBox.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        confirmationBox.setWindowTitle(strings["appName"])
        return confirmationBox.exec() == QtGui.QMessageBox.Yes

    def progressStop(self):
        self.progressIndicator.setVisible(False)

    def show(self):
        super(ConversationsWinGui, self).show()
        self.refreshList.emit()

    def refresh(self):
        self.progressStart()

        def refreshThread():
            try:
                sharedData.messages += self.session.getMessages()
            except SecureMessagingException as error:
                self.showMessageBox.emit(strings["errorText"] + responseTranslations[str(error)])
                print(error)
                return
            sharedData.messages = sorted(sharedData.messages, key=messageSort)
            storeMessages(self.session.username, sharedData.messages)
            self.refreshList.emit()

        Thread(target=refreshThread).start()

    def refreshListSlot(self, refreshChildren=True):
        seenPeers = set()
        self.ui.conversationsList.clear()
        for message in reversed(sharedData.messages):
            if not message.messagePeer in seenPeers:
                seenPeers.add(message.messagePeer)
                peerName = QListWidgetItem(self.ui.conversationsList)
                nameFont = QtGui.QFont()
                contentsFont = QtGui.QFont()
                nameFont.setPointSize(14)
                if not message.read:
                    nameFont.setBold(True)
                    contentsFont.setBold(True)
                peerName.setFont(nameFont)
                peerName.setText(message.messagePeer)
                messageContents = QListWidgetItem(self.ui.conversationsList)
                if message.isFile:
                    contentLines = [message.fileName]
                elif not message.contents:
                    contentLines = [""]
                else:
                    contentLines = message.contents.splitlines(keepends=False)
                contentsFont.setPointSize(10)
                contents = contentLines[0]
                if len(contentLines) > 1:
                    contents += "\n" + contentLines[1]
                if len(contents) > 20:
                    contents = contents[:20]
                messageContents.setFont(contentsFont)
                messageContents.setText(contents)
                self.ui.conversationsList.addItem(peerName)
                self.ui.conversationsList.addItem(messageContents)
                separatorItem = QListWidgetItem(self.ui.conversationsList)
                separatorItem.setFlags(QtCore.Qt.NoItemFlags)
                self.ui.conversationsList.addItem(separatorItem)
        if refreshChildren:
            for peerName in self.activeConversationWindows:
                self.activeConversationWindows[peerName].refreshDisplay.emit()
        self.progressStop()

    def startNewConversation(self):
        if self.newConversationWindow:
            self.newConversationWindow.activateWindow()
            return
        self.newConversationWindow = MainWinGui(self.session, "")
        self.newConversationWindow.refreshDisplay.connect(self.refreshTriggeredInChild)
        self.newConversationWindow.onClose.connect(self.newConversationWindowClosed)
        self.newConversationWindow.sendCompleted.connect(self.conversationStarted)
        self.newConversationWindow.show()

    def conversationStarted(self, message):
        if message.messagePeer in self.activeConversationWindows:
            self.activeConversationWindows[message.messagePeer].close()
        self.newConversationWindow.onClose.disconnect(self.newConversationWindowClosed)
        self.newConversationWindow.sendCompleted.disconnect(self.conversationStarted)
        self.newConversationWindow.onClose.connect(lambda peerName: self.activeConversationWindows.pop(peerName))
        self.activeConversationWindows[message.messagePeer] = self.newConversationWindow
        self.newConversationWindow = None

    def newConversationWindowClosed(self, *args, **kwargs):
        self.newConversationWindow = None

    def conversationSelected(self, listItem):
        row = self.ui.conversationsList.indexFromItem(listItem).row()
        selectedPeer = ""
        if row % 3 == 0:
            selectedPeer += listItem.text()
        elif row % 3 == 1:
            selectedPeer += self.ui.conversationsList.item(row - 1).text()
        else:
            listItem.setSelected(False)
        if selectedPeer != "":
            if selectedPeer in self.activeConversationWindows:
                self.activeConversationWindows[selectedPeer].activateWindow()
            else:
                newWindow = MainWinGui(self.session, selectedPeer)
                self.activeConversationWindows[selectedPeer] = newWindow
                newWindow.onClose.connect(lambda peerName: self.activeConversationWindows.pop(peerName))
                newWindow.refreshDisplay.connect(self.refreshTriggeredInChild)
                newWindow.show()

    def refreshTriggeredInChild(self):
        self.refreshListSlot(False)

    def clearMessages(self):
        if self.askForConfirmation(strings["confirmDeleteAllMessagesText"]):
            sharedData.messages = []
            storeMessages(self.session.username, None)
            self.refreshList.emit()

    def setPassword(self):
        setPasswordUi = SetPasswordGui(self.session, parent=self)
        setPasswordUi.show()

    def renameUser(self):
        renameUi = RenameGui(self.session, parent=self)
        renameUi.show()

    def logout(self):
        self.session.logout()
        self.loggedOut.emit()

    def unauthorize(self):
        if self.askForConfirmation(strings["confirmUnauthorizeClientText"]):
            try:
                self.session.unauthorize()
            except SecureMessagingException as error:
                self.showMessageBox.emit(strings["errorText"] + responseTranslations[str(error)])
                return
            storeMessages(self.session.username, None)
            storeToken(self.session.username, None)
            storeKey(self.session.username, None, None)
            self.logout()

    def closeEvent(self, *args, **kwargs):
        windowsToClose = []
        for window in self.activeConversationWindows:
            windowsToClose += [self.activeConversationWindows[window]]
        for window in windowsToClose:
            window.close()
        if self.newConversationWindow:
            self.newConversationWindow.close()
        self.windowClosed.emit()

    def markAllAsRead(self):
        sharedData.markAsRead()
        storeMessages(self.session.username, sharedData.messages)
        self.refreshList.emit()
