import locale
import os
import sys
import json

englishLocaleFile = "en_US.json"
currentLocaleFile = locale.getdefaultlocale()[0] + ".json"
resourcesDir = os.path.dirname(os.path.abspath(sys.argv[0])) + os.path.sep + "resources" + os.path.sep


class _StringGetter:
    def __init__(self):
        self.englishResources = json.load(open(resourcesDir + englishLocaleFile, encoding="utf-8"))
        try:
            self.localizedResources = json.load(open(resourcesDir + currentLocaleFile, encoding="utf-8"))
        except FileNotFoundError:
            self.localizedResources = {}

    def __getitem__(self, item):
        if item in self.localizedResources:
            return self.localizedResources[item]
        else:
            return self.englishResources[item]


strings = _StringGetter()