from secureMessagingClient.HTTPStrings import *

from stringresources import strings

translationMappings = {
    RESPONSE_MSG_NO_PEER: strings["badPeerErrorText"],
    RESPONSE_USER_NAME_EXISTS: strings["usernameExistsErrorText"],
    RESPONSE_USER_EMAIL_EXISTS: strings["emailExistsErrorText"],
    RESPONSE_USER_BAD_USERNAME: strings["badUsernameErrorText"],
    RESPONSE_USER_BAD_PASSWORD: strings["badPasswordErrorText"],
    RESPONSE_USER_NOT_VERIFIED: strings["userNotVerifiedErrorText"],
    RESPONSE_USER_BAD_EMAIL: strings["badEmailErrorText"],
    RESPONSE_USER_BAD_TOKEN: strings["badTokenErrorText"],
    RESPONSE_NETWORK_ERROR: strings["networkErrorText"],
    RESPONSE_UNHANDLED_ERROR: strings["serverErrorText"],
    RESPONSE_UNAUTHORIZED: strings["unauthorizedClientErrorText"],
    RESPONSE_PEER_INACTIVE: strings["peerInactiveErrorText"],
    RESPONSE_ALREADY_AUTHORIZED: strings["alreadyAuthorizedErrorText"],
    RESPONSE_BAD_FILE_KEY: strings["badFileKeyErrorText"],
    RESPONSE_FILE_CONTENTS_INVALID: strings["badFileErrorText"],
    RESPONSE_TIMEDIFF_TOO_BIG: strings["timediffTooBigErrorText"]
}


class _Translator:
    def __getitem__(self, item):
        try:
            if type(item) == str:
                item = item.encode("utf-8")
            return translationMappings[item]
        except KeyError:
            return strings["unknownErrorText"] + item.decode("utf-8")


responseTranslations = _Translator()
