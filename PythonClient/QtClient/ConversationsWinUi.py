# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ConversationsWin.ui'
#
# Created: Sat Sep 12 19:56:42 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(367, 500)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.conversationsList = QtGui.QListWidget(self.centralwidget)
        self.conversationsList.setObjectName(_fromUtf8("conversationsList"))
        self.horizontalLayout_2.addWidget(self.conversationsList)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 1)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.newConversation = QtGui.QPushButton(self.centralwidget)
        self.newConversation.setObjectName(_fromUtf8("newConversation"))
        self.horizontalLayout.addWidget(self.newConversation)
        self.refresh = QtGui.QPushButton(self.centralwidget)
        self.refresh.setObjectName(_fromUtf8("refresh"))
        self.horizontalLayout.addWidget(self.refresh)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 367, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuTools = QtGui.QMenu(self.menubar)
        self.menuTools.setObjectName(_fromUtf8("menuTools"))
        MainWindow.setMenuBar(self.menubar)
        self.statusBar = QtGui.QStatusBar(MainWindow)
        self.statusBar.setObjectName(_fromUtf8("statusBar"))
        MainWindow.setStatusBar(self.statusBar)
        self.logout = QtGui.QAction(MainWindow)
        self.logout.setObjectName(_fromUtf8("logout"))
        self.unauthorizeClient = QtGui.QAction(MainWindow)
        self.unauthorizeClient.setObjectName(_fromUtf8("unauthorizeClient"))
        self.deleteMessages = QtGui.QAction(MainWindow)
        self.deleteMessages.setObjectName(_fromUtf8("deleteMessages"))
        self.uploadFile = QtGui.QAction(MainWindow)
        self.uploadFile.setObjectName(_fromUtf8("uploadFile"))
        self.setPassword = QtGui.QAction(MainWindow)
        self.setPassword.setObjectName(_fromUtf8("setPassword"))
        self.markAllAsRead = QtGui.QAction(MainWindow)
        self.markAllAsRead.setObjectName(_fromUtf8("markAllAsRead"))
        self.renameUser = QtGui.QAction(MainWindow)
        self.renameUser.setObjectName(_fromUtf8("renameUser"))
        self.menuTools.addAction(self.logout)
        self.menuTools.addAction(self.unauthorizeClient)
        self.menuTools.addAction(self.deleteMessages)
        self.menuTools.addAction(self.setPassword)
        self.menuTools.addAction(self.markAllAsRead)
        self.menuTools.addAction(self.renameUser)
        self.menubar.addAction(self.menuTools.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Secure messaging", None))
        self.newConversation.setText(_translate("MainWindow", "PushButton", None))
        self.refresh.setText(_translate("MainWindow", "Refresh", None))
        self.menuTools.setTitle(_translate("MainWindow", "Tools", None))
        self.logout.setText(_translate("MainWindow", "Log out", None))
        self.unauthorizeClient.setText(_translate("MainWindow", "Unauthorize client", None))
        self.deleteMessages.setText(_translate("MainWindow", "Delete all messages", None))
        self.uploadFile.setText(_translate("MainWindow", "Upload file", None))
        self.setPassword.setText(_translate("MainWindow", "Set password", None))
        self.markAllAsRead.setText(_translate("MainWindow", "Mark all as read", None))
        self.renameUser.setText(_translate("MainWindow", "Rename user", None))

