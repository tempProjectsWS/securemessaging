from secureMessagingClient import addUser, SecureMessagingException
from threading import Thread

from PyQt4 import QtCore
from PyQt4.QtGui import QProgressBar

from AddUserUi import Ui_MainWindow
from baseWindow import BaseWindow
from responseTranslation import responseTranslations
from stringresources import strings


class AddUserGui(BaseWindow):
    addUserCompleted = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(AddUserGui, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.adduser.clicked.connect(self.adduser)

        self.progressIndicator = QProgressBar(self.ui.statusBar)
        self.progressIndicator.setMinimumHeight(5)
        self.progressIndicator.setVisible(False)
        self.progressIndicator.setMaximum(0)
        self.progressIndicator.setMinimum(0)
        self.ui.statusBar.addWidget(self.progressIndicator)

        self.addUserCompleted.connect(self.addUserCompletedSlot)

        self.uiTranslate()

    def uiTranslate(self):
        self.setWindowTitle(strings["registerUserWindowTitle"])
        self.ui.username.setPlaceholderText(strings["usernameBoxHint"])
        self.ui.password.setPlaceholderText(strings["passwordBoxHint"])
        self.ui.email.setPlaceholderText(strings["emailBoxHint"])
        self.ui.confirm.setPlaceholderText(strings["confirmPasswordBoxHint"])
        self.ui.adduser.setText(strings["registerUserButtonText"])

    def addUserCompletedSlot(self):
        self.progressIndicator.setVisible(False)
        self.showMessageBox.emit(strings["registrationSuccessText"])

    def adduser(self):
        self.progressIndicator.setVisible(True)

        def addUserThread():
            name = self.ui.username.text()
            password = self.ui.password.text()
            confirm = self.ui.confirm.text()
            email = self.ui.email.text()
            if name == "" or password == "" or email == "":
                self.showMessageBox.emit(strings["missingFieldsErrorText"])
                return
            if password != confirm:
                self.showMessageBox.emit(strings["passwordsDontMatchErrorText"])
                return
            try:
                addUser(name, email, password)
                self.addUserCompleted.emit()
            except SecureMessagingException as error:
                self.showMessageBox.emit(strings["errorText"] + responseTranslations[str(error)])
                return

        Thread(target=addUserThread).start()
