from secureMessagingClient import SecureMessagingException, requestRecovery
from threading import Thread

from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QProgressBar

from ForgotPasswordUi import Ui_MainWindow
from baseWindow import BaseWindow
from responseTranslation import responseTranslations
from stringresources import strings


class ForgotPasswordUi(BaseWindow, QtGui.QMainWindow):
    requestSendingCompleted = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(ForgotPasswordUi, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.forgot.clicked.connect(self.forgot)
        self.ui.forgot.setDefault(True)
        self.shortcut = QtGui.QShortcut(QtGui.QKeySequence("Return"), self)
        self.shortcut.activated.connect(self.forgot)
        self.shortcut2 = QtGui.QShortcut(QtGui.QKeySequence("Up"), self)
        self.shortcut2.activated.connect(self.focusUp)
        self.shortcut2 = QtGui.QShortcut(QtGui.QKeySequence("Down"), self)
        self.shortcut2.activated.connect(self.focusDown)

        self.progressIndicator = QProgressBar(self.ui.statusBar)
        self.progressIndicator.setMinimumHeight(5)
        self.progressIndicator.setVisible(False)
        self.progressIndicator.setMaximum(0)
        self.progressIndicator.setMinimum(0)
        self.ui.statusBar.addWidget(self.progressIndicator)

        self.requestSendingCompleted.connect(self.requestSendingCompleteSlot)

        self.uiTranslate()

    def uiTranslate(self):
        self.setWindowTitle(strings["forgotPasswordWindowTitle"])
        self.ui.username.setPlaceholderText(strings["usernameBoxHint"])
        self.ui.email.setPlaceholderText(strings["emailBoxHint"])
        self.ui.forgot.setText(strings["forgotPasswordButtonText"])

    def requestSendingCompleteSlot(self):
        self.progressIndicator.setVisible(False)
        self.showMessageBox.emit(strings["forgotSuccessText"])

    def focusUp(self):
        self.ui.username.setFocus()

    def focusDown(self):
        self.ui.email.setFocus()

    def forgot(self):
        self.progressIndicator.setVisible(True)

        def requestSenderThread():
            if self.ui.username.text() and self.ui.email.text():
                try:
                    requestRecovery(self.ui.username.text(), self.ui.email.text())
                    self.requestSendingCompleted.emit()
                except SecureMessagingException as error:
                    self.showMessageBox.emit(strings["errorText"] + responseTranslations[str(error)])
            else:
                self.showMessageBox.emit(strings["missingFieldsErrorText"])

        Thread(target=requestSenderThread).start()
