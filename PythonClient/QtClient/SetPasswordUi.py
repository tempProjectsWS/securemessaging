# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SetPassword.ui'
#
# Created: Sat Sep 12 19:56:42 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(254, 168)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.password = QtGui.QLineEdit(self.centralwidget)
        self.password.setEchoMode(QtGui.QLineEdit.Password)
        self.password.setObjectName(_fromUtf8("password"))
        self.verticalLayout.addWidget(self.password)
        self.newPassword = QtGui.QLineEdit(self.centralwidget)
        self.newPassword.setEchoMode(QtGui.QLineEdit.Password)
        self.newPassword.setObjectName(_fromUtf8("newPassword"))
        self.verticalLayout.addWidget(self.newPassword)
        self.confirm = QtGui.QLineEdit(self.centralwidget)
        self.confirm.setEchoMode(QtGui.QLineEdit.Password)
        self.confirm.setObjectName(_fromUtf8("confirm"))
        self.verticalLayout.addWidget(self.confirm)
        self.setPassword = QtGui.QPushButton(self.centralwidget)
        self.setPassword.setObjectName(_fromUtf8("setPassword"))
        self.verticalLayout.addWidget(self.setPassword)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusBar = QtGui.QStatusBar(MainWindow)
        self.statusBar.setObjectName(_fromUtf8("statusBar"))
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Set password", None))
        self.password.setPlaceholderText(_translate("MainWindow", "Old password", None))
        self.newPassword.setPlaceholderText(_translate("MainWindow", "New password", None))
        self.confirm.setPlaceholderText(_translate("MainWindow", "Confirm password", None))
        self.setPassword.setText(_translate("MainWindow", "Set password", None))

