#!/usr/bin/python3
import sys

from PyQt4 import QtGui as gui

import LoginWin


if __name__ == "__main__":
    mainApp = gui.QApplication(sys.argv)
    loginWindow = LoginWin.LoginWinGui()
    loginWindow.show()
    mainApp.exec_()