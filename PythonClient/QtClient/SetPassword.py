from secureMessagingClient import SecureMessagingException
from threading import Thread

from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QProgressBar

from SetPasswordUi import Ui_MainWindow
from baseWindow import BaseWindow
from responseTranslation import responseTranslations
from stringresources import strings


class SetPasswordGui(BaseWindow, QtGui.QMainWindow):
    setPasswordCompleted = QtCore.pyqtSignal()

    def __init__(self, session, parent=None):
        super(SetPasswordGui, self).__init__(parent)

        self.session = session

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.setPassword.clicked.connect(self.setPassword)

        self.progressIndicator = QProgressBar(self.ui.statusBar)
        self.progressIndicator.setMinimumHeight(5)
        self.progressIndicator.setVisible(False)
        self.progressIndicator.setMaximum(0)
        self.progressIndicator.setMinimum(0)
        self.ui.statusBar.addWidget(self.progressIndicator)

        self.setPasswordCompleted.connect(self.setPasswordCompletedSlot)

        self.uiTranslate()

    def uiTranslate(self):
        self.setWindowTitle(strings["setPasswordWindowTitle"])
        self.ui.password.setPlaceholderText(strings["passwordBoxHint"])
        self.ui.confirm.setPlaceholderText(strings["confirmPasswordBoxHint"])
        self.ui.newPassword.setPlaceholderText(strings["newPasswordBoxHint"])
        self.ui.setPassword.setText(strings["setPasswordButtonText"])

    def setPasswordCompletedSlot(self):
        self.progressIndicator.setVisible(False)
        self.showMessageBox.emit(strings["passwordSetSuccessText"])

    def setPassword(self):
        self.progressIndicator.setVisible(True)

        def setPasswordThread():
            password = self.ui.password.text()
            newPassword = self.ui.newPassword.text()
            confirm = self.ui.confirm.text()
            if confirm == "" or password == "":
                self.showMessageBox.emit(strings["missingFieldsErrorText"])
                return
            if newPassword != confirm:
                self.showMessageBox.emit(strings["passwordsDontMatchErrorText"])
                return
            if password != self.session.password.decode("utf-8"):
                self.showMessageBox.emit(strings["oldPasswordInvalidErrorText"])
                return
            try:
                self.session.setPassword(newPassword)
                self.setPasswordCompleted.emit()
            except SecureMessagingException as error:
                self.showMessageBox.emit(strings["errorText"] + responseTranslations[str(error)])
                return

        Thread(target=setPasswordThread).start()
