messages = []


def markAsRead(peerName=None):
    for message in messages:
        if (not peerName) or message.messagePeer == peerName:
            message.read = True
