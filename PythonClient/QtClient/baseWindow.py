from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QMainWindow

from stringresources import strings


class BaseWindow(QMainWindow):
    showMessageBox = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)
        self.showMessageBox.connect(self.showMessageBoxSlot)

    def showMessageBoxSlot(self, messageText):
        self.progressIndicator.setVisible(False)
        messageBox = QtGui.QMessageBox(parent=self, text=(messageText))
        messageBox.setWindowTitle(strings["appName"])
        messageBox.show()