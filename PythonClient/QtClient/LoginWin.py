from secureMessagingClient import Session, SecureMessagingException
from secureMessagingClient import HTTPStrings
from threading import Thread

from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QProgressBar

from AddUser import AddUserGui
from LoginWinUi import Ui_MainWindow
from ForgotPassword import ForgotPasswordUi
from baseWindow import BaseWindow
import storage
from responseTranslation import responseTranslations
from stringresources import strings
from ConversationsWin import ConversationsWinGui


class LoginWinGui(BaseWindow, QtGui.QMainWindow):
    loginComplete = QtCore.pyqtSignal(Session)

    def __init__(self, parent=None):
        super(LoginWinGui, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.login.clicked.connect(self.login)
        self.ui.login.setDefault(True)
        self.ui.adduser.triggered.connect(self.adduser)
        self.ui.forgot.triggered.connect(self.forgot)
        QtGui.QShortcut(QtGui.QKeySequence("Return"), self).activated.connect(self.login)
        QtGui.QShortcut(QtGui.QKeySequence("Enter"), self).activated.connect(self.login)
        QtGui.QShortcut(QtGui.QKeySequence("Up"), self).activated.connect(self.focusUp)
        QtGui.QShortcut(QtGui.QKeySequence("Down"), self).activated.connect(self.focusDown)
        self.progressIndicator = QProgressBar(self.ui.statusBar)
        self.progressIndicator.setMinimumHeight(5)
        self.progressIndicator.setVisible(False)
        self.progressIndicator.setMaximum(0)
        self.progressIndicator.setMinimum(0)
        self.ui.statusBar.addWidget(self.progressIndicator)
        self.loginComplete.connect(self.loginCompleteSlot)

        self.uiTranslate()

    def uiTranslate(self):
        self.setWindowTitle(strings["loginWindowTitle"])
        self.ui.username.setPlaceholderText(strings["usernameBoxHint"])
        self.ui.password.setPlaceholderText(strings["passwordBoxHint"])
        self.ui.login.setText(strings["loginButtonText"])
        self.ui.adduser.setText(strings["registerUserWindowTitle"])
        self.ui.forgot.setText(strings["forgotPasswordWindowTitle"])
        self.ui.menuTools.setTitle(strings["toolsMenuText"])

    def loginCompleteSlot(self, session):
        self.progressIndicator.setVisible(False)
        self.mainWin = ConversationsWinGui(session)
        self.mainWin.windowClosed.connect(self.close)
        self.mainWin.loggedOut.connect(self.loggedOut)
        self.mainWin.show()
        self.hide()

    def loggedOut(self):
        self.show()
        self.mainWin.windowClosed.disconnect(self.close)
        self.mainWin.close()

    def onMainClose(self):
        pass

    def focusUp(self):
        self.ui.username.setFocus()

    def focusDown(self):
        self.ui.password.setFocus()

    def login(self):
        self.progressIndicator.setVisible(True)

        def loginThread():
            username = self.ui.username.text()
            password = self.ui.password.text()
            if username == "" or password == "":
                self.showMessageBox.emit(strings["missingFieldsErrorText"])
                return
            try:
                if storage.getToken(username) and storage.getKey(username):
                    token = storage.getToken(username)
                    public, private = storage.getKey(username)
                    newSession = Session(username, password, token, public, private)
                    storage.storeKey(newSession.username, newSession.public, newSession.private)
                else:
                    token, public, private = Session.requestNewToken(username, password)
                    newSession = Session(username, password, token, public, private)
                    storage.storeToken(newSession.username, token)
                    storage.storeKey(newSession.username, newSession.public, newSession.private)
                self.loginComplete.emit(newSession)
            except SecureMessagingException as error:
                if error.message == HTTPStrings.RESPONSE_UNAUTHORIZED:
                    storage.storeKey(username, None, None)
                    storage.storeToken(username, None)
                    loginThread()
                    return
                self.showMessageBox.emit(strings["errorText"] + responseTranslations[str(error)])

        Thread(target=loginThread).start()

    def adduser(self):
        addUserGui = AddUserGui(parent=self)
        addUserGui.show()

    def forgot(self):
        forgotUi = ForgotPasswordUi(parent=self)
        forgotUi.show()