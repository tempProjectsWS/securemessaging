import sys

from cx_Freeze import setup, Executable

build_exe_options = {
    "packages": ["PyQt4.QtCore", "inspect", "os", "multiprocessing", "sys", "pickle", "atexit",
                 "hashlib",
                 "hmac", "base64", "Crypto", "urllib", "http", "json"],
    "include_msvcr": True,
    "include_files": ["resources"]}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(name="CryptoChat",
      version="0.1",
      description="Secure messaging",
      options={"build_exe": build_exe_options},
      executables=[Executable("main.py", base=base, shortcutName="CryptoChat Client",
                              targetName="CryptoChat.exe", icon="icon.ico")])