import json
import pickle
from base64 import b64decode, b64encode

from PyQt4.QtCore import QSettings


storedTokens = QSettings("SecureMessaging", "tokens")
storedPrivateKeys = QSettings("SecureMessaging", "privates")
storedPublicKeys = QSettings("SecureMessaging", "publics")
storedMessages = QSettings("SecureMessaging", "messages")


def storeToken(name, token):
    if (not token) and (storedTokens.contains(name)):
        storedTokens.remove(name)
    elif token:
        storedTokens.setValue(name, token)
    storedTokens.sync()


def getToken(name):
    if storedTokens.contains(name):
        return storedTokens.value(name)
    else:
        return None


def storeKey(name, public, private):
    if (not private) or (not public):
        if storedPublicKeys.contains(name):
            storedPublicKeys.remove(name)
        if storedPrivateKeys.contains(name):
            storedPrivateKeys.remove(name)
    elif private and public:
        storedPublicKeys.setValue(name, public)
        storedPrivateKeys.setValue(name, private)
    storedPrivateKeys.sync()
    storedPublicKeys.sync()


def getKey(name):
    if storedPrivateKeys.contains(name) and storedPublicKeys.contains(name):
        return storedPublicKeys.value(name), storedPrivateKeys.value(name)
    else:
        return None


def storeMessages(name, messages):
    if (not messages) and (storedMessages.contains(name)):
        storedMessages.remove(name)
    elif messages:
        storable = []
        for message in messages:
            storable += [b64encode(pickle.dumps(message, protocol=3)).decode("ascii")]
        storable = json.dumps(storable)
        storedMessages.setValue(name, storable)
    storedMessages.sync()


def getMessages(name):
    if storedMessages.contains(name):
        stored = storedMessages.value(name)
        stored = json.loads(stored)
        messages = []
        for storedMessage in stored:
            messages += [pickle.loads(b64decode(storedMessage))]
        return messages


