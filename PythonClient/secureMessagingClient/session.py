from atexit import register
from datetime import datetime, timedelta

from .HTTPStuff import makeOpener, sendRequest, uploadFile
from .HTTPStrings import *
from .cryptography import calculateDHPublic, generateDHPrivate
from .message import Message
from .exception import SecureMessagingException


class Session:
    MAX_TIMEDIFF = 3600

    def __init__(self, username, password, token, publicKey, privateKey, updateKeys=True):
        self.loggedIn = False
        self.opener = makeOpener()
        self.username = username
        self.timeDiff = 0
        self.private = privateKey
        self.public = publicKey
        self.oldPrivate = None
        self.oldPublic = None
        self.token = token
        if type(password) == str:
            self.password = password.encode("utf-8")
        else:
            self.password = password
        self.login(updateKeys)

    @staticmethod
    def requestNewToken(username, password):
        if type(password) == str:
            password = password.encode("utf-8")
        private = generateDHPrivate()
        public = calculateDHPublic(private)
        opener = makeOpener()
        response = sendRequest(opener, ADDR_AUTHORIZE_NEW_TOKEN, {POST_USER_NAME: username,
                                                                  POST_USER_PASSWORD: password,
                                                                  POST_USER_PUBKEY: public},
                               rawResponse=True)
        token = response
        return token, public, private

    def updateKey(self):
        self.oldPrivate = self.private
        self.private = generateDHPrivate()
        self.oldPublic = self.public
        self.public = calculateDHPublic(self.private)
        sendRequest(self.opener, ADDR_UPDATE_PUBKEY, {POST_USER_PUBKEY: self.public})
        return self.public, self.private

    def login(self, updateKeys):
        sendRequest(self.opener, ADDR_LOGIN, {POST_USER_NAME: self.username,
                                              POST_USER_PASSWORD: self.password,
                                              POST_USER_TOKEN: self.token})
        if updateKeys:
            self.updateKey()
        serverUnixTime = int(sendRequest(self.opener, ADDR_TIME, {}, rawResponse=True))
        serverTime = datetime(year=1970, month=1, day=1, hour=0, minute=0, second=0) + timedelta(seconds=serverUnixTime)
        localTime = datetime.utcnow()
        try:
            self.timeDiff = int((serverTime - localTime).total_seconds())
        except ValueError:
            raise SecureMessagingException(RESPONSE_NETWORK_ERROR)
        if self.timeDiff>self.MAX_TIMEDIFF:
            raise SecureMessagingException(RESPONSE_TIMEDIFF_TOO_BIG)
        self.loggedIn = True
        register(self.logoutOnExit, self)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        if self.loggedIn:
            self.logout()

    @staticmethod
    def logoutOnExit(self):
        if self.loggedIn:
            self.logout()

    def logout(self):
        sendRequest(self.opener, ADDR_LOGOUT)
        self.loggedIn = False

    def getMessages(self):
        POST = {POST_MSG_COUNT: 10,
                POST_SUPPORTS_FILES: "FILES"}
        more = True
        receivedMessages = []
        while more:
            data = sendRequest(self.opener, ADDR_RECEIVE_MESSAGE, POST)
            for message in data[JSON_MESSAGES]:
                receivedMessages.append(
                    Message.decrypt(message, self.username, self.public, self.private,
                                    self.oldPublic, self.oldPrivate))
            more = data[JSON_MORE_MESSAGES]
        return receivedMessages

    def sendMessage(self, contents, peer, signatureSecret=None):
        peerKey = self._getPublicKey(peer)
        parameters = Message.generatePOST(contents, peer, peerKey, signatureSecret, self.timeDiff)
        sendRequest(self.opener, ADDR_SEND_MESSAGE, data=parameters)

    def sendFile(self, fileName, fileContents, peer, mimeType=b'application/octet-stream'):
        peerKey = self._getPublicKey(peer)
        uploadAddress = sendRequest(self.opener, ADDR_REQUEST_FILE_UPLOAD, rawResponse=True).decode("utf-8")
        uploadParams = Message.generateEncryptedFileContents(fileContents, peer, peerKey, self.timeDiff)
        uploadFile(self.opener, uploadAddress, fileName, uploadParams[0], mimeType, uploadParams[1])

    def downloadFile(self, message):
        parameters = {POST_FILE_KEY: message.fileKey}
        fileContents = sendRequest(self.opener, ADDR_DOWNLOAD_FILE, parameters, rawResponse=True)
        return Message.decryptFileContents(fileContents, message)

    def unauthorize(self):
        sendRequest(self.opener, ADDR_UNAUTHORIZE, {})

    def setPassword(self, newPassword):
        if type(newPassword) == str:
            newPassword = newPassword.encode("utf-8")
        self.password = newPassword
        sendRequest(self.opener, ADDR_SET_PASSWORD, {POST_USER_PASSWORD: self.password}, )

    def hasNewMessages(self):
        return sendRequest(self.opener, ADDR_CHECK_NEW_MESSAGES,
                           {POST_SUPPORTS_FILES: "TRUE"}) == RESPONSE_HAS_NEW_MESSAGES

    def renameUser(self, newUsername):
        sendRequest(self.opener, ADDR_RENAME_USER, {POST_NEW_USERNAME: newUsername})
        self.username = newUsername

    def _getPublicKey(self, peer=None):
        if peer:
            key = sendRequest(self.opener, ADDR_GET_PUBKEY, {POST_MSG_PEER: peer}, rawResponse=True)
            return key
        else:
            key = sendRequest(self.opener, ADDR_GET_PUBKEY, rawResponse=True)
            return key
