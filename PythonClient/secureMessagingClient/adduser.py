from .HTTPStuff import sendRequest, makeOpener
from .HTTPStrings import *


def addUser(name, email, password):
    data = {POST_USER_NAME: name,
            POST_USER_EMAIL: email,
            POST_USER_PASSWORD: password,}
    sendRequest(makeOpener(), ADDR_ADD_USER, data)