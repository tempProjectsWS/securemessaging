from .HTTPStrings import *
from .HTTPStuff import makeOpener, sendRequest


def requestRecovery(username, email):
    opener = makeOpener()
    sendRequest(opener, ADDR_FORGOT, {POST_USER_EMAIL: email,
                                      POST_USER_NAME: username})


def setNewPassword(token, username, newPassword):
    opener = makeOpener()
    sendRequest(opener, ADDR_RECOVERY, {POST_USER_PASSWORD: newPassword,
                                        POST_USER_NAME: username,
                                        POST_USER_TOKEN: token})
