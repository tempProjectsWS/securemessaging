import datetime
import json

from .cryptography import AESDecrypt, AESEncrypt, DecryptionError, calculateDHPublic, calculateDHAES, \
    generateDHPrivate, verifySignature, generateSignature
from .HTTPStrings import *


class Message:
    def __init__(self, contents="", sender="", receiver="", messagePeer="", timeDiff=0):
        self.id = 0
        self.contents = contents
        self.timestamp = datetime.datetime.now() + datetime.timedelta(seconds=timeDiff)
        self.sender = sender
        self.receiver = receiver
        self.messagePeer = messagePeer
        self.isFile = False
        self.fileName = ""
        self.fileKey = ""
        self.fileMimeType = ""
        self.fileEncryptionKey = b""
        self.signature = ""
        self.publicKeyCalculated = ""

    def __new__(cls, *args, **kwargs):
        newMessage = super(Message, cls).__new__(cls)
        newMessage.read = True
        return newMessage

    @staticmethod
    def decrypt(message, myUsername, myPublic1, myPrivate1, myPublic2, myPrivate2):
        newMessage = Message()
        newMessage.read = False
        try:
            publicUsed = message[JSON_MSG_PUBLICKEY_USED].encode("utf-8")
            isFile = message[JSON_MESSAGE_IS_FILE]
            newMessage.isFile = isFile
            newMessage.publicKeyCalculated = message[JSON_MSG_PUBLCIKEY_CALCULATED]
            if publicUsed == myPublic1:
                key = calculateDHAES(newMessage.publicKeyCalculated, myPrivate1)
            elif publicUsed == myPublic2:
                key = calculateDHAES(newMessage.publicKeyCalculated, myPrivate2)
            else:
                raise DecryptionError("No valid key")
            if not isFile:
                newMessage.contents = AESDecrypt(message[JSON_MSG_CONTENTS], key)
                if len(newMessage.contents) < 18:
                    raise DecryptionError("Invalid message")
                timestampString = newMessage.contents[-18:]
                newMessage.contents = newMessage.contents[:-18]
                try:
                    messageTimeStamp = int(timestampString)
                    if abs(messageTimeStamp - message[JSON_MSG_TIMESTAMP]) > 150:
                        raise ValueError
                except ValueError:
                    raise DecryptionError("Invalid timestamp")
            else:
                newMessage.fileName = message[JSON_FILENAME]
                newMessage.fileKey = message[JSON_FILE_KEY]
                newMessage.fileMimeType = message[JSON_FILE_MIMETYPE]
                newMessage.fileEncryptionKey = key

        except DecryptionError as error:
            newMessage.contents = "Invalid message: " + error.message
        newMessage.id = message[JSON_MSG_ID]
        sender = message[JSON_MSG_SENDER]
        newMessage.sender = sender
        timestamp = message[JSON_MSG_TIMESTAMP]
        newMessage.timestamp = datetime.datetime.fromtimestamp(timestamp)

        if sender == myUsername:
            newMessage.messagePeer = message[JSON_MSG_RECEIVER]
        else:
            newMessage.messagePeer = message[JSON_MSG_SENDER]
        newMessage.receiver = message[JSON_MSG_RECEIVER]
        if JSON_MSG_SM_SIGNATURE in message:
            newMessage.signature = message[JSON_MSG_SM_SIGNATURE]
        return newMessage

    @staticmethod
    def generatePOST(contents, peer, peerKey, signatureKey, timeDiff):
        timeStamp = datetime.datetime.utcnow()
        timeStamp = (timeStamp - datetime.datetime(1970, 1, 1, 0, 0, 0)).total_seconds() + timeDiff
        timeStamp = str(int(timeStamp))
        timeStamp = (18 - len(timeStamp)) * "0" + timeStamp
        contents += timeStamp
        messagePrivateKey = generateDHPrivate()
        public = calculateDHPublic(messagePrivateKey)
        key = calculateDHAES(peerKey, messagePrivateKey)
        encryptedMessage = AESEncrypt(contents, key)
        postContents = {POST_MSG_PEER: peer,
                        POST_MSG_CONTENTS: encryptedMessage,
                        POST_MSG_PUBLCIKEY_CALCULATED: public,
                        POST_MSG_PUBLICKEY_USED: peerKey}
        if signatureKey:
            postContents[POST_MSG_SM_SIGNATURE] = generateSignature(public, signatureKey)
        return postContents

    @staticmethod
    def generateEncryptedFileContents(contents, peer, peerKey, timeDiff):
        timeStamp = datetime.datetime.utcnow()
        timeStamp = (timeStamp - datetime.datetime(1970, 1, 1, 0, 0, 0)).total_seconds() + timeDiff
        timeStamp = str(int(timeStamp)).encode("ascii")
        timeStamp = (18 - len(timeStamp)) * b"0" + timeStamp
        contents += timeStamp
        messagePrivateKey = generateDHPrivate()
        public = calculateDHPublic(messagePrivateKey)
        key = calculateDHAES(peerKey, messagePrivateKey)
        encryptedMessage = AESEncrypt(contents, key)
        return (json.dumps({JSON_MSG_CONTENTS: encryptedMessage.decode("ascii"),
                            JSON_MSG_PUBLICKEY_USED: peerKey.decode("ascii"),
                            JSON_MSG_PUBLCIKEY_CALCULATED: public.decode("ascii")}).encode("utf-8"),
                {POST_MSG_PEER: peer})

    @staticmethod
    def decryptFileContents(fileContents, message):
        fileContents = json.loads(fileContents.decode("utf-8"))
        key = message.fileEncryptionKey
        contents = AESDecrypt(fileContents[JSON_MSG_CONTENTS], key, decode=False)
        if len(contents) < 18:
            raise DecryptionError("Invalid message")
        timestampString = contents[-18:]
        contents = contents[:-18]
        try:
            messageTimeStamp = int(timestampString)
            if abs((datetime.datetime.fromtimestamp(messageTimeStamp) - message.timestamp).total_seconds()) > 300:
                raise ValueError
        except ValueError:
            raise DecryptionError("Invalid timestamp")
        return contents

    def verifySignature(self, secret):
        return verifySignature(self.publicKeyCalculated, secret, self.signature)

    def isSigned(self):
        try:
            return self.signature
        except AttributeError:
            return False

    def __str__(self):
        return "Mesage from: " + self.sender + " Contents: " + self.contents

    def getTimeStamp(self, timeformat="%Y-%m-%d %H:%M"):
        return self.timestamp.strftime(format=timeformat)

    def __repr__(self):
        return self.__str__()
