from urllib.request import build_opener, HTTPCookieProcessor, Request
from http.cookiejar import CookieJar
from urllib.parse import urlencode
from json import loads
from .HTTPStrings import *
from .exception import SecureMessagingException
from .cryptography import encode64
from os import urandom


def makeOpener():
    opener = build_opener(HTTPCookieProcessor(CookieJar()))
    opener.addheaders = [('User-agent', "Python Client library")]
    return opener


def sendRequest(opener, address, data={}, rawResponse=False):
    data = urlencode(data).encode("utf-8")
    try:
        response = opener.open(ADDR_SITE + address, data=data).read()
    except OSError:
        response = RESPONSE_NETWORK_ERROR
    if response in errorResponses:
        raise SecureMessagingException(response)
    elif response in goodResponses:
        return response
    elif rawResponse:
        return response
    else:
        return loads(response.decode("utf-8"))


def uploadFile(opener, address, fileName, fileContents, mimeType, data):
    boundary = encode64(urandom(32))
    fileName = fileName.encode("utf-8")
    contentType = b'multipart/form-data; boundary=' + boundary
    partBoundary = b"--" + boundary
    formContents = []
    formContents.extend([b'',
                         partBoundary,
                         b'Content-Disposition: form-data; name="' + POST_UPLOADED_FILE.encode(
                             "ascii") + b'"; filename="' + fileName + b'"',
                         b'Content-Type: ' + mimeType,
                         b'',
                         fileContents,
                         partBoundary
    ])
    for key in data:
        addedParams = [b'Content-Disposition: form-data; name="' + key.encode("utf-8") + b'"',
                       b'',
                       data[key].encode("ascii"),
                       partBoundary
                       ]
        formContents.extend(addedParams)
    formContents[-1] = b"--"+boundary+b"--"
    requestBody = b"\r\n".join(formContents)
    try:
        request = Request(address)
        request.add_header('Content-type', contentType)
        request.add_header('Content-length', len(requestBody))
        request.data = requestBody
        response = opener.open(request).read()
    except OSError:
        response = RESPONSE_NETWORK_ERROR
    if response in errorResponses:
        raise SecureMessagingException(response)
    return response